package com.ug.eon.android.tv;

/* renamed from: com.ug.eon.android.tv.BuildConfigDev */
public final class BuildConfigDev {
    public static final String APPLICATION_ID = "com.ug.eon.android.tv";
    public static final String BUILD_TYPE = "release";
    public static final String CLIENT_ID = "5a3e24b8-70cd-4958-b716-af9ce053e594";
    public static final String CLIENT_SECRET = "aazy6orsi9elhhs17e47lfb4palgszw6igf4y26z";
    public static final String COMMIT_HASH = "6733a01";
    public static final boolean DEBUG = false;
    public static final String DEFAULT_CDN_INFO = "{\"identifier\":\"ug\",\"isDefault\":true,\"domains\":{\"baseApi\":{\"be\":\"ug-be.cdn.united.cloud\",\"af31\":\"ug-af31.cdn.united.cloud\"},\"baseImages\":{\"be\":\"ug-be.cdn.united.cloud\",\"af31\":\"ug-af31.cdn.united.cloud\"},\"baseStatic\":{\"be\":\"ug-be.cdn.united.cloud\",\"af31\":\"ug-af31.cdn.united.cloud\"}},\"id\":1}";
    public static final String DEVICE_PLATFORM = "android_tv";
    public static final String DEVICE_TYPE = "android_tv_";
    public static final String FIRMWARE_VERSION_PROP = "";
    public static final String FLAVOR = "tvProd";
    public static final String FLAVOR_env = "prod";
    public static final String FLAVOR_eon = "tv";
    public static final String VENDOR_APP_ID_SUFFIX = "";
    public static final int VERSION_CODE = 75;
    public static final String VERSION_NAME = "3.4.4";
    public static final int VOICE_SEARCH_MAX_SILENCE_PERIODS_COUNT = 30;
}
