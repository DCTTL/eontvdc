package com.ug.eon.android.tv.util.filesystem;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/* renamed from: com.ug.eon.android.tv.util.filesystem.FileDownloadApi */
interface FileDownloadApi {
    @GET
    Call<ResponseBody> downloadFile(@Url String str);
}
