package com.ug.eon.android.tv.util.function;

/* renamed from: com.ug.eon.android.tv.util.function.Supplier */
public interface Supplier<T> {
    T get();
}
