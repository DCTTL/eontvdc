package com.ug.eon.android.tv.web;

import android.content.Context;
import android.os.AsyncTask;
import com.ug.eon.android.tv.prefs.PreferenceManager;
import com.ug.eon.android.tv.util.LogUC;
import com.ug.eon.android.tv.util.filesystem.DefaultFileDownloader;
import com.ug.eon.android.tv.util.filesystem.FileDownloader;
import com.ug.eon.android.tv.util.filesystem.ServerAddress;
import java.io.File;

/* renamed from: com.ug.eon.android.tv.web.BatchFileDownloadTask */
public class BatchFileDownloadTask extends AsyncTask<String, Void, Boolean> {
    private static final String TAG = BatchFileDownloadTask.class.getName();
    private Context mContext;
    private String mDestination;
    private PreferenceManager mPreferenceManager;
    private BatchFileDownloadTaskCompletedListener mResultReceiver;
    private ServerAddress mServer;

    public BatchFileDownloadTask(Context context, PreferenceManager preferenceManager, ServerAddress server, String destination, BatchFileDownloadTaskCompletedListener resultReceiver) {
        this.mContext = context;
        this.mPreferenceManager = preferenceManager;
        this.mServer = server;
        this.mDestination = destination;
        this.mResultReceiver = resultReceiver;
    }

    /* access modifiers changed from: protected */
    public Boolean doInBackground(String... paths) {
        LogUC.m39d(TAG, "Task starting");
        FileDownloader downloader = new DefaultFileDownloader(this.mContext, this.mPreferenceManager, this.mServer);
        boolean result = true;
        for (String path : paths) {
            if (isCancelled()) {
                return Boolean.valueOf(false);
            }
            String fullPath = this.mContext.getCacheDir().getPath() + this.mDestination + path;
            if (!new File(fullPath).isFile()) {
                LogUC.m42v(TAG, "Downloading file: " + fullPath);
                result &= downloader.downloadFile(path, this.mDestination);
            } else {
                LogUC.m42v(TAG, "File exists, skipping download: " + fullPath);
            }
        }
        LogUC.m39d(TAG, "Task finished");
        return Boolean.valueOf(result);
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        this.mResultReceiver.onDownloadTaskCompleted(aBoolean.booleanValue());
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        super.onCancelled();
        LogUC.m39d(TAG, "Task has been canceled!");
    }
}
