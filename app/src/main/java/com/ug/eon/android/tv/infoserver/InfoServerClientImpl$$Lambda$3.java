package com.ug.eon.android.tv.infoserver;

import java.util.function.Supplier;

/* renamed from: com.ug.eon.android.tv.infoserver.InfoServerClientImpl$$Lambda$3 */
final /* synthetic */ class InfoServerClientImpl$$Lambda$3 implements Supplier {
    static final Supplier $instance = new InfoServerClientImpl$$Lambda$3();

    private InfoServerClientImpl$$Lambda$3() {
    }

    public Object get() {
        return InfoServerClientImpl.lambda$getWatchNextItems$3$InfoServerClientImpl();
    }
}
