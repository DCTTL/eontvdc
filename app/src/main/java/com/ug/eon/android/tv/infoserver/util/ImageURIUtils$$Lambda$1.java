package com.ug.eon.android.tv.infoserver.util;

import com.ug.eon.android.tv.prefs.ServerPrefs;
import com.ug.eon.android.tv.util.Optional.Function;

/* renamed from: com.ug.eon.android.tv.infoserver.util.ImageURIUtils$$Lambda$1 */
final /* synthetic */ class ImageURIUtils$$Lambda$1 implements Function {
    private final String arg$1;

    ImageURIUtils$$Lambda$1(String str) {
        this.arg$1 = str;
    }

    public Object apply(Object obj) {
        return ImageURIUtils.lambda$getImageURI$1$ImageURIUtils(this.arg$1, (ServerPrefs) obj);
    }
}
