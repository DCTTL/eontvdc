package com.ug.eon.android.tv.exoplayer;

import com.ug.eon.android.tv.searchintegration.Consumer;

/* renamed from: com.ug.eon.android.tv.exoplayer.UcDrmSessionManager$$Lambda$1 */
final /* synthetic */ class UcDrmSessionManager$$Lambda$1 implements Consumer {
    private final UcDrmSessionManager arg$1;
    private final byte[] arg$2;

    UcDrmSessionManager$$Lambda$1(UcDrmSessionManager ucDrmSessionManager, byte[] bArr) {
        this.arg$1 = ucDrmSessionManager;
        this.arg$2 = bArr;
    }

    public void accept(Object obj) {
        this.arg$1.lambda$acquireSession$1$UcDrmSessionManager(this.arg$2, (byte[]) obj);
    }
}
