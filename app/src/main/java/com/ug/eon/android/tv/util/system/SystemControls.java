package com.ug.eon.android.tv.util.system;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.content.Intent;
import android.os.PowerManager;
import android.text.TextUtils;
import com.ug.eon.android.tv.BuildConfigDev;
import com.ug.eon.android.tv.EonApplication;
import com.ug.eon.android.tv.util.LogUC;
import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import ch.qos.logback.core.joran.action.Action;

/* renamed from: com.ug.eon.android.tv.util.system.SystemControls */
public class SystemControls {
    private static final String DEFAULT_TIME_ZONE_BELGRADE = "Europe/Belgrade";
    private static final String TAG = SystemControls.class.getName();

    private SystemControls() {
    }

    public static void rebootDevice() {
        LogUC.m41i(TAG, "Rebooting device");
        ((PowerManager) EonApplication.getGlobalContext().getSystemService(PowerManager.class)).reboot(null);
    }

    public static void clearAppData() {
        LogUC.m41i(TAG, "Clearing Eon app Data");
        ((ActivityManager) EonApplication.getGlobalContext().getSystemService(ActivityManager.class)).clearApplicationUserData();
    }

    public static void clearAppCache() {
        LogUC.m41i(TAG, "Clearing Eon app Cache");
        File cacheDir = EonApplication.getGlobalContext().getCacheDir();
        File externalCacheDir = EonApplication.getGlobalContext().getExternalCacheDir();
        try {
            FileUtils.deleteDirectory(cacheDir);
            if (externalCacheDir != null) {
                FileUtils.deleteDirectory(externalCacheDir);
            }
        } catch (IOException e) {
            LogUC.m43w(TAG, "Could not remove cache folders");
        }
    }

    public static void restartEonApp() {
        LogUC.m41i(TAG, "Restarting Eon app");
        Intent intent = new Intent("com.ug.eon.android.tv.specialkeys");
        intent.setPackage(BuildConfigDev.APPLICATION_ID);
        intent.putExtra(Action.KEY_ATTRIBUTE, "red");
        EonApplication.getGlobalContext().sendBroadcast(intent);
    }

    public static void setTimeZone(String timeZone) {
        if (TextUtils.isEmpty(timeZone)) {
            LogUC.m41i(TAG, "Invalid time zone, using default (Belgrade)");
            timeZone = DEFAULT_TIME_ZONE_BELGRADE;
        }
        LogUC.m41i(TAG, String.format("Setting time zone: %s", new Object[]{timeZone}));
        try {
            ((AlarmManager) EonApplication.getGlobalContext().getSystemService(AlarmManager.class)).setTimeZone(timeZone);
        } catch (IllegalArgumentException e) {
            LogUC.m43w(TAG, String.format("Illegal argument for time zone: %s", new Object[]{timeZone}));
        }
    }

    public static void setTime(long time) {
        LogUC.m39d(TAG, String.format("Setting time: %s", new Object[]{Long.valueOf(time)}));
        ((AlarmManager) EonApplication.getGlobalContext().getSystemService(AlarmManager.class)).setTime(time);
    }
}
