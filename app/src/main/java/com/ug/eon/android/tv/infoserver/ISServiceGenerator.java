package com.ug.eon.android.tv.infoserver;

import com.ug.eon.android.tv.prefs.PreferenceManager;
import com.ug.eon.android.tv.prefs.ServerPrefs;
import com.ug.eon.android.tv.util.Optional;
import okhttp3.OkHttpClient.Builder;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/* renamed from: com.ug.eon.android.tv.infoserver.ISServiceGenerator */
class ISServiceGenerator {
    private ISServiceGenerator() {
    }

    static Optional<ISApi> createISClient(AuthInterceptor authInterceptor, PreferenceManager preferenceManager) {
        Optional<String> apiBaseUrl = generateApiBaseUrl(preferenceManager);
        if (!apiBaseUrl.isPresent()) {
            return Optional.empty();
        }
        Builder httpClient = new Builder();
        httpClient.addInterceptor(authInterceptor);
        return Optional.m44of(new Retrofit.Builder().baseUrl((String) apiBaseUrl.get()).addConverterFactory(GsonConverterFactory.create()).client(httpClient.build()).build().create(ISApi.class));
    }

    private static Optional<String> generateApiBaseUrl(PreferenceManager preferenceManager) {
        return preferenceManager.getServerPrefs().map(ISServiceGenerator$$Lambda$0.$instance);
    }

    static final /* synthetic */ String lambda$generateApiBaseUrl$0$ISServiceGenerator(ServerPrefs servers) {
        if (servers.getInfoServerBaseUrl() == null || servers.getInfoServerBaseUrl().isEmpty() || servers.getApiVersion() == null || servers.getApiVersion().isEmpty()) {
            return null;
        }
        return servers.getInfoServerBaseUrl() + "/" + servers.getApiVersion() + "/";
    }
}
