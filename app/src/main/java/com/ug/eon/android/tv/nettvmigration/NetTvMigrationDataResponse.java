package com.ug.eon.android.tv.nettvmigration;

import com.ug.eon.android.tv.util.LogUC;

/* renamed from: com.ug.eon.android.tv.nettvmigration.NetTvMigrationDataResponse */
public class NetTvMigrationDataResponse {
    private static final NetTvMigrationDataResponse EMPTY_MIGRATION_DATA = new NetTvMigrationDataResponse(false, null);
    private static final String TAG = NetTvMigrationDataResponse.class.getName();
    private final boolean hasMigrationData;
    private final MigrationData migrationData;

    private NetTvMigrationDataResponse(boolean hasMigrationData2, MigrationData migrationData2) {
        this.migrationData = migrationData2;
        this.hasMigrationData = hasMigrationData2;
    }

    public static NetTvMigrationDataResponse empty() {
        return EMPTY_MIGRATION_DATA;
    }

    public static NetTvMigrationDataResponse from(MigrationData migrationData2) {
        if (migrationData2 != null) {
            return new NetTvMigrationDataResponse(true, migrationData2);
        }
        LogUC.m40e(TAG, "Cannot create NetTvMigrationDataResponse with empty migration info. Creating empty NetTvMigrationDataResponse.");
        return EMPTY_MIGRATION_DATA;
    }

    public boolean isHasMigrationData() {
        return this.hasMigrationData;
    }

    public MigrationData getMigrationData() {
        return this.migrationData;
    }
}
