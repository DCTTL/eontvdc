package com.ug.eon.android.tv.util.filesystem;

import android.content.Context;
import com.ug.eon.android.tv.prefs.PreferenceManager;
import com.ug.eon.android.tv.util.LogUC;
import java.io.IOException;
import okhttp3.ResponseBody;
import retrofit2.Response;

/* renamed from: com.ug.eon.android.tv.util.filesystem.DefaultFileDownloader */
public class DefaultFileDownloader implements FileDownloader {
    private static final String TAG = DefaultFileDownloader.class.getName();
    private Context mContext;
    private FileDownloadApi mFileDownloadApi;

    public DefaultFileDownloader(Context context, PreferenceManager preferenceManager, ServerAddress server) {
        this.mContext = context;
        this.mFileDownloadApi = FileDownloadServiceGenerator.createFileDownloadClient(preferenceManager, server);
    }

    public boolean downloadFile(String path, String destinationDir) {
        if (path == null || path.isEmpty() || destinationDir == null || destinationDir.isEmpty() || this.mFileDownloadApi == null) {
            return false;
        }
        try {
            Response<ResponseBody> response = this.mFileDownloadApi.downloadFile(path).execute();
            if (response.isSuccessful()) {
                ResponseBody body = (ResponseBody) response.body();
                if (body == null) {
                    return false;
                }
                new DefaultFileReaderWriter(this.mContext).writeFileToCache(destinationDir + path, body.bytes());
                return true;
            }
            LogUC.m43w(TAG, "Server contact failed");
            return false;
        } catch (IOException e) {
            LogUC.m43w(TAG, e.toString());
            return false;
        }
    }
}
