package com.ug.eon.android.tv.exoplayer;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player$EventListener$$CC;
import com.google.android.exoplayer2.Player.EventListener;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.AdaptiveMediaSourceEventListener;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;

/* renamed from: com.ug.eon.android.tv.exoplayer.UcExoListener */
public abstract class UcExoListener implements AdaptiveMediaSourceEventListener, EventListener {
    public void onPlayerError(ExoPlaybackException exoPlaybackException) {
        Player$EventListener$$CC.onPlayerError(this, exoPlaybackException);
    }

    public void onPlayerStateChanged(boolean z, int i) {
        Player$EventListener$$CC.onPlayerStateChanged(this, z, i);
    }

    public void onTimelineChanged(Timeline timeline, Object obj, int i) {
        Player$EventListener$$CC.onTimelineChanged(this, timeline, obj, i);
    }

    public void onPositionDiscontinuity(int reason) {
    }

    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
    }

    public void onSeekProcessed() {
    }

    public void onRepeatModeChanged(int repeatMode) {
    }

    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {
    }

    public void onLoadingChanged(boolean isLoading) {
    }

    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
    }
}
