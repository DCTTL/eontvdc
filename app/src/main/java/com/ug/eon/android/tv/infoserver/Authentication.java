package com.ug.eon.android.tv.infoserver;

import com.ug.eon.android.tv.infoserver.AuthInterceptor.AuthFailListener;
import com.ug.eon.android.tv.util.LogUC;
import com.ug.eon.android.tv.web.AuthFailedHandler;
import java.util.ArrayList;
import java.util.List;

/* renamed from: com.ug.eon.android.tv.infoserver.Authentication */
public class Authentication implements AuthFailListener, AuthHandler {
    private static final String TAG = Authentication.class.getName();
    private AuthFailedHandler authFailedHandler;
    private AuthInterceptor authInterceptor;
    private List<AuthListener> authListenerList = new ArrayList();

    /* renamed from: com.ug.eon.android.tv.infoserver.Authentication$AuthListener */
    public interface AuthListener {
        void onAuthFail();

        void onAuthSuccess();
    }

    public Authentication(InfoServerClient isClient, AuthFailedHandler afh) {
        this.authInterceptor = isClient.getAuthInterceptor();
        this.authFailedHandler = afh;
    }

    public void onAuthFailed() {
        LogUC.m39d("UC_AUTH/" + TAG, "auth failed, notifying listeners");
        for (AuthListener listener : this.authListenerList) {
            listener.onAuthFail();
        }
        this.authFailedHandler.onAuthFailed();
        this.authInterceptor.unregisterListener();
    }

    public void onAuthenticated() {
        LogUC.m39d("UC_AUTH/" + TAG, "client authenticated, notifying listeners");
        this.authInterceptor.registerListener(this);
        for (AuthListener listener : this.authListenerList) {
            listener.onAuthSuccess();
        }
    }

    public void registerListener(AuthListener authListener) {
        this.authListenerList.add(authListener);
    }

    public void unregisterListener(AuthListener authListener) {
        this.authListenerList.remove(authListener);
    }
}
