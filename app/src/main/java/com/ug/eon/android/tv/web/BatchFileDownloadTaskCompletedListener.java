package com.ug.eon.android.tv.web;

/* renamed from: com.ug.eon.android.tv.web.BatchFileDownloadTaskCompletedListener */
public interface BatchFileDownloadTaskCompletedListener {
    void onDownloadTaskCompleted(boolean z);
}
