package com.ug.eon.android.tv.infoserver.entities;

import java.util.List;

/* renamed from: com.ug.eon.android.tv.infoserver.entities.Assets */
public class Assets {
    private List<CuTVAsset> CUTV;

    /* renamed from: TV */
    private List<LiveTVAsset> f33TV;
    private List<VodAsset> VOD;

    public List<LiveTVAsset> getLiveTv() {
        return this.f33TV;
    }

    public List<CuTVAsset> getCuTv() {
        return this.CUTV;
    }

    public List<VodAsset> getVod() {
        return this.VOD;
    }
}
