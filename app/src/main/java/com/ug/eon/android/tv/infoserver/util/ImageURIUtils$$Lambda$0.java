package com.ug.eon.android.tv.infoserver.util;

import com.ug.eon.android.tv.infoserver.entities.Image;
import java.util.function.Predicate;

/* renamed from: com.ug.eon.android.tv.infoserver.util.ImageURIUtils$$Lambda$0 */
final /* synthetic */ class ImageURIUtils$$Lambda$0 implements Predicate {
    private final String arg$1;

    ImageURIUtils$$Lambda$0(String str) {
        this.arg$1 = str;
    }

    public boolean test(Object obj) {
        return ((Image) obj).getSize().equals(this.arg$1);
    }
}
