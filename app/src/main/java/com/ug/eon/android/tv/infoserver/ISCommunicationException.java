package com.ug.eon.android.tv.infoserver;

/* renamed from: com.ug.eon.android.tv.infoserver.ISCommunicationException */
public class ISCommunicationException extends RuntimeException {
    public ISCommunicationException(String message) {
        super(message);
    }

    public ISCommunicationException(String message, Throwable error) {
        super(message, error);
    }
}
