package com.ug.eon.android.tv.web;

/* renamed from: com.ug.eon.android.tv.web.PlayerMode */
public enum PlayerMode {
    DVB_C,
    OTT;

    public static boolean isDvb(PlayerMode playerMode) {
        return playerMode == DVB_C;
    }

    public static boolean isOtt(PlayerMode playerMode) {
        return playerMode == OTT;
    }
}
