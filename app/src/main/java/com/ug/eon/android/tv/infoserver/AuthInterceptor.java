package com.ug.eon.android.tv.infoserver;

import com.ug.eon.android.tv.util.LogUC;
import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.Interceptor.Chain;
import okhttp3.Response;

/* renamed from: com.ug.eon.android.tv.infoserver.AuthInterceptor */
public class AuthInterceptor implements Interceptor {
    private static final String TAG = AuthInterceptor.class.getName();
    private AuthFailListener listener;

    /* renamed from: com.ug.eon.android.tv.infoserver.AuthInterceptor$AuthFailListener */
    public interface AuthFailListener {
        void onAuthFailed();
    }

    /* access modifiers changed from: 0000 */
    public void registerListener(AuthFailListener listener2) {
        this.listener = listener2;
    }

    /* access modifiers changed from: 0000 */
    public void unregisterListener() {
        this.listener = null;
    }

    public Response intercept(Chain chain) throws IOException {
        Response response = chain.proceed(chain.request());
        if (response.code() == 401) {
            LogUC.m43w("UC_AUTH/" + TAG, "received HTTP error 401. notifying..");
            authFailed();
        }
        return response;
    }

    private void authFailed() {
        if (this.listener != null) {
            this.listener.onAuthFailed();
        }
    }
}
