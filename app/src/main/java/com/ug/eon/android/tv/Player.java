package com.ug.eon.android.tv;

import com.ug.eon.android.tv.util.LogUC;
import com.ug.eon.android.tv.web.PlayerInterface;

/* renamed from: com.ug.eon.android.tv.Player */
public final class Player implements PlayerInterface {
    private static final String TAG = Player.class.getName();
    private boolean mDestroyed = false;
    private PlayerInterface mPlayer;

    public Player(PlayerInterface playerInterface) {
        this.mPlayer = playerInterface;
    }

    public void initPlayer() {
        this.mPlayer.initPlayer();
        this.mDestroyed = false;
    }

    public void playVideo(String data, boolean drmProtected) {
        if (this.mDestroyed) {
            LogUC.m39d(TAG, "playVideo, player has been destroyed.");
        } else {
            this.mPlayer.playVideo(data, drmProtected);
        }
    }

    public void playVideo(String url, Double ms, boolean drmProtected) {
        if (this.mDestroyed) {
            LogUC.m39d(TAG, "playVideo, player has been destroyed.");
        } else {
            this.mPlayer.playVideo(url, ms, drmProtected);
        }
    }

    public void seekTo(double ms) {
        if (this.mDestroyed) {
            LogUC.m39d(TAG, "seekTo, player has been destroyed.");
        } else {
            this.mPlayer.seekTo(ms);
        }
    }

    public void resume() {
        if (this.mDestroyed) {
            LogUC.m39d(TAG, "resume, player has been destroyed.");
        } else {
            this.mPlayer.resume();
        }
    }

    public void playPause(boolean playPause) {
        if (this.mDestroyed) {
            LogUC.m39d(TAG, "playPause, player has been destroyed.");
        } else {
            this.mPlayer.playPause(playPause);
        }
    }

    public void stop() {
        if (this.mDestroyed) {
            LogUC.m39d(TAG, "stop, player has been destroyed.");
        } else {
            this.mPlayer.stop();
        }
    }

    public void destroy() {
        if (this.mDestroyed) {
            LogUC.m39d(TAG, "destroy, player has been destroyed.");
            return;
        }
        this.mPlayer.destroy();
        this.mDestroyed = true;
    }

    public void playDvbVideo(int networkId, int streamId, int serviceId) {
        if (this.mDestroyed) {
            LogUC.m39d(TAG, "playDvbVideo, player has been destroyed.");
        } else {
            this.mPlayer.playDvbVideo(networkId, streamId, serviceId);
        }
    }

    public void teletext() {
        if (this.mDestroyed) {
            LogUC.m39d(TAG, "teletext, player has been destroyed.");
        } else {
            this.mPlayer.teletext();
        }
    }
}
