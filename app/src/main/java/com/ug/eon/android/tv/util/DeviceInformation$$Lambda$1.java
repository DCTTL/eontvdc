package com.ug.eon.android.tv.util;

import com.ug.eon.android.tv.prefs.AuthPrefs;
import com.ug.eon.android.tv.util.Optional.Function;

/* renamed from: com.ug.eon.android.tv.util.DeviceInformation$$Lambda$1 */
final /* synthetic */ class DeviceInformation$$Lambda$1 implements Function {
    static final Function $instance = new DeviceInformation$$Lambda$1();

    private DeviceInformation$$Lambda$1() {
    }

    public Object apply(Object obj) {
        return Integer.valueOf(((AuthPrefs) obj).getCommId());
    }
}
