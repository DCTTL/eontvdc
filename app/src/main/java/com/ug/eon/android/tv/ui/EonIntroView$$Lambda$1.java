package com.ug.eon.android.tv.ui;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;

/* renamed from: com.ug.eon.android.tv.ui.EonIntroView$$Lambda$1 */
final /* synthetic */ class EonIntroView$$Lambda$1 implements OnCompletionListener {
    private final EonIntroView arg$1;

    EonIntroView$$Lambda$1(EonIntroView eonIntroView) {
        this.arg$1 = eonIntroView;
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        this.arg$1.lambda$loadVideo$1$EonIntroView(mediaPlayer);
    }
}
