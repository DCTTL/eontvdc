package com.ug.eon.android.tv.exoplayer;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.SurfaceView;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.RenderersFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.drm.DefaultDrmSessionEventListener$$CC;
import com.google.android.exoplayer2.drm.DefaultDrmSessionManager.EventListener;
import com.google.android.exoplayer2.drm.DrmSessionManager;
import com.google.android.exoplayer2.drm.FrameworkMediaCrypto;
import com.google.android.exoplayer2.drm.UnsupportedDrmException;
import com.google.android.exoplayer2.source.BehindLiveWindowException;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MediaSource.MediaPeriodId;
import com.google.android.exoplayer2.source.MediaSourceEventListener.LoadEventInfo;
import com.google.android.exoplayer2.source.MediaSourceEventListener.MediaLoadData;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection.Factory;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.DefaultAllocator;
import com.google.android.exoplayer2.upstream.HttpDataSource.HttpDataSourceException;
import com.google.android.exoplayer2.upstream.HttpDataSource.InvalidResponseCodeException;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.ug.eon.android.tv.util.EventListener;
import com.ug.eon.android.tv.util.LogUC;
import com.ug.eon.android.tv.util.function.Supplier;
import com.ug.eon.android.tv.web.PlayerInterface;
import com.ug.eon.android.tv.web.PlayerInterface$$CC;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/* renamed from: com.ug.eon.android.tv.exoplayer.ExoPlayer */
public class ExoPlayer implements PlayerInterface {
    private static final int BUFFER_FOR_PLAYBACK_AFTER_REBUFFER_MS = 3600;
    private static final int BUFFER_FOR_PLAYBACK_MS = 1400;
    public static final boolean DEBUG = true;
    /* access modifiers changed from: private */
    public static final String TAG = ExoPlayer.class.getName();
    private EventListener drmSessionListener = new EventListener() {
        public void onDrmSessionAcquired() {
            DefaultDrmSessionEventListener$$CC.onDrmSessionAcquired(this);
        }

        public void onDrmSessionReleased() {
            DefaultDrmSessionEventListener$$CC.onDrmSessionReleased(this);
        }

        public void onDrmKeysLoaded() {
        }

        public void onDrmSessionManagerError(Exception e) {
            LogUC.m40e(ExoPlayer.TAG, "drm session manager error : " + e.getMessage());
            ExoPlayer.this.destroy();
            ExoPlayer.this.initPlayer();
        }

        public void onDrmKeysRestored() {
        }

        public void onDrmKeysRemoved() {
        }
    };
    private final Context mContext;
    private final Supplier<DrmInfo> mDrmInfoSupplier;
    private SimpleExoPlayer mExoPlayer;
    private final ExoMediaSource mMediaSource;
    private long mStartZapTime;
    private final SurfaceView mSurfaceView;
    private final TrackSelector mTrackSelector;
    /* access modifiers changed from: private */
    public com.ug.eon.android.tv.util.EventListener mUcEventListener;
    private UcExoListener mUcExoListener = new UcExoListener() {
        public void onDownstreamFormatChanged(int windowIndex, @Nullable MediaPeriodId mediaPeriodId, MediaLoadData mediaLoadData) {
            LogUC.m39d(ExoPlayer.TAG, "onDownstreamFormatChanged: " + (mediaLoadData.trackFormat != null ? mediaLoadData.trackFormat.toString() : "null"));
        }

        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            switch (playbackState) {
                case 1:
                    LogUC.m39d(ExoPlayer.TAG, "onPlayerStateChanged: STATE_IDLE");
                    ExoPlayer.this.mUcEventListener.sendEvent(7);
                    return;
                case 2:
                    LogUC.m39d(ExoPlayer.TAG, "onPlayerStateChanged: STATE_BUFFERING");
                    ExoPlayer.this.mUcEventListener.sendEvent(8);
                    return;
                case 3:
                    LogUC.m39d(ExoPlayer.TAG, "onPlayerStateChanged: STATE_READY");
                    ExoPlayer.this.mUcEventListener.sendEvent(3);
                    return;
                case 4:
                    LogUC.m39d(ExoPlayer.TAG, "onPlayerStateChanged: STATE_ENDED");
                    return;
                default:
                    return;
            }
        }

        public void onLoadError(int windowIndex, @Nullable MediaPeriodId mediaPeriodId, LoadEventInfo loadEventInfo, MediaLoadData mediaLoadData, IOException error, boolean wasCanceled) {
            LogUC.m43w(ExoPlayer.TAG, "ExoPlayer: onLoadError");
            if (error instanceof HttpDataSourceException) {
                HttpDataSourceException httpDataSourceException = (HttpDataSourceException) error;
                LogUC.m40e(ExoPlayer.TAG, "HttpDataSourceException, dataSpec: " + (httpDataSourceException.dataSpec != null ? httpDataSourceException.dataSpec.toString() : "null"));
            }
            if (error instanceof InvalidResponseCodeException) {
                parseHeader(((InvalidResponseCodeException) error).headerFields);
            }
            LogUC.m40e(ExoPlayer.TAG, "onLoadError: " + Log.getStackTraceString(error));
            ExoPlayer.this.mUcEventListener.sendEvent(1);
        }

        public void onReadingStarted(int windowIndex, MediaPeriodId mediaPeriodId) {
        }

        public void onUpstreamDiscarded(int windowIndex, MediaPeriodId mediaPeriodId, MediaLoadData mediaLoadData) {
        }

        public void onPlayerError(ExoPlaybackException error) {
            if (error.getCause() instanceof BehindLiveWindowException) {
                LogUC.m40e(ExoPlayer.TAG, "BehindLiveWindowException 404, There's a gap between the old index and the new one which means we've slipped behind the live window and can't proceed.");
            }
            if (error.type == 0) {
                LogUC.m40e(ExoPlayer.TAG, "The error occurred loading data from a MediaSource.");
            } else if (2 == error.type) {
                LogUC.m40e(ExoPlayer.TAG, "The unexpected error occurred: " + error.getMessage());
                ExoPlayer.this.destroy();
                ExoPlayer.this.initPlayer();
            } else if (1 == error.type) {
                LogUC.m40e(ExoPlayer.TAG, "The renderer error occurred: " + error.getMessage());
            }
            ExoPlayer.this.mUcEventListener.sendEvent(1);
        }

        private void parseHeader(Map<String, List<String>> headers) {
            if (headers != null) {
                JsonObject lastHeader = new JsonObject();
                for (Entry<String, List<String>> entry : headers.entrySet()) {
                    JsonArray jsonArray = new JsonArray();
                    for (String listString : (List) entry.getValue()) {
                        jsonArray.add(listString);
                    }
                    lastHeader.add(entry.getKey() != null ? (String) entry.getKey() : "null", jsonArray);
                }
                LogUC.m40e(ExoPlayer.TAG, "Header: " + lastHeader);
            }
        }

        public void onMediaPeriodCreated(int windowIndex, MediaPeriodId mediaPeriodId) {
        }

        public void onMediaPeriodReleased(int windowIndex, MediaPeriodId mediaPeriodId) {
        }

        public void onLoadStarted(int windowIndex, @Nullable MediaPeriodId mediaPeriodId, LoadEventInfo loadEventInfo, MediaLoadData mediaLoadData) {
        }

        public void onLoadCompleted(int windowIndex, @Nullable MediaPeriodId mediaPeriodId, LoadEventInfo loadEventInfo, MediaLoadData mediaLoadData) {
        }

        public void onLoadCanceled(int windowIndex, @Nullable MediaPeriodId mediaPeriodId, LoadEventInfo loadEventInfo, MediaLoadData mediaLoadData) {
        }
    };

    public void playDvbVideo(int i, int i2, int i3) {
        PlayerInterface$$CC.playDvbVideo(this, i, i2, i3);
    }

    public void teletext() {
        PlayerInterface$$CC.teletext(this);
    }

    public ExoPlayer(Context context, SurfaceView surfaceView, Supplier<DrmInfo> drmSupplier, com.ug.eon.android.tv.util.EventListener eventListener) {
        this.mContext = context;
        this.mSurfaceView = surfaceView;
        this.mUcEventListener = eventListener;
        this.mMediaSource = new ExoMediaSource(this.mContext);
        this.mTrackSelector = new DefaultTrackSelector((Factory) getAdaptiveFactory());
        this.mDrmInfoSupplier = drmSupplier;
        initPlayer();
    }

    public void initPlayer() {
        try {
            this.mExoPlayer = ExoPlayerFactory.newSimpleInstance(this.mContext, (RenderersFactory) new DefaultRenderersFactory(this.mContext), this.mTrackSelector, (LoadControl) getLoadControl(), (DrmSessionManager<FrameworkMediaCrypto>) new UcDrmSessionManager<FrameworkMediaCrypto>(this.mDrmInfoSupplier, this.drmSessionListener, new Handler()));
        } catch (UnsupportedDrmException e) {
            LogUC.m40e(TAG, "unsupported drm, reason: " + e.reason);
        }
        if (this.mExoPlayer != null) {
            this.mExoPlayer.setVideoSurfaceView(this.mSurfaceView);
            this.mExoPlayer.addListener(this.mUcExoListener);
        }
    }

    public void playVideo(String url, boolean drmProtected) {
        LogUC.m39d(TAG, "play cuttv/live: " + url + ", drm: " + drmProtected);
        playStream(url, 0);
    }

    public void playVideo(String url, Double ms, boolean drmProtected) {
        LogUC.m39d(TAG, "play vod: " + url + ", drm: " + drmProtected);
        playStream(url, ms.longValue());
    }

    public void seekTo(double ms) {
        if (this.mExoPlayer != null) {
            this.mExoPlayer.seekTo((long) ms);
        }
    }

    public void resume() {
        playPause(true);
    }

    public void playPause(boolean playPause) {
        LogUC.m39d(TAG, "playPause: " + playPause);
        if (this.mExoPlayer != null) {
            this.mExoPlayer.setPlayWhenReady(playPause);
        }
    }

    public void stop() {
        LogUC.m39d(TAG, "stop");
        if (this.mExoPlayer != null) {
            this.mExoPlayer.stop();
        }
    }

    public void destroy() {
        if (this.mExoPlayer != null) {
            this.mExoPlayer.stop();
            this.mExoPlayer.release();
            this.mExoPlayer = null;
        }
    }

    private MediaSource getMediaSource(String url) {
        if (url.contains("player=dash")) {
            LogUC.m39d(TAG, "MediaSource: DASH");
            return this.mMediaSource.getDashMediaSource(Uri.parse(url), this.mUcExoListener);
        }
        LogUC.m39d(TAG, "MediaSource: HLS");
        return this.mMediaSource.getHlsMediaSource(Uri.parse(url), this.mUcExoListener);
    }

    private void playStream(String url, long ms) {
        if (this.mExoPlayer != null) {
            this.mExoPlayer.prepare(getMediaSource(url));
            this.mExoPlayer.setPlayWhenReady(true);
            if (ms > 0) {
                this.mExoPlayer.seekTo(ms);
            }
        }
    }

    private AdaptiveTrackSelection.Factory getAdaptiveFactory() {
        return new AdaptiveTrackSelection.Factory(ExoMediaSource.BANDWIDTH_METER);
    }

    private DefaultLoadControl getLoadControl() {
        return new DefaultLoadControl(new DefaultAllocator(true, 65536), 15000, DefaultLoadControl.DEFAULT_MAX_BUFFER_MS, BUFFER_FOR_PLAYBACK_MS, 3600, -1, true);
    }
}
