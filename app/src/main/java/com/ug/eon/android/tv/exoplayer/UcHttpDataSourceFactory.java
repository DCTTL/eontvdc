package com.ug.eon.android.tv.exoplayer;

import com.google.android.exoplayer2.upstream.DefaultHttpDataSource;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.upstream.HttpDataSource.BaseFactory;
import com.google.android.exoplayer2.upstream.HttpDataSource.RequestProperties;
import com.google.android.exoplayer2.upstream.TransferListener;

/* renamed from: com.ug.eon.android.tv.exoplayer.UcHttpDataSourceFactory */
public class UcHttpDataSourceFactory extends BaseFactory {
    private static final String PLAYER_AGENT = "exoplayer";
    private final TransferListener listener;

    UcHttpDataSourceFactory(TransferListener listener2) {
        this.listener = listener2;
    }

    /* access modifiers changed from: protected */
    public HttpDataSource createDataSourceInternal(RequestProperties defaultRequestProperties) {
        return createDefaultHttpDataSource(defaultRequestProperties);
    }

    private HttpDataSource createDefaultHttpDataSource(RequestProperties defaultRequestProperties) {
        return new DefaultHttpDataSource(PLAYER_AGENT, null, this.listener, 8000, 8000, false, defaultRequestProperties);
    }
}
