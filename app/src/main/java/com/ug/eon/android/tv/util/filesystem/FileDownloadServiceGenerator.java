package com.ug.eon.android.tv.util.filesystem;

import com.ug.eon.android.tv.prefs.PreferenceManager;
import com.ug.eon.android.tv.prefs.ServerPrefs;
import retrofit2.Retrofit.Builder;
import retrofit2.converter.gson.GsonConverterFactory;

/* renamed from: com.ug.eon.android.tv.util.filesystem.FileDownloadServiceGenerator */
class FileDownloadServiceGenerator {
    private FileDownloadServiceGenerator() {
    }

    static FileDownloadApi createFileDownloadClient(String baseUrl) {
        return (FileDownloadApi) new Builder().baseUrl(baseUrl).addConverterFactory(GsonConverterFactory.create()).build().create(FileDownloadApi.class);
    }

    static FileDownloadApi createFileDownloadClient(PreferenceManager preferenceManager, ServerAddress server) {
        String serverPath = null;
        switch (server) {
            case IMAGE_SERVER:
                serverPath = (String) preferenceManager.getServerPrefs().map(FileDownloadServiceGenerator$$Lambda$0.$instance).get();
                break;
            case STATIC_SERVER:
                serverPath = (String) preferenceManager.getServerPrefs().map(FileDownloadServiceGenerator$$Lambda$1.$instance).get();
                break;
        }
        if (serverPath != null) {
            return createFileDownloadClient(serverPath);
        }
        return null;
    }

    static final /* synthetic */ String lambda$createFileDownloadClient$0$FileDownloadServiceGenerator(ServerPrefs servers) {
        if (servers.getImageServerUrl() == null || servers.getImageServerUrl().isEmpty()) {
            return null;
        }
        return servers.getImageServerUrl() + "/";
    }

    static final /* synthetic */ String lambda$createFileDownloadClient$1$FileDownloadServiceGenerator(ServerPrefs servers) {
        if (servers.getStaticServer() == null || servers.getStaticServer().isEmpty()) {
            return null;
        }
        return servers.getStaticServer() + "/";
    }
}
