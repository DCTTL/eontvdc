package com.ug.eon.android.tv.web;

import android.app.Activity;
import android.arch.lifecycle.LifecycleObserver;
import android.content.Context;
import android.content.SharedPreferences.Editor;
import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
 import android.util.DisplayMetrics;
import android.view.SurfaceView;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;

import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;

import com.google.android.exoplayer2.util.MimeTypes;
import com.google.gson.JsonObject;
import com.ug.eon.android.tv.BuildConfigDev;
import com.ug.eon.android.tv.R;
import com.ug.eon.android.tv.Chameleon;
import com.ug.eon.android.tv.ChameleonView;
import com.ug.eon.android.tv.NetworkCheck.OnNetworkChange;
import com.ug.eon.android.tv.PlayerFactory;
import com.ug.eon.android.tv.PlayerType;
import com.ug.eon.android.tv.analytics.AnalyticsManager;
import com.ug.eon.android.tv.common.InstanceUniqueID;
import com.ug.eon.android.tv.infoserver.AuthHandler;
import com.ug.eon.android.tv.prefs.PreferenceFile;
import com.ug.eon.android.tv.prefs.PreferenceManager;
import com.ug.eon.android.tv.prefs.PreferenceManagerImpl;
import com.ug.eon.android.tv.prefs.SharedPrefsProviderImpl;
import com.ug.eon.android.tv.searchintegration.EonSpeechRecognition;
import com.ug.eon.android.tv.util.DeviceInformation;
import com.ug.eon.android.tv.util.EventListener;
import com.ug.eon.android.tv.util.LogUC;
import com.ug.eon.android.tv.util.Optional;
import com.ug.eon.android.tv.util.filesystem.DefaultFileReaderWriter;
import com.ug.eon.android.tv.util.filesystem.ServerAddress;
import java.io.IOException;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.ug.eon.android.tv.web.UcWebInterface */
public class UcWebInterface extends PlayerControls implements EventListener, OnNetworkChange, AuthFailedHandler, LifecycleObserver {
    private static final String JS_NAMESPACE = "ANDROMAN";
    private static final String ON_BITRATE_CHANGED_CALLBACK = "onBitrateChanged";
    private static final String ON_NATIVE_KEY_DOWN_CALLBACK = "onNativeKeyDown";
    private static final String ON_NATIVE_KEY_UP_CALLBACK = "onNativeKeyUp";
    private static final String ON_NETWORK_CHECK_CALLBACK = "onNetworkCheck";
    private static final String ON_PLAYER_STATE_CHANGED_CALLBACK = "onPlayerStateChanged";
    private static final String ON_RESUME_PAUSE_ACTIVITY_CALLBACK = "onResumePauseActivity";
    public static final String STB_PROVISIONING_MODE = "stbprovisioning";
    private static final String TAG = UcWebInterface.class.getName();
    private OnAudioFocusChangeListener audioFocusChangeListener = new UcWebInterface$$Lambda$0(this);
    protected AuthHandler authHandler;
    protected Activity mActivity;
    private AnalyticsManager mAnalyticsManager;
    protected Context mAppContext;
    private PlayerAudioFocus mAudioFocus;
    private JSONArray mCallbackFunctions;
    protected Chameleon mChameleon;
    private EonSpeechRecognition mEonSpeechRecognition;
    private InstanceUniqueID mInstanceId;
    protected PreferenceManager mPreferenceManager;
    private StartupParameters mStartupParams;
    private boolean onBitrateChanged = false;
    private boolean onNativeKeyDown = false;
    private boolean onNativeKeyUp = false;
    private boolean onNetworkCheck = false;
    private boolean onPlayerStateChanged = false;
    private boolean onResumePauseActivity = false;

    /* access modifiers changed from: private */
    /* renamed from: onSearchFinalResult */
    public void bridge$lambda$0$UcWebInterface(String result) {
        if (result != null) {
            this.mChameleon.onFinalVoiceResult(result);
        } else {
            this.mChameleon.onCancelVoiceSearch();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: onSearchPartialResult */
    public void bridge$lambda$1$UcWebInterface(String result) {
        if (result != null) {
            this.mChameleon.onPartialVoiceResult(result);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: onSearchError */
    public void bridge$lambda$2$UcWebInterface(Integer error) {
        LogUC.m39d(TAG, "onCancelVoiceSearch");
        if (error.intValue() != 5) {
            this.mEonSpeechRecognition.stopListening();
            this.mChameleon.onCancelVoiceSearch();
        }
    }

    public void setAuthHandler(AuthHandler ah) {
        this.authHandler = ah;
    }

    public UcWebInterface(Activity activity, PreferenceManager preferenceManager, AnalyticsManager analyticsManager, StartupParameters params) {
        LogUC.m39d(TAG, "UcWebInterface created!");
        this.mActivity = activity;
        this.mAppContext = this.mActivity.getApplicationContext();
        this.mCallbackFunctions = new JSONArray();
        this.mPreferenceManager = preferenceManager;
        this.mInstanceId = new InstanceUniqueID(this.mActivity);
        if (params == null) {
            params = new StartupParameters();
        }
        this.mStartupParams = params;
        this.mAnalyticsManager = analyticsManager;
        DisplayMetrics dm = new DisplayMetrics();
        this.mActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        this.mChameleon = new ChameleonView((WebView) this.mActivity.findViewById(R.C0745id.main_web_view));
        this.mChameleon.loadChameleon(this.mStartupParams);
        this.mChameleon.addInterface(this, JS_NAMESPACE);
        this.mChameleon.setUpScale(width);
        this.mChameleon.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
        this.mEonSpeechRecognition = new EonSpeechRecognition(this.mActivity, new UcWebInterface$$Lambda$1(this), new UcWebInterface$$Lambda$2(this), new UcWebInterface$$Lambda$3(this), this.mStartupParams.getRecordAudioPermissionCheck(), this.mPreferenceManager);
        this.mAudioFocus = new PlayerAudioFocus((AudioManager) this.mActivity.getSystemService(MimeTypes.BASE_TYPE_AUDIO), this.audioFocusChangeListener);
        this.mAudioFocus.requestAudioFocus();
    }

    /* access modifiers changed from: protected */
    public Chameleon getChameleon() {
        return this.mChameleon;
    }

    private static String getMACAddress(String interfaceName) {
        try {
            for (NetworkInterface intf : Collections.list(NetworkInterface.getNetworkInterfaces())) {
                if (interfaceName != null) {
                    if (intf.getName().equalsIgnoreCase(interfaceName)) {
                    }
                }
                byte[] mac = intf.getHardwareAddress();
                if (mac == null) {
                    return "";
                }
                StringBuilder buf = new StringBuilder();
                for (byte macByte : mac) {
                    buf.append(String.format("%02X:", new Object[]{Byte.valueOf(macByte)}));
                }
                if (buf.length() > 0) {
                    buf.deleteCharAt(buf.length() - 1);
                }
                return buf.toString();
            }
        } catch (Exception e) {
            LogUC.m40e(TAG, "getMACAddress exception");
        }
        return "";
    }

    @JavascriptInterface
    public void startVoiceSearch() {
        this.mActivity.runOnUiThread(new UcWebInterface$$Lambda$4(this));
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void lambda$startVoiceSearch$0$UcWebInterface() {
        this.mEonSpeechRecognition.startListening();
    }

    @JavascriptInterface
    public void cancelVoiceSearch() {
        this.mActivity.runOnUiThread(new UcWebInterface$$Lambda$5(this));
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void lambda$cancelVoiceSearch$1$UcWebInterface() {
        this.mEonSpeechRecognition.stopListening();
    }

    @JavascriptInterface
    private static String execCmd(String cmd) throws IOException {
        return DeviceInformation.execCmd(cmd);
    }

    @JavascriptInterface
    public void initHal(String callbacks) {
        LogUC.m39d(TAG, "initHal " + callbacks);
        analyzeClbks(callbacks);
        try {
            this.mCallbackFunctions = new JSONArray(callbacks);
        } catch (Exception e) {
            LogUC.m40e(TAG, "error to init hal " + e.getMessage());
        }
    }

    private void analyzeClbks(String callbacks) {
        try {
            JSONArray array = new JSONArray(callbacks);
            for (int i = 0; i < array.length(); i++) {
                LogUC.m39d("Ananlyzing", array.get(i).toString());
                String obj = array.get(i).toString();
                char c = 65535;
                switch (obj.hashCode()) {
                    case -2083632092:
                        if (obj.equals(ON_NATIVE_KEY_UP_CALLBACK)) {
                            c = 1;
                            break;
                        }
                        break;
                    case -1917197754:
                        if (obj.equals(ON_BITRATE_CHANGED_CALLBACK)) {
                            c = 5;
                            break;
                        }
                        break;
                    case -945785181:
                        if (obj.equals(ON_PLAYER_STATE_CHANGED_CALLBACK)) {
                            c = 4;
                            break;
                        }
                        break;
                    case -916184085:
                        if (obj.equals(ON_NATIVE_KEY_DOWN_CALLBACK)) {
                            c = 2;
                            break;
                        }
                        break;
                    case 1921602297:
                        if (obj.equals(ON_NETWORK_CHECK_CALLBACK)) {
                            c = 3;
                            break;
                        }
                        break;
                    case 2077185945:
                        if (obj.equals(ON_RESUME_PAUSE_ACTIVITY_CALLBACK)) {
                            c = 0;
                            break;
                        }
                        break;
                }
                switch (c) {
                    case 0:
                        this.onResumePauseActivity = true;
                        break;
                    case 1:
                        this.onNativeKeyUp = true;
                        break;
                    case 2:
                        this.onNativeKeyDown = true;
                        break;
                    case 3:
                        this.onNetworkCheck = true;
                        break;
                    case 4:
                        this.onPlayerStateChanged = true;
                        break;
                    case 5:
                        this.onBitrateChanged = true;
                        break;
                }
            }
        } catch (JSONException e) {
            LogUC.m40e(TAG, "error to parse json: " + e.getMessage());
        }
        LogUC.m39d(TAG, ON_NATIVE_KEY_UP_CALLBACK + String.valueOf(this.onNativeKeyUp));
        LogUC.m39d(TAG, ON_NATIVE_KEY_DOWN_CALLBACK + String.valueOf(this.onNativeKeyDown));
    }

    public void reloadState(boolean value, boolean shouldPlayLastViewedChannel) {
        LogUC.m39d(TAG, "ANDROMAN refresh, value = " + value + ", shouldPlayLastViewedChannel = " + shouldPlayLastViewedChannel);
        if (this.onResumePauseActivity) {
            this.mChameleon.onResumePauseActivity(value, shouldPlayLastViewedChannel);
        }
        if (value) {
            this.mAudioFocus.requestAudioFocus();
        } else {
            this.mAudioFocus.abandonAudioFocus();
        }
    }

    public boolean dispatchNativeKey(int keyCode, int action) {
        if (action == 1) {
            if (this.onNativeKeyUp) {
                this.mChameleon.onNativeKeyUp(keyCode);
            }
            return this.onNativeKeyUp;
        } else if (action != 0) {
            return false;
        } else {
            if (this.onNativeKeyDown) {
                this.mChameleon.onNativeKeyDown(keyCode);
            }
            return this.onNativeKeyDown;
        }
    }

    public void doDeepLink(@NonNull DeepLink deepLink, String data) {
        switch (deepLink) {
            case WATCH_NEXT:
                this.mChameleon.changeRoute("PlayerTv", DeepLinkContent.createFrom(data).getChannelId());
                return;
            case DETAIL:
                DeepLinkContent detailContent = DeepLinkContent.createFrom(data);
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty(TtmlNode.ATTR_ID, (Number) Integer.valueOf(detailContent.getId()));
                jsonObject.addProperty("channelId", (Number) Integer.valueOf(detailContent.getChannelId()));
                this.mChameleon.linkToContent(detailContent.getType(), jsonObject);
                return;
            default:
                this.mChameleon.changeRoute(deepLink.getRoute());
                return;
        }
    }

    public void dispatchNetworkCheck(boolean value) {
        if (this.onNetworkCheck) {
            this.mChameleon.onNetworkCheck(value);
        }
    }

    public void onConnectionChange(boolean result) {
        LogUC.m39d(TAG, "onConnectionChange " + result);
        if (this.onNetworkCheck) {
            this.mChameleon.onNetworkCheck(result);
        }
    }

    public void onBitrateChange(int bitrate) {
        LogUC.m39d("onBitrateChange ", bitrate + "");
        int finalEvent = bitrate;
        if (this.onBitrateChanged) {
            this.mActivity.runOnUiThread(new UcWebInterface$$Lambda$6(this, finalEvent));
        }
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void lambda$onBitrateChange$2$UcWebInterface(int finalEvent) {
        this.mChameleon.onBitrateChanged(finalEvent);
    }

    @JavascriptInterface
    public String getWrapperVersion() {
        return DeviceInformation.getWrapperVersion(this.mActivity);
    }

    @JavascriptInterface
    public void setAppStatus(String currentStatus) {
        if (!this.mStartupParams.getStartupAction().isEmpty() && currentStatus.equals("loaded")) {
            this.mActivity.runOnUiThread(new UcWebInterface$$Lambda$7(this));
        }
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void lambda$setAppStatus$3$UcWebInterface() {
        LogUC.m41i(TAG, "calling doDeepLink(), startup action: " + this.mStartupParams.getStartupAction());
        DeepLink deepLink = DeepLink.getDeepLink(this.mStartupParams.getStartupAction());
        if (deepLink != null) {
            doDeepLink(deepLink, this.mStartupParams.getStartupActionData());
        }
    }

    @JavascriptInterface
    public void onEnterExitZap(boolean value) {
        LogUC.m39d(TAG, "Web Interface surfice " + value);
    }

    @JavascriptInterface
    public void appLoaded(boolean value) {
        LogUC.m39d(TAG, "App loaded event " + value);
    }

    @JavascriptInterface
    public String getMac() {
        return DeviceInformation.getMac(this.mActivity);
    }

    @JavascriptInterface
    public String getMACNew() {
        return getMACAddress("eth0");
    }

    @JavascriptInterface
    public String getMACNewWIFI() {
        return getMACAddress("wlan0");
    }

    @JavascriptInterface
    public String getSerial() {
        if (getDeviceType().equals("stb")) {
            return getDeviceSerial();
        }
        return this.mInstanceId.getId();
    }

    @JavascriptInterface
    public String getNetworkInfo() {
        DhcpInfo dhcpInfo = ((WifiManager) this.mAppContext.getSystemService(Context.WIFI_SERVICE)).getDhcpInfo();
        String dns1 = "DNS 1: " + dhcpInfo.dns1;
        String dns2 = "DNS 2: " + dhcpInfo.dns2;
        String gateway = "Default Gateway: " + dhcpInfo.gateway;
        String ipAddress = "IP Address: " + dhcpInfo.ipAddress;
        String leaseDuration = "Lease Time: " + dhcpInfo.leaseDuration;
        String netmask = "Subnet Mask: " + dhcpInfo.netmask;
        String serverAddress = "Server IP: " + dhcpInfo.serverAddress;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("DNS1", dns1);
            jsonObject.put("DNS2", dns2);
            jsonObject.put("Default Gateway", gateway);
            jsonObject.put("IP Address:", ipAddress);
            jsonObject.put("Lease Time:", leaseDuration);
            jsonObject.put("Subnet Mask:", netmask);
            jsonObject.put("Server IP:", serverAddress);
        } catch (JSONException e) {
            LogUC.m40e(TAG, "error to parse json " + e.getMessage());
        }
        return jsonObject.toString();
    }

    @JavascriptInterface
    public String getLocalIpAddress() {
        String localIP = "";
        try {
            return execCmd("getprop dhcp.eth0.ipaddress");
        } catch (Exception err) {
            LogUC.m40e(TAG, "error getting localIP " + err.toString());
            return localIP;
        }
    }

    @JavascriptInterface
    public String getSubnetMask() {
        String mask = "";
        try {
            return execCmd("getprop dhcp.eth0.mask");
        } catch (Exception err) {
            LogUC.m40e(TAG, "error getting mask " + err.toString());
            return mask;
        }
    }

    @JavascriptInterface
    public String getPrimaryDNS() {
        String primaryDNS = "";
        try {
            return execCmd("getprop dhcp.eth0.dns1");
        } catch (Exception err) {
            LogUC.m40e(TAG, "error primaryDNS " + err.toString());
            return primaryDNS;
        }
    }

    @JavascriptInterface
    public String getSecondaryDns() {
        String secondaryDNS = "";
        try {
            return execCmd("getprop dhcp.eth0.dns2");
        } catch (Exception err) {
            LogUC.m40e(TAG, "error secondaryDNS " + err.toString());
            return secondaryDNS;
        }
    }

    @JavascriptInterface
    public String getGateway() {
        String gateway = "";
        try {
            return execCmd("getprop dhcp.eth0.gateway");
        } catch (Exception err) {
            LogUC.m40e(TAG, "error getting gateway " + err.toString());
            return gateway;
        }
    }

    @JavascriptInterface
    public String isItOnLAN() {
        String lanOn = "";
        try {
            return String.valueOf(execCmd("getprop dhcp.eth0.result"));
        } catch (Exception err) {
            LogUC.m40e("error LAN", err.toString());
            LogUC.m39d(TAG, "final lanOn = " + lanOn);
            return lanOn;
        }
    }

    @JavascriptInterface
    public String isItOnWiFi() {
        String wifiOn = "";
        try {
            return String.valueOf(execCmd("getprop dhcp.wlan0.result"));
        } catch (Exception err) {
            LogUC.m40e(TAG, "error LAN" + err.toString());
            LogUC.m39d(TAG, "final wifiOn = " + wifiOn);
            return wifiOn;
        }
    }

    @JavascriptInterface
    public String getLocalIpAddressWiFi() {
        String localIPWiFi = "";
        try {
            return execCmd("getprop dhcp.wlan0.ipaddress");
        } catch (Exception err) {
            LogUC.m40e(TAG, "error localIPWiFi " + err.toString());
            return localIPWiFi;
        }
    }

    @JavascriptInterface
    public String getSubnetMaskWiFi() {
        String subnetWiFi = "";
        try {
            return execCmd("getprop dhcp.wlan0.mask");
        } catch (Exception err) {
            LogUC.m40e(TAG, "error localIPWiFi " + err.toString());
            return subnetWiFi;
        }
    }

    @JavascriptInterface
    public String getPrimaryDNSWiFi() {
        String primaryDNSWiFi = "";
        try {
            return execCmd("getprop dhcp.wlan0.dns1");
        } catch (Exception err) {
            LogUC.m40e(TAG, "error PrimaryDNSWiFi " + err.toString());
            return primaryDNSWiFi;
        }
    }

    @JavascriptInterface
    public String getSecondaryDnsWiFi() {
        String secondaryDnsWiFi = "";
        try {
            return execCmd("getprop dhcp.wlan0.dns2");
        } catch (Exception err) {
            LogUC.m40e(TAG, "error SecondaryDnsWiFi " + err.toString());
            return secondaryDnsWiFi;
        }
    }

    @JavascriptInterface
    public String getGatewayWiFi() {
        String gatewayWiFi = "";
        try {
            return execCmd("getprop dhcp.wlan0.gateway");
        } catch (Exception err) {
            LogUC.m40e(TAG, "error GatewayWiFi" + err.toString());
            return gatewayWiFi;
        }
    }

    @JavascriptInterface
    public String getSDKVersion() {
        return DeviceInformation.getSDKVersion();
    }

    @JavascriptInterface
    public String getReleaseVersion() {
        return DeviceInformation.getReleaseVersion();
    }

    @JavascriptInterface
    public String getDeviceModel() {
        return DeviceInformation.getDeviceModel();
    }

    @JavascriptInterface
    public String getDeviceManufacturer() {
        String deviceManufacturer = "";
        try {
            return execCmd("getprop ro.product.manufacturer");
        } catch (Exception err) {
            LogUC.m40e(TAG, "err deviceManufacturer " + err.toString());
            return deviceManufacturer;
        }
    }

    @JavascriptInterface
    public String getDeviceModelGroup() {
        return DeviceInformation.getDeviceModelGroup();
    }

    @JavascriptInterface
    public String getWrapperCommitHash() {
        return BuildConfigDev.COMMIT_HASH;
    }

    @JavascriptInterface
    public void exitApp() {
        this.mActivity.setResult(-1);
        this.mActivity.finish();
    }

    @JavascriptInterface
    public String load(String name) {
        LogUC.m42v(TAG, "ANDROMAN load " + name);
        String res = this.mActivity.getSharedPreferences(DeviceInformation.PREFS_NAME, 0).getString(name, "");
        LogUC.m42v(TAG, "ANDROMAN loaded " + res);
        return res;
    }

    @JavascriptInterface
    public void store(String name, String val) {
        LogUC.m42v(TAG, "ANDROMAN store " + name + " " + val);
        Editor editor = this.mActivity.getSharedPreferences(DeviceInformation.PREFS_NAME, 0).edit();
        editor.putString(name, val);
        editor.apply();
        LogUC.m42v(TAG, "ANDROMAN stored " + name);
    }

    @JavascriptInterface
    public void remove(String name) {
        LogUC.m42v(TAG, "ANDROMAN remove " + name);
        Editor editor = this.mActivity.getSharedPreferences(DeviceInformation.PREFS_NAME, 0).edit();
        editor.remove(name);
        editor.apply();
        LogUC.m42v(TAG, "ANDROMAN removed " + name);
    }

    @JavascriptInterface
    public String hasInternet() {
        NetworkInfo activeNetwork = ((ConnectivityManager) this.mActivity.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetwork != null) {
            try {
                if (activeNetwork.getType() == 1) {
                    return "true";
                }
            } catch (Exception err) {
                LogUC.m40e(TAG, "err deviceManufacturer " + err.toString());
                return "false";
            }
        }
        if (activeNetwork == null || activeNetwork.getType() != 9) {
            return "false";
        }
        return "true";
    }

    @JavascriptInterface
    public boolean clearCache() {
        try {
            this.mChameleon.clearCache();
            return true;
        } catch (Exception err) {
            LogUC.m40e(TAG, "error clearing cache " + err.toString());
            return false;
        }
    }

    @JavascriptInterface
    public String getDeviceType() {
        return this.mActivity.getString(R.string.device_type);
    }

    @JavascriptInterface
    public String getDeviceSerial() {
        return DeviceInformation.getDeviceSerial();
    }

    public void sendEvent(int event) {
        LogUC.m39d("CUSTOM EVENT ", event + "");
        int finalEvent = event;
        if (this.mCallbackFunctions != null) {
            this.mActivity.runOnUiThread(new UcWebInterface$$Lambda$8(this, event, finalEvent));
        }
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void lambda$sendEvent$4$UcWebInterface(int event, int finalEvent) {
        LogUC.m39d(TAG, "UI thread, i am the UI thread sendEvent");
        for (int i = 0; i < this.mCallbackFunctions.length(); i++) {
            try {
                if (this.mCallbackFunctions.get(i).toString().equals(ON_PLAYER_STATE_CHANGED_CALLBACK)) {
                    LogUC.m39d(TAG, "sentevent: " + event);
                    this.mChameleon.onPlayerStateChanged(finalEvent);
                }
            } catch (JSONException e) {
                LogUC.m40e(TAG, "error to parse json " + e.getMessage());
            }
        }
    }

    @JavascriptInterface
    public void onAuthenticated() {
        LogUC.m41i("UC_AUTH/" + TAG, "client authenticated. notifying..");
        this.authHandler.onAuthenticated();
    }

    public void onAuthFailed() {
        LogUC.m41i("UC_AUTH/" + TAG, "auth token expired. asking for refresh..");
        this.mActivity.runOnUiThread(new UcWebInterface$$Lambda$9(this));
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void lambda$onAuthFailed$5$UcWebInterface() {
        this.mChameleon.onRefreshToken();
    }

    @JavascriptInterface
    public void setChannelData(String tvChannelList, String radioChannelList) {
        LogUC.m39d(TAG, "set channel data");
        if (tvChannelList == null && radioChannelList == null) {
            LogUC.m39d(TAG, "list channels are empty");
        } else {
            startChannelLogoDownloadTask(tvChannelList, radioChannelList);
        }
    }

    /* access modifiers changed from: protected */
    public void startChannelLogoDownloadTask(String tvChannels, String radioChannels) {
        List<String> channelLogosPaths = extractImagePaths(tvChannels);
        channelLogosPaths.addAll(extractImagePaths(radioChannels));
        new BatchFileDownloadTask(this.mAppContext, new PreferenceManagerImpl(new SharedPrefsProviderImpl(this.mAppContext)), ServerAddress.IMAGE_SERVER, "/images", this.mChameleon).execute(channelLogosPaths.toArray(new String[0]));
    }

    private List<String> extractImagePaths(String channelsJson) {
        List<String> paths = new ArrayList<>();
        try {
            JSONArray channels = new JSONArray(channelsJson);
            for (int i = 0; i < channels.length(); i++) {
                JSONObject channel = channels.getJSONObject(i);
                if (!channel.isNull("images")) {
                    JSONArray images = channel.getJSONArray("images");
                    for (int j = 0; j < images.length(); j++) {
                        JSONObject image = images.getJSONObject(j);
                        if (!image.isNull("size") && !image.isNull("path")) {
                            String size = image.getString("size");
                            if (size.equals("STB_FHD") || size.equals("S")) {
                                paths.add(image.getString("path"));
                            }
                        }
                    }
                }
            }
        } catch (JSONException e) {
            LogUC.m40e(TAG, e.toString());
        }
        return paths;
    }

    @JavascriptInterface
    public void downloadImages(String imagesJson) {
        List<String> paths = new ArrayList<>();
        try {
            JSONArray images = new JSONArray(imagesJson);
            for (int i = 0; i < images.length(); i++) {
                JSONObject image = images.getJSONObject(i);
                if (!image.isNull("path")) {
                    paths.add(image.getString("path"));
                }
            }
        } catch (JSONException e) {
            LogUC.m40e(TAG, e.toString());
        }
        if (paths.isEmpty()) {
            LogUC.m43w(TAG, "List of images empty. Downaload task will not start.");
            return;
        }
        new BatchFileDownloadTask(this.mAppContext, new PreferenceManagerImpl(new SharedPrefsProviderImpl(this.mAppContext)), ServerAddress.IMAGE_SERVER, "/images", this.mChameleon).execute(paths.toArray(new String[0]));
    }

    @JavascriptInterface
    public void saveToLocalCache(String key, String value) {
        if (this.mPreferenceManager != null) {
            LogUC.m42v(TAG, "saveToLocalCache, key: " + key + ", value: " + value);
            this.mPreferenceManager.setValue(PreferenceFile.LOCAL_CACHE, key, value);
        }
    }

    @JavascriptInterface
    public String getFromLocalCache(String key) {
        if (this.mPreferenceManager == null) {
            return "";
        }
        Optional<String> value = this.mPreferenceManager.getValue(PreferenceFile.LOCAL_CACHE, key);
        String result = value.isPresent() ? (String) value.get() : "";
        LogUC.m42v(TAG, "getFromLocalCache, key: " + key + ", result: " + result);
        return result;
    }

    @JavascriptInterface
    public void saveFileToLocalCache(String fileName, String data) {
        LogUC.m42v(TAG, "saveFileToLocalCache: fileName: " + fileName + ", data: " + data);
        new DefaultFileReaderWriter(this.mAppContext).writeFileToCache(fileName, data);
    }

    @JavascriptInterface
    public String getFileFromLocalCache(String fileName) {
        String result = new DefaultFileReaderWriter(this.mAppContext).readFile(fileName);
        LogUC.m42v(TAG, "getFileFromLocalCache: fileName: " + fileName + ", result: " + result);
        return result;
    }

    @JavascriptInterface
    public String getSvgImageContent(String fileName) {
        return getFileFromLocalCache(String.format("/images%s", new Object[]{fileName}));
    }

    @JavascriptInterface
    public String getImageCacheLocation() {
        return this.mAppContext.getCacheDir().getPath() + "/images";
    }

    @JavascriptInterface
    public void firebaseLogEvent(String eventName, String eventParams) {
        this.mAnalyticsManager.logEvent(eventName, eventParams);
    }

    @JavascriptInterface
    public void test() {
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void lambda$new$6$UcWebInterface(int focusChange) {
        if (focusChange == 1) {
            LogUC.m39d(TAG, "audio focus gain");
        } else if (focusChange == -1) {
            pause();
        }
    }

    public PlayerInterface getPlayerInstance(PlayerType playerType) {
        return PlayerFactory.playerInstance(this.mActivity, playerType, (SurfaceView) this.mActivity.findViewById(R.C0745id.main_surface_view), this.mPreferenceManager, this);
    }
}
