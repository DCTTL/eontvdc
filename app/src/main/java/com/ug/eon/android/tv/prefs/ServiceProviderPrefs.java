package com.ug.eon.android.tv.prefs;

/* renamed from: com.ug.eon.android.tv.prefs.ServiceProviderPrefs */
public class ServiceProviderPrefs {
    private String certificateUrlFairplay;

    /* renamed from: id */
    private int f36id;
    private String identifier;
    private String licenseServerUrlFairplay;
    private String licenseServerUrlPlayready;
    private String licenseServerUrlWidewine;
    private String name;
    private String supportPhoneNumber;
    private String supportWebAddress;

    public int getId() {
        return this.f36id;
    }

    public void setId(int id) {
        this.f36id = id;
    }

    public String getIdentifier() {
        return this.identifier;
    }

    public void setIdentifier(String identifier2) {
        this.identifier = identifier2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getCertificateUrlFairplay() {
        return this.certificateUrlFairplay;
    }

    public void setCertificateUrlFairplay(String certificateUrlFairplay2) {
        this.certificateUrlFairplay = certificateUrlFairplay2;
    }

    public String getLicenseServerUrlFairplay() {
        return this.licenseServerUrlFairplay;
    }

    public void setLicenseServerUrlFairplay(String licenseServerUrlFairplay2) {
        this.licenseServerUrlFairplay = licenseServerUrlFairplay2;
    }

    public String getLicenseServerUrlPlayready() {
        return this.licenseServerUrlPlayready;
    }

    public void setLicenseServerUrlPlayready(String licenseServerUrlPlayready2) {
        this.licenseServerUrlPlayready = licenseServerUrlPlayready2;
    }

    public String getLicenseServerUrlWidewine() {
        return this.licenseServerUrlWidewine;
    }

    public void setLicenseServerUrlWidewine(String licenseServerUrlWidewine2) {
        this.licenseServerUrlWidewine = licenseServerUrlWidewine2;
    }

    public String getSupportPhoneNumber() {
        return this.supportPhoneNumber;
    }

    public void setSupportPhoneNumber(String supportPhoneNumber2) {
        this.supportPhoneNumber = supportPhoneNumber2;
    }

    public String getSupportWebAddress() {
        return this.supportWebAddress;
    }

    public void setSupportWebAddress(String supportWebAddress2) {
        this.supportWebAddress = supportWebAddress2;
    }
}
