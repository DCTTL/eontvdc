package com.ug.eon.android.tv.config;

/* renamed from: com.ug.eon.android.tv.config.ConfigurationManager */
public interface ConfigurationManager {
    boolean isFeatureEnabled(EonFeature eonFeature);
}
