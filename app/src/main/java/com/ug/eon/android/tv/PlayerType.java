package com.ug.eon.android.tv;

import com.ug.eon.android.tv.util.LogUC;

/* renamed from: com.ug.eon.android.tv.PlayerType */
public enum PlayerType {
    EXOPLAYER;

    public static PlayerType getPlayerType(String value) {
        PlayerType[] values;
        for (PlayerType playerType : values()) {
            if (playerType.name().equalsIgnoreCase(value)) {
                return playerType;
            }
        }
        LogUC.m43w("PlayerType", "Invalid playerType request " + value);
        return EXOPLAYER;
    }
}
