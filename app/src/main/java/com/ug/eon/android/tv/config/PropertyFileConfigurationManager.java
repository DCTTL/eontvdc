package com.ug.eon.android.tv.config;

import android.content.Context;
import com.ug.eon.android.tv.util.LogUC;
import com.ug.eon.android.tv.util.system.SystemInfo;
import java.util.Properties;

/* renamed from: com.ug.eon.android.tv.config.PropertyFileConfigurationManager */
public class PropertyFileConfigurationManager implements ConfigurationManager {
    private static final String FEATURES_PROPERTY_FILE = "config/features.properties";
    private static final String TAG = PropertyFileConfigurationManager.class.getName();
    public static final String WATCH_NEXT_MIN_API = "watchNextMinAPI";
    public static final String WATCH_NEXT_PROPERTY = "watchNext";
    private Properties properties;

    public PropertyFileConfigurationManager(Context context, PropertyFileProvider propertyFileProvider) {
        this.properties = propertyFileProvider.getProperties(context, FEATURES_PROPERTY_FILE);
    }

    public boolean isFeatureEnabled(EonFeature eonFeature) {
        switch (eonFeature) {
            case WatchNext:
                if (!isWatchNextEnabled() || !isWatchNextEnabledByMinApiLevel()) {
                    return false;
                }
                return true;
            default:
                LogUC.m39d(TAG, "Unknown feature requested: isFeatureEnabled()");
                return false;
        }
    }

    private boolean isWatchNextEnabledByMinApiLevel() {
        int watchNextMinSupportedVersionResult = SystemInfo.getWatchNextMinSupportedApiLevel();
        String watchNextMinApiProperty = this.properties.getProperty(WATCH_NEXT_MIN_API);
        if (watchNextMinApiProperty != null) {
            try {
                watchNextMinSupportedVersionResult = Math.max(watchNextMinSupportedVersionResult, Integer.parseInt(watchNextMinApiProperty));
            } catch (NumberFormatException e) {
                LogUC.m39d(TAG, "watchNextMinAPI config param has invalid value: " + watchNextMinApiProperty);
            }
        }
        if (SystemInfo.getAndroidSDKVersion() >= watchNextMinSupportedVersionResult) {
            return true;
        }
        return false;
    }

    private boolean isWatchNextEnabled() {
        String watchNextEnabled = this.properties.getProperty(WATCH_NEXT_PROPERTY);
        if (watchNextEnabled == null) {
            return true;
        }
        return watchNextEnabled.equals("true");
    }
}
