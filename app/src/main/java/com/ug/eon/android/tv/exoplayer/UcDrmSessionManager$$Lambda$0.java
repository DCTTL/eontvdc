package com.ug.eon.android.tv.exoplayer;

/* renamed from: com.ug.eon.android.tv.exoplayer.UcDrmSessionManager$$Lambda$0 */
final /* synthetic */ class UcDrmSessionManager$$Lambda$0 implements Runnable {
    private final UcDrmSessionManager arg$1;
    private final IllegalStateException arg$2;

    UcDrmSessionManager$$Lambda$0(UcDrmSessionManager ucDrmSessionManager, IllegalStateException illegalStateException) {
        this.arg$1 = ucDrmSessionManager;
        this.arg$2 = illegalStateException;
    }

    public void run() {
        this.arg$1.lambda$acquireSession$0$UcDrmSessionManager(this.arg$2);
    }
}
