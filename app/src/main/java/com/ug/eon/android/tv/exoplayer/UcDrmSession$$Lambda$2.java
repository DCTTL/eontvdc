package com.ug.eon.android.tv.exoplayer;

import com.google.android.exoplayer2.drm.DefaultDrmSessionManager.EventListener;

/* renamed from: com.ug.eon.android.tv.exoplayer.UcDrmSession$$Lambda$2 */
final /* synthetic */ class UcDrmSession$$Lambda$2 implements Runnable {
    private final EventListener arg$1;

    private UcDrmSession$$Lambda$2(EventListener eventListener) {
        this.arg$1 = eventListener;
    }

    static Runnable get$Lambda(EventListener eventListener) {
        return new UcDrmSession$$Lambda$2(eventListener);
    }

    public void run() {
        this.arg$1.onDrmKeysLoaded();
    }
}
