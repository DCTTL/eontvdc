package com.ug.eon.android.tv.prefs;

/* renamed from: com.ug.eon.android.tv.prefs.ServerPrefs */
public class ServerPrefs {
    private String apiVersion;
    private String imageServerUrl;
    private String infoServerBaseUrl;
    private String name;
    private String staticServer;

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getInfoServerBaseUrl() {
        return this.infoServerBaseUrl;
    }

    public void setInfoServerBaseUrl(String infoServerBaseUrl2) {
        this.infoServerBaseUrl = infoServerBaseUrl2;
    }

    public String getApiVersion() {
        return this.apiVersion;
    }

    public void setApiVersion(String apiVersion2) {
        this.apiVersion = apiVersion2;
    }

    public String getImageServerUrl() {
        return this.imageServerUrl;
    }

    public void setImageServerUrl(String imageServerUrl2) {
        this.imageServerUrl = imageServerUrl2;
    }

    public String getStaticServer() {
        return this.staticServer;
    }

    public void setStaticServer(String staticServer2) {
        this.staticServer = staticServer2;
    }
}
