package com.ug.eon.android.tv.nettvmigration;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;

/* renamed from: com.ug.eon.android.tv.nettvmigration.NetTvMigrationDataRetriever */
public class NetTvMigrationDataRetriever {
    private static final int MIGRATION_DATA_COLUMN_INDEX = 1;
    private static final Uri MIGRATION_QUERY = Uri.parse(String.format("content://%s/%s", new Object[]{NETTV_MIGRATION_AUTHORITY, NETTV_MIGRATION_TABLE_NAME}));
    private static final String NETTV_MIGRATION_AUTHORITY = "vlaf.android.player.nettv.provider.migration";
    private static final String NETTV_MIGRATION_TABLE_NAME = "migration_info";
    private static final int STATUS_COLUMN_INDEX = 0;
    private static final String SUCCESS = "SUCCESS";
    private static final String TAG = NetTvMigrationDataRetriever.class.getName();
    private ContentResolver resolver;

    public NetTvMigrationDataRetriever(Context context) {
        this.resolver = context.getContentResolver();
    }

    /* JADX WARNING: type inference failed for: r2v0, types: [java.lang.Throwable, java.lang.String[]] */
    /* JADX WARNING: type inference failed for: r3v0, types: [java.lang.String] */
    /* JADX WARNING: type inference failed for: r4v0, types: [java.lang.String[]] */
    /* JADX WARNING: type inference failed for: r5v0, types: [java.lang.String] */
    /* JADX WARNING: type inference failed for: r2v1, types: [java.lang.Throwable] */
    /* JADX WARNING: type inference failed for: r0v6, types: [java.lang.Throwable] */
    /* JADX WARNING: type inference failed for: r2v2 */
    /* JADX WARNING: type inference failed for: r2v3 */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r2v0, types: [java.lang.Throwable, java.lang.String[]]
      assigns: [?[int, float, boolean, short, byte, char, OBJECT, ARRAY]]
      uses: [?[OBJECT, ARRAY], java.lang.String[], ?[int, boolean, OBJECT, ARRAY, byte, short, char], java.lang.Throwable]
      mth insns count: 64
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.base/java.util.ArrayList.forEach(ArrayList.java:1540)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.base/java.util.ArrayList.forEach(ArrayList.java:1540)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 6 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.ug.eon.android.tv.nettvmigration.NetTvMigrationDataResponse getNetTvMigrationData() {
        /*
            r10 = this;
            r2 = 0
            android.content.ContentResolver r0 = r10.resolver
            android.net.Uri r1 = MIGRATION_QUERY
            r3 = r2
            r4 = r2
            r5 = r2
            android.database.Cursor r8 = r0.query(r1, r2, r3, r4, r5)
            if (r8 == 0) goto L_0x0014
            boolean r0 = r8.moveToFirst()     // Catch:{ Throwable -> 0x008a, all -> 0x00a0 }
            if (r0 != 0) goto L_0x0030
        L_0x0014:
            java.lang.String r0 = TAG     // Catch:{ Throwable -> 0x008a, all -> 0x00a0 }
            java.lang.String r1 = "Could not get cursor migration data from NETTV."
            com.ug.eon.android.tv.util.LogUC.m40e(r0, r1)     // Catch:{ Throwable -> 0x008a, all -> 0x00a0 }
            com.ug.eon.android.tv.nettvmigration.NetTvMigrationDataResponse r0 = com.ug.eon.android.tv.nettvmigration.NetTvMigrationDataResponse.empty()     // Catch:{ Throwable -> 0x008a, all -> 0x00a0 }
            if (r8 == 0) goto L_0x0026
            if (r2 == 0) goto L_0x002c
            r8.close()     // Catch:{ Throwable -> 0x0027 }
        L_0x0026:
            return r0
        L_0x0027:
            r1 = move-exception
            r2.addSuppressed(r1)
            goto L_0x0026
        L_0x002c:
            r8.close()
            goto L_0x0026
        L_0x0030:
            r0 = 0
            java.lang.String r9 = r8.getString(r0)     // Catch:{ Throwable -> 0x008a, all -> 0x00a0 }
            java.lang.String r0 = TAG     // Catch:{ Throwable -> 0x008a, all -> 0x00a0 }
            java.lang.String r1 = "Migration query returned %s"
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x008a, all -> 0x00a0 }
            r4 = 0
            r3[r4] = r9     // Catch:{ Throwable -> 0x008a, all -> 0x00a0 }
            java.lang.String r1 = java.lang.String.format(r1, r3)     // Catch:{ Throwable -> 0x008a, all -> 0x00a0 }
            com.ug.eon.android.tv.util.LogUC.m41i(r0, r1)     // Catch:{ Throwable -> 0x008a, all -> 0x00a0 }
            java.lang.String r0 = "SUCCESS"
            boolean r0 = r0.equals(r9)     // Catch:{ Throwable -> 0x008a, all -> 0x00a0 }
            if (r0 != 0) goto L_0x0063
            com.ug.eon.android.tv.nettvmigration.NetTvMigrationDataResponse r0 = com.ug.eon.android.tv.nettvmigration.NetTvMigrationDataResponse.empty()     // Catch:{ Throwable -> 0x008a, all -> 0x00a0 }
            if (r8 == 0) goto L_0x0026
            if (r2 == 0) goto L_0x005f
            r8.close()     // Catch:{ Throwable -> 0x005a }
            goto L_0x0026
        L_0x005a:
            r1 = move-exception
            r2.addSuppressed(r1)
            goto L_0x0026
        L_0x005f:
            r8.close()
            goto L_0x0026
        L_0x0063:
            r0 = 1
            java.lang.String r7 = r8.getString(r0)     // Catch:{ Throwable -> 0x008a, all -> 0x00a0 }
            com.google.gson.Gson r0 = new com.google.gson.Gson     // Catch:{ Throwable -> 0x008a, all -> 0x00a0 }
            r0.<init>()     // Catch:{ Throwable -> 0x008a, all -> 0x00a0 }
            java.lang.Class<com.ug.eon.android.tv.nettvmigration.MigrationData> r1 = com.ug.eon.android.tv.nettvmigration.MigrationData.class
            java.lang.Object r6 = r0.fromJson(r7, r1)     // Catch:{ Throwable -> 0x008a, all -> 0x00a0 }
            com.ug.eon.android.tv.nettvmigration.MigrationData r6 = (com.ug.eon.android.tv.nettvmigration.MigrationData) r6     // Catch:{ Throwable -> 0x008a, all -> 0x00a0 }
            com.ug.eon.android.tv.nettvmigration.NetTvMigrationDataResponse r0 = com.ug.eon.android.tv.nettvmigration.NetTvMigrationDataResponse.from(r6)     // Catch:{ Throwable -> 0x008a, all -> 0x00a0 }
            if (r8 == 0) goto L_0x0026
            if (r2 == 0) goto L_0x0086
            r8.close()     // Catch:{ Throwable -> 0x0081 }
            goto L_0x0026
        L_0x0081:
            r1 = move-exception
            r2.addSuppressed(r1)
            goto L_0x0026
        L_0x0086:
            r8.close()
            goto L_0x0026
        L_0x008a:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x008c }
        L_0x008c:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x008f:
            if (r8 == 0) goto L_0x0096
            if (r2 == 0) goto L_0x009c
            r8.close()     // Catch:{ Throwable -> 0x0097 }
        L_0x0096:
            throw r0
        L_0x0097:
            r1 = move-exception
            r2.addSuppressed(r1)
            goto L_0x0096
        L_0x009c:
            r8.close()
            goto L_0x0096
        L_0x00a0:
            r0 = move-exception
            goto L_0x008f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ug.eon.android.tv.nettvmigration.NetTvMigrationDataRetriever.getNetTvMigrationData():com.ug.eon.android.tv.nettvmigration.NetTvMigrationDataResponse");
    }
}
