package com.ug.eon.android.tv.prefs;

import android.content.Context;
import android.content.SharedPreferences.Editor;

/* renamed from: com.ug.eon.android.tv.prefs.SharedPrefsProviderImpl */
public class SharedPrefsProviderImpl implements SharedPrefsProvider {
    private static final String DEFAULT_PREFS_FILE = "EData";
    private static final String EON_PREFERENCE_STORE = "eon.preferences";
    private static final String LOCAL_CACHE_PREFS_FILE = "LocalCache";
    private Context context;

    public SharedPrefsProviderImpl(Context c) {
        this.context = c;
    }

    public String getString(String key, String defaultValue) {
        return getString(PreferenceFile.DEFAULT, key, defaultValue);
    }

    public boolean getBoolean(PreferenceFile file, String key, boolean defaultValue) {
        return this.context.getSharedPreferences(getFile(file), 0).getBoolean(key, defaultValue);
    }

    public long getLong(PreferenceFile file, String key, long defaultValue) {
        return this.context.getSharedPreferences(getFile(file), 0).getLong(key, defaultValue);
    }

    public String getString(PreferenceFile file, String key, String defaultValue) {
        return this.context.getSharedPreferences(getFile(file), 0).getString(key, defaultValue);
    }

    public void setString(String key, String value) {
        setString(PreferenceFile.DEFAULT, key, value);
    }

    public void setString(PreferenceFile file, String key, String value) {
        Editor editor = this.context.getSharedPreferences(getFile(file), 0).edit();
        editor.putString(key, value);
        editor.apply();
    }

    public void setBoolean(PreferenceFile file, String key, boolean value) {
        Editor editor = this.context.getSharedPreferences(getFile(file), 0).edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public void setLong(PreferenceFile file, String key, long value) {
        Editor editor = this.context.getSharedPreferences(getFile(file), 0).edit();
        editor.putLong(key, value);
        editor.apply();
    }

    public void remove(String key) {
        remove(PreferenceFile.DEFAULT, key);
    }

    public void remove(PreferenceFile file, String key) {
        Editor editor = this.context.getSharedPreferences(getFile(file), 0).edit();
        editor.remove(key);
        editor.apply();
    }

    private String getFile(PreferenceFile file) {
        switch (file) {
            case LOCAL_CACHE:
                return LOCAL_CACHE_PREFS_FILE;
            case PREFERENCE:
                return EON_PREFERENCE_STORE;
            default:
                return "EData";
        }
    }
}
