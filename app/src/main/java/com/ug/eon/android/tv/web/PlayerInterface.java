package com.ug.eon.android.tv.web;

/* renamed from: com.ug.eon.android.tv.web.PlayerInterface */
public interface PlayerInterface {
    void destroy();

    void initPlayer();

    void playDvbVideo(int i, int i2, int i3);

    void playPause(boolean z);

    void playVideo(String str, Double d, boolean z);

    void playVideo(String str, boolean z);

    void resume();

    void seekTo(double d);

    void stop();

    void teletext();
}
