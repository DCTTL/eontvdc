package com.ug.eon.android.tv.cdnresolver;

/* renamed from: com.ug.eon.android.tv.cdnresolver.CDNResolveTaskResultListener */
public interface CDNResolveTaskResultListener {
    void onCDNInfoReceived(String str);
}
