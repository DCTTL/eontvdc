package com.ug.eon.android.tv.web;

import android.media.AudioManager.OnAudioFocusChangeListener;

/* renamed from: com.ug.eon.android.tv.web.UcWebInterface$$Lambda$0 */
final /* synthetic */ class UcWebInterface$$Lambda$0 implements OnAudioFocusChangeListener {
    private final UcWebInterface arg$1;

    UcWebInterface$$Lambda$0(UcWebInterface ucWebInterface) {
        this.arg$1 = ucWebInterface;
    }

    public void onAudioFocusChange(int i) {
        this.arg$1.lambda$new$6$UcWebInterface(i);
    }
}
