package com.ug.eon.android.tv.web.hal;

import android.app.Activity;
import android.webkit.JavascriptInterface;
import com.google.gson.Gson;
import com.ug.eon.android.tv.analytics.AnalyticsManager;
import com.ug.eon.android.tv.infoserver.Authentication;
import com.ug.eon.android.tv.infoserver.InfoServerClientImpl;
import com.ug.eon.android.tv.nettvmigration.NetTvMigrationDataRetriever;
import com.ug.eon.android.tv.prefs.PreferenceManager;
import com.ug.eon.android.tv.web.StartupParameters;
import com.ug.eon.android.tv.web.UcWebInterface;

/* renamed from: com.ug.eon.android.tv.web.hal.WebDeviceInterface */
public class WebDeviceInterface extends UcWebInterface {
    private static final String TAG = WebDeviceInterface.class.getName();
    private final NetTvMigrationDataRetriever netTvMigrationDataRetriever;

    public WebDeviceInterface(Activity activity, PreferenceManager preferenceManager, AnalyticsManager analyticsManager, StartupParameters params) {
        this(activity, preferenceManager, analyticsManager, params, new NetTvMigrationDataRetriever(activity));
    }

    public WebDeviceInterface(Activity activity, PreferenceManager preferenceManager, AnalyticsManager analyticsManager, StartupParameters params, NetTvMigrationDataRetriever netTvMigrationDataRetriever2) {
        super(activity, preferenceManager, analyticsManager, params);
        this.netTvMigrationDataRetriever = netTvMigrationDataRetriever2;
    }

    @JavascriptInterface
    public String getMigrationData() {
        return new Gson().toJson((Object) this.netTvMigrationDataRetriever.getNetTvMigrationData());
    }

    @JavascriptInterface
    public void userSignedOut() {
        clearPlayer();
        this.authHandler = null;
    }

    @JavascriptInterface
    public void onAuthenticated() {
        if (this.authHandler == null) {
            setUpAuthHandling();
        }
        super.onAuthenticated();
    }

    private void setUpAuthHandling() {
        setAuthHandler(new Authentication(new InfoServerClientImpl(this.mPreferenceManager), this));
    }
}
