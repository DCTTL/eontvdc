package com.ug.eon.android.tv.nettvmigration;

/* renamed from: com.ug.eon.android.tv.nettvmigration.MigrationData */
public class MigrationData {
    private String cdnIdentifier;
    private String lang;
    private String mac;
    private String serial;

    public MigrationData(String serial2, String mac2, String lang2, String cdnIdentifier2) {
        this.serial = serial2;
        this.mac = mac2;
        this.lang = lang2;
        this.cdnIdentifier = cdnIdentifier2;
    }

    public String getSerial() {
        return this.serial;
    }

    public String getMac() {
        return this.mac;
    }

    public String getLang() {
        return this.lang;
    }

    public String getCdnIdentifier() {
        return this.cdnIdentifier;
    }
}
