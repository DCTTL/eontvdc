package com.ug.eon.android.tv.infoserver;

import com.google.gson.JsonObject;
import com.ug.eon.android.tv.infoserver.entities.AssetType;
import com.ug.eon.android.tv.infoserver.entities.Assets;
import com.ug.eon.android.tv.infoserver.entities.LauncherRouteItem;
import com.ug.eon.android.tv.infoserver.entities.WatchNextItem;
import java.util.List;

/* renamed from: com.ug.eon.android.tv.infoserver.InfoServerClient */
public interface InfoServerClient {
    AuthInterceptor getAuthInterceptor();

    String getDrmToken();

    List<LauncherRouteItem> getLauncherRouteItems() throws ISCommunicationException;

    List<WatchNextItem> getWatchNextItems() throws ISCommunicationException;

    void registerStbDevice(JsonObject jsonObject, InfoServiceCallback<JsonObject> infoServiceCallback);

    Assets searchAssets(String str);

    Assets searchAssets(String str, AssetType assetType);
}
