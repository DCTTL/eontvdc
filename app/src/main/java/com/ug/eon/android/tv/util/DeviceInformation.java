package com.ug.eon.android.tv.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;
import android.os.Build.VERSION;
import android.provider.Settings.Global;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.ug.eon.android.tv.BuildConfigDev;
import com.ug.eon.android.tv.R;
import com.ug.eon.android.tv.EonApplication;
import com.ug.eon.android.tv.common.InstanceUniqueID;
import com.ug.eon.android.tv.prefs.PreferenceManager;
import com.ug.eon.android.tv.prefs.PreferenceManagerImpl;
import com.ug.eon.android.tv.prefs.SharedPrefsProviderImpl;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import ch.qos.logback.core.joran.action.Action;

/* renamed from: com.ug.eon.android.tv.util.DeviceInformation */
public class DeviceInformation {
    static final String BIG_RCU_FW_ID = "RCU_T4H_BIG_FW";
    private static final String EVENT_ID_READ = "ID read";
    private static final String KEY_APPLICATION_ID = "appId";
    private static final String KEY_APPLICATION_VERSION = "version";
    private static final String KEY_WEBPORTAL_VERSION = "webportal_version";
    public static final String PREFS_NAME = "EData";
    static final String SMALL_RCU_FW_ID = "RCU_T4H_SMALL_FW";
    private static final String TAG = DeviceInformation.class.getName();

    public static List<String> getPackageList() {
        return Collections.unmodifiableList(Arrays.asList(EonApplication.getGlobalContext().getResources().getStringArray(R.array.app_list)));
    }

    private DeviceInformation() {
    }

    public static String getWrapperVersion(Context context) {
        return getPackageVersion(context.getPackageName(), context);
    }

    private static String getPackageVersion(String packageName, Context context) {
        String version = "00";
        try {
            version = context.getPackageManager().getPackageInfo(packageName, 0).versionName;
            LogUC.m39d(TAG, "PACKAGE VERSION " + version);
            return version;
        } catch (NameNotFoundException e) {
            LogUC.m39d(TAG, "Version Error " + e.toString());
            return version;
        }
    }

    public static String getSDKVersion() {
        String sdkver = "";
        try {
            return execCmd("getprop ro.build.version.sdk");
        } catch (Exception err) {
            LogUC.m40e(TAG, "error sdkver " + err.toString());
            return sdkver;
        }
    }

    public static JsonArray getApplicationVersions(Context context) {
        JsonArray array = new JsonArray();
        for (String pack : getPackageList()) {
            JsonObject json = new JsonObject();
            json.addProperty(KEY_APPLICATION_ID, pack + "");
            json.addProperty(KEY_APPLICATION_VERSION, getPackageVersion(pack, context));
            array.add((JsonElement) json);
        }
        EonApplication.getRcuVersions().readRCUVersions();
        String bigRCUVersion = EonApplication.getRcuVersions().getBigRCUVersion();
        String smallRCUVersion = EonApplication.getRcuVersions().getSmallRCUVersion();
        if (bigRCUVersion != null) {
            JsonObject json2 = new JsonObject();
            json2.addProperty(KEY_APPLICATION_ID, BIG_RCU_FW_ID);
            json2.addProperty(KEY_APPLICATION_VERSION, bigRCUVersion);
            array.add((JsonElement) json2);
        }
        if (smallRCUVersion != null) {
            JsonObject json3 = new JsonObject();
            json3.addProperty(KEY_APPLICATION_ID, SMALL_RCU_FW_ID);
            json3.addProperty(KEY_APPLICATION_VERSION, smallRCUVersion);
            array.add((JsonElement) json3);
        }
        return array;
    }

    public static String getReleaseVersion() {
        String releasever = "";
        try {
            return execCmd("getprop ro.build.version.release");
        } catch (Exception err) {
            LogUC.m40e(TAG, "error releasever " + err.toString());
            return releasever;
        }
    }

    public static String getDeviceName(Context context) {
        return Global.getString(context.getContentResolver(), "device_name");
    }

    public static String getDeviceType(Context context) {
        String device = context.getString(R.string.project_id);
        String deviceType = BuildConfigDev.DEVICE_TYPE;
        if (!device.equals("stb")) {
            return deviceType + getSDKVersion();
        }
        return deviceType;
    }

    public static String getDeviceModelGroup() {
        String deviceModelGroup = "";
        try {
            return execCmd("getprop ro.nrdp.modelgroup");
        } catch (Exception err) {
            LogUC.m40e(TAG, "err deviceModelGroup " + err.toString());
            return deviceModelGroup;
        }
    }

    public static String getDeviceModel() {
        String deviceModel = "";
        try {
            deviceModel = execCmd("getprop ro.product.model");
            LogUC.m39d(TAG, "deviceModel " + deviceModel);
            return deviceModel;
        } catch (Exception err) {
            LogUC.m40e(TAG, "error deviceModel " + err.toString());
            return deviceModel;
        }
    }

    public static String getFirmwareVersion() {
        String firmwareVersion = "";
        try {
            firmwareVersion = execCmd(String.format("getprop %s", new Object[]{""})).trim();
            LogUC.m39d(TAG, "firmwareVersion " + firmwareVersion);
            return firmwareVersion;
        } catch (Exception err) {
            LogUC.m40e(TAG, "error firmwareVersion " + err.toString());
            return firmwareVersion;
        }
    }

    @SuppressLint({"MissingPermission"})
    public static String getDeviceSerial() {
        try {
            if (VERSION.SDK_INT >= 26) {
                return Build.getSerial();
            }
            return Build.SERIAL;
        } catch (SecurityException e) {
            LogUC.m39d(TAG, "Security exception in getDeviceSerial");
            return Build.SERIAL;
        }
    }

    public static String getPlatform() {
        return BuildConfigDev.DEVICE_PLATFORM;
    }

    public static String getMac(Context context) {
        String uuid = new InstanceUniqueID(context).getId();
        Answers.getInstance().logCustom((CustomEvent) new CustomEvent(EVENT_ID_READ).putCustomAttribute("ID", uuid));
        return uuid;
    }

    private static String getFcmToken() {
        return FirebaseInstanceId.getInstance().getToken();
    }

    public static String execCmd(String cmd) throws IOException {
        Scanner s = new Scanner(Runtime.getRuntime().exec(cmd).getInputStream()).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    @Nullable
    private static String getWebPortalVersion(PreferenceManager preferenceManager) {
        return (String) preferenceManager.getValue(KEY_WEBPORTAL_VERSION).orElse(null);
    }

    public static long getServiceProviderId(PreferenceManager preferenceManager) {
        return (long) ((Integer) preferenceManager.getServiceProviderPrefs().map(DeviceInformation$$Lambda$0.$instance).orElse(Integer.valueOf(-1))).intValue();
    }

    public static long getCommunityId(PreferenceManager preferenceManager) {
        return (long) ((Integer) preferenceManager.getAuthPrefs().map(DeviceInformation$$Lambda$1.$instance).orElse(Integer.valueOf(-1))).intValue();
    }

    @NonNull
    public static JsonObject getDeviceInfo(Context context) {
        JsonObject json = new JsonObject();
        json.add("applicationVersions", getApplicationVersions(context));
        json.addProperty("clientSwBuild", getWrapperVersion(context));
        String clientSwVersion = getWebPortalVersion(new PreferenceManagerImpl(new SharedPrefsProviderImpl(context)));
        if (clientSwVersion != null) {
            json.addProperty("clientSwVersion", clientSwVersion);
        }
        json.addProperty("deviceName", getDeviceName(context));
        json.addProperty("deviceType", getDeviceType(context));
        json.addProperty("fcmToken", getFcmToken());
        json.addProperty("mac", getMac(context));
        json.addProperty("modelName", getDeviceModel().trim());
        json.addProperty("platform", getPlatform());
        json.addProperty("serial", getDeviceSerial());
        JsonObject systemSwVersion = new JsonObject();
        systemSwVersion.addProperty(Action.NAME_ATTRIBUTE, "Android");
        systemSwVersion.addProperty(KEY_APPLICATION_VERSION, getReleaseVersion().trim());
        systemSwVersion.addProperty("bootImageVersion", getFirmwareVersion());
        systemSwVersion.addProperty("bootLogoVersion", "1.0");
        systemSwVersion.addProperty("portalVersion", "1.0");
        systemSwVersion.addProperty("rcuFwVersion", "1.0");
        systemSwVersion.addProperty("rcuUdbVersion", "1.0");
        systemSwVersion.addProperty("splashScreensVersion", "1.0");
        systemSwVersion.addProperty("systemName", "system");
        systemSwVersion.addProperty("tvSystem", "PAL");
        json.add("systemSwVersion", systemSwVersion);
        Optional<Boolean> wifiStatus = new WiFiStatusFetcher().fetchWiFiStatus();
        if (wifiStatus.isPresent()) {
            json.addProperty("wifiEnabled", (Boolean) wifiStatus.get());
        }
        LogUC.m39d(TAG, "deviceInfo: " + json.toString());
        return json;
    }
}
