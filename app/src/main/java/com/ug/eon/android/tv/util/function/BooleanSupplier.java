package com.ug.eon.android.tv.util.function;

/* renamed from: com.ug.eon.android.tv.util.function.BooleanSupplier */
public interface BooleanSupplier {
    boolean getAsBoolean();
}
