package com.ug.eon.android.tv.util;

/* renamed from: com.ug.eon.android.tv.util.DifferenceResult */
public enum DifferenceResult {
    FIRST_SET,
    SECOND_SET,
    BOTH
}
