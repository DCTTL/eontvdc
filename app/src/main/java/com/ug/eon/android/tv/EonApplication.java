package com.ug.eon.android.tv;

import android.app.Application;
import android.content.Context;
import com.ug.eon.android.tv.util.RCUVersions;

/* renamed from: com.ug.eon.android.tv.EonApplication */
public class EonApplication extends Application {
    private static final String TAG = EonApplication.class.getSimpleName();
    private static Context appContext;
    private static RCUVersions rcuVersions = new RCUVersions();

    public static Context getGlobalContext() {
        return appContext;
    }

    public static RCUVersions getRcuVersions() {
        return rcuVersions;
    }

    public void onCreate() {
        super.onCreate();
        appContext = this;
    }
}
