package com.ug.eon.android.tv.prefs;

import com.ug.eon.android.tv.util.Optional.Function;

/* renamed from: com.ug.eon.android.tv.prefs.PreferenceManagerImpl$$Lambda$1 */
final /* synthetic */ class PreferenceManagerImpl$$Lambda$1 implements Function {
    static final Function $instance = new PreferenceManagerImpl$$Lambda$1();

    private PreferenceManagerImpl$$Lambda$1() {
    }

    public Object apply(Object obj) {
        return ((AuthPrefs) obj).getTopics();
    }
}
