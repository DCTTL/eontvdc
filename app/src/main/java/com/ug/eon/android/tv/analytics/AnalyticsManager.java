package com.ug.eon.android.tv.analytics;

import android.content.Context;
import android.os.Bundle;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.ug.eon.android.tv.util.DeviceInformation;
import com.ug.eon.android.tv.util.LogUC;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.ug.eon.android.tv.analytics.AnalyticsManager */
public class AnalyticsManager {
    private static final String TAG = AnalyticsManager.class.getName();
    private String mAppVersion;
    private String mDeviceType;
    private FirebaseAnalytics mFirebaseAnalytics;
    private String mPlatform = DeviceInformation.getPlatform();

    public AnalyticsManager(Context context) {
        this.mAppVersion = DeviceInformation.getWrapperVersion(context);
        this.mDeviceType = DeviceInformation.getDeviceType(context);
        this.mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
        this.mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
    }

    public void logEvent(String eventName, String eventParams) {
        LogUC.m39d(TAG, "Sending event: " + eventName + ", params: " + eventParams);
        Bundle bundle = new Bundle();
        bundle.putString("appVersion", this.mAppVersion);
        bundle.putString("platform", this.mPlatform);
        bundle.putString("deviceType", this.mDeviceType);
        try {
            JSONObject params = new JSONObject(eventParams);
            Iterator iterator = params.keys();
            while (iterator.hasNext()) {
                String paramName = (String) iterator.next();
                bundle.putString(paramName, params.getString(paramName));
            }
        } catch (JSONException e) {
            LogUC.m40e(TAG, "error to parse json " + e.getMessage());
        }
        this.mFirebaseAnalytics.logEvent(eventName, bundle);
    }
}
