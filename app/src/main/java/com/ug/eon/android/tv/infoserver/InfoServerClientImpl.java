package com.ug.eon.android.tv.infoserver;

import com.google.gson.JsonObject;
import com.ug.eon.android.tv.infoserver.entities.AssetType;
import com.ug.eon.android.tv.infoserver.entities.Assets;
import com.ug.eon.android.tv.infoserver.entities.DrmToken;
import com.ug.eon.android.tv.infoserver.entities.LauncherRouteItem;
import com.ug.eon.android.tv.infoserver.entities.WatchNextItem;
import com.ug.eon.android.tv.prefs.AuthPrefs;
import com.ug.eon.android.tv.prefs.PreferenceManager;
import com.ug.eon.android.tv.prefs.ServiceProviderPrefs;
import com.ug.eon.android.tv.util.CryptoUtils;
import com.ug.eon.android.tv.util.LogUC;
import com.ug.eon.android.tv.util.Optional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import javax.crypto.spec.IvParameterSpec;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/* renamed from: com.ug.eon.android.tv.infoserver.InfoServerClientImpl */
public class InfoServerClientImpl implements InfoServerClient {
    private static final String TAG = InfoServerClientImpl.class.getName();
    private AuthInterceptor authInterceptor = new AuthInterceptor();
    private Optional<ISApi> isApiClient;
    private Optional<AuthPrefs> mAuthPrefsOptional = Optional.empty();
    private Optional<ServiceProviderPrefs> mServiceProviderPrefsOptional = Optional.empty();
    private PreferenceManager preferenceManager;

    public InfoServerClientImpl(PreferenceManager pm) {
        this.preferenceManager = pm;
    }

    public AuthInterceptor getAuthInterceptor() {
        return this.authInterceptor;
    }

    public void registerStbDevice(JsonObject deviceInfo, final InfoServiceCallback<JsonObject> callback) {
        ((ISApi) getIsApiClient().get()).registerStbDevice(getRequestHeader(), deviceInfo).enqueue(new Callback<JsonObject>() {
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    callback.onResponse(response.body());
                } else {
                    callback.onResponse(null);
                }
            }

            public void onFailure(Call<JsonObject> call, Throwable t) {
                callback.onResponse(null);
            }
        });
    }

    public String getDrmToken() {
        return (String) getIsApiClient().map(new InfoServerClientImpl$$Lambda$0(this)).orElse("");
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ String lambda$getDrmToken$0$InfoServerClientImpl(ISApi isApi) {
        String str;
        AuthPrefs authPrefs = getAuthPrefsOptional();
        if (authPrefs == null) {
            return null;
        }
        ServiceProviderPrefs serviceProviderPrefs = getServiceProviderPrefsOptional();
        if (serviceProviderPrefs == null) {
            return null;
        }
        byte[] iv = CryptoUtils.getRandomIvParameter().getIV();
        Call<DrmToken> call = isApi.getDrmToken(Base64.getUrlEncoder().encodeToString(iv), getEncryptedText(new IvParameterSpec(iv), authPrefs, serviceProviderPrefs.getIdentifier()), authPrefs.getStreamUn(), serviceProviderPrefs.getIdentifier());
        LogUC.m39d(TAG, "call url: " + call.request().url().toString());
        DrmToken token = null;
        try {
            token = (DrmToken) call.execute().body();
            LogUC.m39d(TAG, "drmTokenResponse: " + token.getToken());
        } catch (IOException e) {
            LogUC.m39d(TAG, "Fetch DRM token: I/O exception: " + e);
        }
        if (token != null) {
            str = token.getToken();
        } else {
            str = null;
        }
        return str;
    }

    public Assets searchAssets(String query) {
        return searchAssets(query, null);
    }

    public Assets searchAssets(String query, AssetType assetType) {
        if (query == null) {
            return null;
        }
        String strAssetType = null;
        switch (assetType) {
            case LIVETV:
                strAssetType = "TV";
                break;
            case CUTV:
                strAssetType = "CUTV";
                break;
            case VOD:
                strAssetType = "VOD";
                break;
        }
        return (Assets) getIsApiClient().map(new InfoServerClientImpl$$Lambda$1(this, query, strAssetType)).orElse(null);
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ Assets lambda$searchAssets$1$InfoServerClientImpl(String query, String searchAssetType, ISApi isApi) {
        try {
            return (Assets) isApi.searchAssets(getRequestHeader(), query, "RECOMMENDED", searchAssetType).execute().body();
        } catch (IOException e) {
            LogUC.m39d(TAG, "Search Assets: I/O exception: " + e);
            return null;
        }
    }

    public List<WatchNextItem> getWatchNextItems() throws ISCommunicationException {
        return (List) getIsApiClient().map(new InfoServerClientImpl$$Lambda$2(this)).orElseThrow(InfoServerClientImpl$$Lambda$3.$instance);
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ List lambda$getWatchNextItems$2$InfoServerClientImpl(ISApi isApi) {
        new ArrayList();
        try {
            Response<List<WatchNextItem>> response = isApi.getWatchNextItems(getRequestHeader()).execute();
            if (response.isSuccessful()) {
                return (List) response.body();
            }
            throw new ISCommunicationException("Fetch Watch Next items: request not successful");
        } catch (IOException e) {
            throw new ISCommunicationException("Fetch Watch Next items: I/O exception ", e);
        }
    }

    static final /* synthetic */ ISCommunicationException lambda$getWatchNextItems$3$InfoServerClientImpl() {
        return new ISCommunicationException("Fetch Watch Next items: Could not create IS API client");
    }

    public List<LauncherRouteItem> getLauncherRouteItems() throws ISCommunicationException {
        return (List) getIsApiClient().map(new InfoServerClientImpl$$Lambda$4(this)).orElseThrow(InfoServerClientImpl$$Lambda$5.$instance);
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ List lambda$getLauncherRouteItems$4$InfoServerClientImpl(ISApi isApi) {
        new ArrayList();
        try {
            Response<List<LauncherRouteItem>> response = isApi.getLauncherRouteItems(getRequestHeader()).execute();
            if (response.isSuccessful()) {
                return (List) response.body();
            }
            throw new ISCommunicationException("Fetch Launcher Route items: request not successful ");
        } catch (IOException e) {
            throw new ISCommunicationException("Fetch Launcher Route items: I/O exception ", e);
        }
    }

    static final /* synthetic */ ISCommunicationException lambda$getLauncherRouteItems$5$InfoServerClientImpl() {
        return new ISCommunicationException("Fetch Launcher Route items: Could not create IS API client");
    }

    private Optional<ISApi> getIsApiClient() {
        if (this.isApiClient == null || !this.isApiClient.isPresent()) {
            this.isApiClient = ISServiceGenerator.createISClient(this.authInterceptor, this.preferenceManager);
        }
        return this.isApiClient;
    }

    private AuthPrefs getAuthPrefsOptional() {
        if (!this.mAuthPrefsOptional.isPresent()) {
            this.mAuthPrefsOptional = this.preferenceManager.getAuthPrefs();
        }
        return (AuthPrefs) this.mAuthPrefsOptional.orElse(null);
    }

    private ServiceProviderPrefs getServiceProviderPrefsOptional() {
        if (!this.mServiceProviderPrefsOptional.isPresent()) {
            this.mServiceProviderPrefsOptional = this.preferenceManager.getServiceProviderPrefs();
        }
        return (ServiceProviderPrefs) this.mServiceProviderPrefsOptional.orElse(null);
    }

    private String getEncryptedText(IvParameterSpec ivParameterSpec, AuthPrefs authPrefs, String spIdentifier) {
        return Base64.getUrlEncoder().encodeToString(CryptoUtils.encryptAES("u=" + authPrefs.getStreamUn() + ";sp=" + spIdentifier + ";device=" + authPrefs.getDeviceNumber() + ";", authPrefs.getStreamKey(), ivParameterSpec));
    }

    private String getRequestHeader() {
        return "Bearer " + ((String) this.preferenceManager.getAuthToken().orElse(""));
    }
}
