package com.ug.eon.android.tv.prefs;

import com.ug.eon.android.tv.util.Optional;

/* renamed from: com.ug.eon.android.tv.prefs.PreferenceManager */
public interface PreferenceManager {
    Optional<String[]> getAppSubscribedTopics();

    Optional<AuthPrefs> getAuthPrefs();

    Optional<String> getAuthToken();

    Optional<Boolean> getBoolean(String str, boolean z);

    Optional<ConfigPref> getConfigPrefs();

    Optional<String[]> getFcmTopics();

    Optional<Long> getLong(String str);

    Optional<PlayerLanguages> getPlayerLanguagesPrefs();

    Optional<ServerPrefs> getServerPrefs();

    Optional<ServiceProviderPrefs> getServiceProviderPrefs();

    Optional<String> getValue(PreferenceFile preferenceFile, String str);

    Optional<String> getValue(String str);

    void remove(String str);

    void setBoolean(String str, boolean z);

    void setConfigPrefs(ConfigPref configPref);

    void setLong(String str, long j);

    void setValue(PreferenceFile preferenceFile, String str, String str2);

    void setValue(String str, String str2);
}
