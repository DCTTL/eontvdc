package com.ug.eon.android.tv.web;

import android.webkit.JavascriptInterface;
import com.ug.eon.android.tv.PlayerType;
import com.ug.eon.android.tv.util.LogUC;

/* renamed from: com.ug.eon.android.tv.web.PlayerControls */
public abstract class PlayerControls {
    private static final String STREAM_DVB = "stream_dvb";
    private static final String STREAM_VOD = "stream_vod";
    private static final String TAG = PlayerControls.class.getName();
    private PlayerMode mPlayerMode;
    private PlayerType mPlayerType;
    private PlayerInterface mUcLivePlayer;
    private PlayerInterface mUcPlayer;

    public abstract PlayerInterface getPlayerInstance(PlayerType playerType);

    /* access modifiers changed from: protected */
    public boolean isPlayerEnabled() {
        return true;
    }

    @JavascriptInterface
    public void playStream(String url, String stream, double startTime, int networkId, int streamId, int serviceId, boolean drmProtected, String playerType) {
        if (isPlayerEnabled()) {
            char c = 65535;
            switch (stream.hashCode()) {
                case -1194441519:
                    if (stream.equals(STREAM_DVB)) {
                        c = 0;
                        break;
                    }
                    break;
                case -1194424436:
                    if (stream.equals(STREAM_VOD)) {
                        c = 1;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    LogUC.m39d(TAG, "dvb, network id: " + networkId + ", stream id: " + streamId + ", service id: " + serviceId);
                    if (this.mUcPlayer != null && PlayerMode.isOtt(this.mPlayerMode)) {
                        this.mUcPlayer.stop();
                    }
                    this.mPlayerMode = PlayerMode.DVB_C;
                    this.mUcLivePlayer.playDvbVideo(networkId, streamId, serviceId);
                    return;
                case 1:
                    LogUC.m39d(TAG, "play vod stream");
                    this.mUcPlayer = getPlayer(PlayerType.getPlayerType(playerType));
                    if (this.mUcLivePlayer != null && PlayerMode.isDvb(this.mPlayerMode)) {
                        this.mUcLivePlayer.stop();
                    }
                    this.mPlayerMode = PlayerMode.OTT;
                    this.mUcPlayer.playVideo(url, Double.valueOf(startTime), drmProtected);
                    return;
                default:
                    LogUC.m39d(TAG, "play hls/dash, default");
                    this.mUcPlayer = getPlayer(PlayerType.getPlayerType(playerType));
                    if (this.mUcLivePlayer != null && PlayerMode.isDvb(this.mPlayerMode)) {
                        this.mUcLivePlayer.stop();
                    }
                    this.mPlayerMode = PlayerMode.OTT;
                    this.mUcPlayer.playVideo(url, drmProtected);
                    return;
            }
        }
    }

    @JavascriptInterface
    public void playStream(String url, String stream, double startTime, int networkId, int streamId, int serviceId, boolean drmProtected) {
        playStream(url, stream, startTime, networkId, streamId, serviceId, drmProtected, PlayerType.EXOPLAYER.name());
    }

    @JavascriptInterface
    public void playStream(String url, String stream, double startTime, int networkId, int streamId, int serviceId) {
        playStream(url, stream, startTime, networkId, streamId, serviceId, false);
    }

    @JavascriptInterface
    public void seekTo(double ms) {
        if (this.mUcPlayer != null) {
            this.mUcPlayer.seekTo(ms);
        }
    }

    @JavascriptInterface
    public void resume() {
        if (this.mUcPlayer != null) {
            this.mUcPlayer.resume();
        }
    }

    @JavascriptInterface
    public void pause() {
        if (this.mUcLivePlayer != null && PlayerMode.isDvb(this.mPlayerMode)) {
            this.mUcLivePlayer.playPause(false);
        } else if (this.mUcPlayer != null) {
            this.mUcPlayer.playPause(false);
        }
    }

    @JavascriptInterface
    public void play() {
        if (this.mUcPlayer != null) {
            this.mUcPlayer.playPause(true);
        }
    }

    @JavascriptInterface
    public void stop() {
        if (this.mUcLivePlayer != null && PlayerMode.isDvb(this.mPlayerMode)) {
            this.mUcLivePlayer.stop();
        } else if (this.mUcPlayer != null) {
            this.mUcPlayer.stop();
        }
    }

    @JavascriptInterface
    public void teletext() {
        if (this.mUcLivePlayer != null && PlayerMode.isDvb(this.mPlayerMode)) {
            this.mUcLivePlayer.teletext();
        }
    }

    public void destroyPlayer() {
        if (this.mUcPlayer != null) {
            this.mUcPlayer.destroy();
        }
    }

    public void setLiveUcPlayer(PlayerInterface ucPlayer) {
        this.mUcLivePlayer = ucPlayer;
    }

    /* access modifiers changed from: protected */
    public void clearPlayer() {
        destroyPlayer();
        this.mUcPlayer = null;
    }

    private PlayerInterface getPlayer(PlayerType playerType) {
        if (playerType == this.mPlayerType && this.mUcPlayer != null) {
            return this.mUcPlayer;
        }
        destroyPlayer();
        this.mPlayerType = playerType;
        return getPlayerInstance(this.mPlayerType);
    }
}
