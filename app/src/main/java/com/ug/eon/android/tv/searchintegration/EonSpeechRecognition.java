package com.ug.eon.android.tv.searchintegration;

import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;
import android.speech.SpeechRecognizer;
import com.ug.eon.android.tv.language.EonLanguageSelection;
import com.ug.eon.android.tv.prefs.PreferenceManager;
import com.ug.eon.android.tv.util.Optional;
import com.ug.eon.android.tv.util.function.BooleanSupplier;

/* renamed from: com.ug.eon.android.tv.searchintegration.EonSpeechRecognition */
public class EonSpeechRecognition implements SpeechRecognition {
    private static final int LISTENING_TIMER_DURATION = 7500;
    private static final int LISTENING_TIMER_TICK = 500;
    private static final int MAX_RESULTS = 5;
    private static final int MAX_RMS_NEGATIVE_COUNT = 30;
    private CountDownTimer countDownTimer;
    /* access modifiers changed from: private */
    public boolean countDownTimerRunning = false;
    private BooleanSupplier mRecordAudioPermissionCheck;
    /* access modifiers changed from: private */
    public int mRmsNegativeCount = 0;
    private Consumer<Integer> mSearchErrorHandler;
    private PreferenceManager preferenceManager;
    /* access modifiers changed from: private */
    public SpeechRecognizer speechRecognizer;

    public EonSpeechRecognition(Context context, Consumer<String> onSearchResult, Consumer<String> onPartialResult, Consumer<Integer> onSearchError, BooleanSupplier recordAudioPermissionCheck, PreferenceManager pm) {
        this.speechRecognizer = SpeechRecognizer.createSpeechRecognizer(context);
        this.mRecordAudioPermissionCheck = recordAudioPermissionCheck;
        this.preferenceManager = pm;
        this.mSearchErrorHandler = onSearchError;
        this.speechRecognizer.setRecognitionListener(new EonRecognitionListener(onSearchResult, onPartialResult, EonSpeechRecognition$$Lambda$0.$instance, EonSpeechRecognition$$Lambda$1.$instance, onSearchError, new EonSpeechRecognition$$Lambda$2(this)));
    }

    static final /* synthetic */ void lambda$new$0$EonSpeechRecognition() {
    }

    static final /* synthetic */ void lambda$new$1$EonSpeechRecognition() {
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void lambda$new$2$EonSpeechRecognition(Float rmsdB) {
        if (rmsdB.floatValue() < 0.0f) {
            this.mRmsNegativeCount++;
        }
    }

    public void startListening() {
        if (!this.mRecordAudioPermissionCheck.getAsBoolean()) {
            this.mSearchErrorHandler.accept(Integer.valueOf(-1));
        } else if (this.countDownTimerRunning) {
            this.mSearchErrorHandler.accept(Integer.valueOf(-1));
        } else {
            this.mRmsNegativeCount = 0;
            this.speechRecognizer.startListening(makeRecognizerIntent());
            this.countDownTimer = new CountDownTimer(7500, 500) {
                public void onTick(long millisUntilFinished) {
                    if (EonSpeechRecognition.this.mRmsNegativeCount >= 30 && EonSpeechRecognition.this.countDownTimerRunning) {
                        EonSpeechRecognition.this.speechRecognizer.stopListening();
                        EonSpeechRecognition.this.countDownTimerRunning = false;
                        cancel();
                    }
                }

                public void onFinish() {
                    if (EonSpeechRecognition.this.countDownTimerRunning) {
                        EonSpeechRecognition.this.speechRecognizer.stopListening();
                        EonSpeechRecognition.this.countDownTimerRunning = false;
                    }
                }
            };
            this.countDownTimerRunning = true;
            this.countDownTimer.start();
        }
    }

    public void stopListening() {
        if (this.countDownTimer != null) {
            this.countDownTimer.cancel();
            this.countDownTimerRunning = false;
        }
        if (this.speechRecognizer != null) {
            this.speechRecognizer.stopListening();
        }
    }

    private Intent makeRecognizerIntent() {
        String intentSpeechLanguage;
        Intent intent = new Intent("android.speech.action.RECOGNIZE_SPEECH");
        intent.putExtra("calling_package", getClass().getPackage().getName());
        intent.putExtra("android.speech.extra.LANGUAGE_MODEL", "free_form");
        intent.putExtra("android.speech.extra.MAX_RESULTS", 5);
        intent.putExtra("android.speech.extra.PARTIAL_RESULTS", true);
        if (this.preferenceManager != null) {
            Optional<String> speechInputLanguage = this.preferenceManager.getValue("speechInputLanguage");
            Optional<String> interfaceLanguage = this.preferenceManager.getValue("lang");
            EonLanguageSelection languageSelection = EonLanguageSelection.ENGLISH;
            if (speechInputLanguage.isPresent()) {
                languageSelection = EonLanguageSelection.fromISO639((String) speechInputLanguage.get());
            } else if (interfaceLanguage.isPresent()) {
                languageSelection = EonLanguageSelection.fromISO639((String) interfaceLanguage.get());
            }
            switch (languageSelection) {
                case BOSNIAN:
                    intentSpeechLanguage = "hr_HR";
                    break;
                case MONTENEGRIN:
                    intentSpeechLanguage = "sr_RS";
                    break;
                case ENGLISH:
                    intentSpeechLanguage = "en_US";
                    break;
                case CROATIAN:
                    intentSpeechLanguage = "hr_HR";
                    break;
                case MACEDONIAN:
                    intentSpeechLanguage = "mk_MK";
                    break;
                case SLOVENIAN:
                    intentSpeechLanguage = "sl";
                    break;
                case SERBIAN:
                    intentSpeechLanguage = "sr_RS";
                    break;
                default:
                    intentSpeechLanguage = "en_US";
                    break;
            }
            intent.putExtra("android.speech.extra.LANGUAGE", intentSpeechLanguage);
        }
        return intent;
    }
}
