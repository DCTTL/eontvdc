package com.ug.eon.android.tv.infoserver.entities;

import android.util.SparseArray;

/* renamed from: com.ug.eon.android.tv.infoserver.entities.AssetType */
public enum AssetType {
    LIVETV(1),
    CUTV(2),
    VOD(3);
    
    private static SparseArray<AssetType> map;
    private int mOrdinal;

    static {
        int i;
        AssetType[] values;
        map = new SparseArray<>();
        for (AssetType asset : values()) {
            map.put(asset.getOrdinal(), asset);
        }
    }

    private AssetType(int ordinal) {
        this.mOrdinal = ordinal;
    }

    public static AssetType valueOf(int v) {
        return (AssetType) map.get(v, null);
    }

    public int getOrdinal() {
        return this.mOrdinal;
    }
}
