package com.ug.eon.android.tv.prefs;

/* renamed from: com.ug.eon.android.tv.prefs.PlayerLanguages */
public class PlayerLanguages {
    private String primaryAudioLanguage;
    private String primarySubtitleLanguage;
    private String secondaryAudioLanguage;
    private String secondarySubtitleLanguage;

    public String getPrimaryAudioLanguage() {
        return this.primaryAudioLanguage;
    }

    public void setPrimaryAudioLanguage(String primaryAudioLanguage2) {
        this.primaryAudioLanguage = primaryAudioLanguage2;
    }

    public String getSecondaryAudioLanguage() {
        return this.secondaryAudioLanguage;
    }

    public void setSecondaryAudioLanguage(String secondaryAudioLanguage2) {
        this.secondaryAudioLanguage = secondaryAudioLanguage2;
    }

    public String getPrimarySubtitleLanguage() {
        return this.primarySubtitleLanguage;
    }

    public void setPrimarySubtitleLanguage(String primarySubtitleLanguage2) {
        this.primarySubtitleLanguage = primarySubtitleLanguage2;
    }

    public String getSecondarySubtitleLanguage() {
        return this.secondarySubtitleLanguage;
    }

    public void setSecondarySubtitleLanguage(String secondarySubtitleLanguage2) {
        this.secondarySubtitleLanguage = secondarySubtitleLanguage2;
    }
}
