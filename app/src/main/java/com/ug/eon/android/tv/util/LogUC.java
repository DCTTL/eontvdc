package com.ug.eon.android.tv.util;

/* renamed from: com.ug.eon.android.tv.util.LogUC */
public class LogUC {
    /* renamed from: d */
    public static void m39d(String tag, String msg) {
        log(3, tag, msg);
    }

    /* renamed from: i */
    public static void m41i(String tag, String msg) {
        log(4, tag, msg);
    }

    /* renamed from: e */
    public static void m40e(String tag, String msg) {
        log(6, tag, msg);
    }

    /* renamed from: v */
    public static void m42v(String tag, String msg) {
        log(2, tag, msg);
    }

    /* renamed from: w */
    public static void m43w(String tag, String msg) {
        log(5, tag, msg);
    }

    private static void log(int level, String tag, String msg) {
    }
}
