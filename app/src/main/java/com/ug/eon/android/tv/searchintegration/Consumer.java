package com.ug.eon.android.tv.searchintegration;

/* renamed from: com.ug.eon.android.tv.searchintegration.Consumer */
public interface Consumer<T> {
    void accept(T t);
}
