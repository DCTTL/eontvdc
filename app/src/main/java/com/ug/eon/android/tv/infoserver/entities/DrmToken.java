package com.ug.eon.android.tv.infoserver.entities;

import com.google.gson.annotations.SerializedName;

/* renamed from: com.ug.eon.android.tv.infoserver.entities.DrmToken */
public class DrmToken {
    @SerializedName("drm_token")
    private String token;

    public String getToken() {
        return this.token;
    }

    public void setToken(String token2) {
        this.token = token2;
    }
}
