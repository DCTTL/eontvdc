package com.ug.eon.android.tv.infoserver;

import java.util.function.Supplier;

/* renamed from: com.ug.eon.android.tv.infoserver.InfoServerClientImpl$$Lambda$5 */
final /* synthetic */ class InfoServerClientImpl$$Lambda$5 implements Supplier {
    static final Supplier $instance = new InfoServerClientImpl$$Lambda$5();

    private InfoServerClientImpl$$Lambda$5() {
    }

    public Object get() {
        return InfoServerClientImpl.lambda$getLauncherRouteItems$5$InfoServerClientImpl();
    }
}
