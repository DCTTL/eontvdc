package com.ug.eon.android.tv.util;

/* renamed from: com.ug.eon.android.tv.util.EventListener */
public interface EventListener {
    public static final int EVENT_BUFFERING = 8;
    public static final int EVENT_DVB_ERROR = 4;
    public static final int EVENT_ERROR = 1;
    public static final int EVENT_IDLE = 7;
    public static final int EVENT_PLAYED = 3;
    public static final int EVENT_RADIO_PLAYED = 6;
    public static final int EVENT_RETUNED = 5;
    public static final int EVENT_STOPPED = 2;

    void onBitrateChange(int i);

    void sendEvent(int i);
}
