package com.ug.eon.android.tv.util;

import com.ug.eon.android.tv.prefs.ServiceProviderPrefs;
import com.ug.eon.android.tv.util.Optional.Function;

/* renamed from: com.ug.eon.android.tv.util.DeviceInformation$$Lambda$0 */
final /* synthetic */ class DeviceInformation$$Lambda$0 implements Function {
    static final Function $instance = new DeviceInformation$$Lambda$0();

    private DeviceInformation$$Lambda$0() {
    }

    public Object apply(Object obj) {
        return Integer.valueOf(((ServiceProviderPrefs) obj).getId());
    }
}
