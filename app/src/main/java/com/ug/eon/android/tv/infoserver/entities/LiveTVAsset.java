package com.ug.eon.android.tv.infoserver.entities;

import com.google.firebase.storage.internal.Util;
import com.ug.eon.android.tv.util.LogUC;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

/* renamed from: com.ug.eon.android.tv.infoserver.entities.LiveTVAsset */
public class LiveTVAsset extends Asset {
    private static final String TAG = LiveTVAsset.class.getName();
    private int channelId;
    private List<Image> channelLogos;
    private String endTime;
    private String startTime;

    public LiveTVAsset() {
        super(AssetType.LIVETV);
    }

    public LiveTVAsset(AssetType assetType) {
        super(assetType);
    }

    public int getChannelId() {
        return this.channelId;
    }

    public long getDuration() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(Util.ISO_8601_FORMAT, Locale.getDefault());
        try {
            return dateFormat.parse(this.endTime).getTime() - dateFormat.parse(this.startTime).getTime();
        } catch (Exception e) {
            LogUC.m40e(TAG, "Could not parse start or end time: " + e.toString());
            return 0;
        }
    }

    public List<Image> getChannelLogos() {
        return this.channelLogos;
    }
}
