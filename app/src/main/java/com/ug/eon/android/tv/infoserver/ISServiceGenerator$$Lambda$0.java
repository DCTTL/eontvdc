package com.ug.eon.android.tv.infoserver;

import com.ug.eon.android.tv.prefs.ServerPrefs;
import com.ug.eon.android.tv.util.Optional.Function;

/* renamed from: com.ug.eon.android.tv.infoserver.ISServiceGenerator$$Lambda$0 */
final /* synthetic */ class ISServiceGenerator$$Lambda$0 implements Function {
    static final Function $instance = new ISServiceGenerator$$Lambda$0();

    private ISServiceGenerator$$Lambda$0() {
    }

    public Object apply(Object obj) {
        return ISServiceGenerator.lambda$generateApiBaseUrl$0$ISServiceGenerator((ServerPrefs) obj);
    }
}
