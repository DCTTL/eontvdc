package com.ug.eon.android.tv.web;

import com.ug.eon.android.tv.util.LogUC;
import org.json.JSONObject;

/* renamed from: com.ug.eon.android.tv.web.DeepLinkContent */
public class DeepLinkContent {
    private static final String TAG = DeepLinkContent.class.getName();
    private int channelId;

    /* renamed from: id */
    private int f37id;
    private String type;

    public String getType() {
        return this.type;
    }

    public void setType(String type2) {
        this.type = type2;
    }

    public int getId() {
        return this.f37id;
    }

    public void setId(int id) {
        this.f37id = id;
    }

    public int getChannelId() {
        return this.channelId;
    }

    public void setChannelId(int channelId2) {
        this.channelId = channelId2;
    }

    public static DeepLinkContent createFrom(String data) {
        DeepLinkContent deepLinkContent = new DeepLinkContent();
        try {
            JSONObject dataContent = new JSONObject(data);
            deepLinkContent.setType(dataContent.getString("type"));
            deepLinkContent.setId(dataContent.getInt(TtmlNode.ATTR_ID));
            try {
                deepLinkContent.setChannelId(dataContent.getInt("channelId"));
            } catch (Exception e) {
                LogUC.m40e(TAG, "ChannelId doesn't exist in JSON content intent data");
            }
        } catch (Exception e2) {
            LogUC.m40e(TAG, "Error in parsing JSON content intent data");
        }
        return deepLinkContent;
    }
}
