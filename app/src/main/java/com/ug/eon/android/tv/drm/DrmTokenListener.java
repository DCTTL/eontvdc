package com.ug.eon.android.tv.drm;


import androidx.annotation.Nullable;

/* renamed from: com.ug.eon.android.tv.drm.DrmTokenListener */
public interface DrmTokenListener {
    void onDrmTokenFetched(@Nullable String str);
}
