package com.ug.eon.android.tv.infoserver;

import com.ug.eon.android.tv.util.Optional.Function;

/* renamed from: com.ug.eon.android.tv.infoserver.InfoServerClientImpl$$Lambda$0 */
final /* synthetic */ class InfoServerClientImpl$$Lambda$0 implements Function {
    private final InfoServerClientImpl arg$1;

    InfoServerClientImpl$$Lambda$0(InfoServerClientImpl infoServerClientImpl) {
        this.arg$1 = infoServerClientImpl;
    }

    public Object apply(Object obj) {
        return this.arg$1.lambda$getDrmToken$0$InfoServerClientImpl((ISApi) obj);
    }
}
