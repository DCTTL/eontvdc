package com.ug.eon.android.tv.infoserver.entities;

import ch.qos.logback.core.CoreConstants;

/* renamed from: com.ug.eon.android.tv.infoserver.entities.Image */
public class Image {
    private int height;
    private String mode;
    private String path;
    private String size;
    private String type;
    private int width;

    public String getPath() {
        return this.path;
    }

    public String getSize() {
        return this.size;
    }

    public String getType() {
        return this.type;
    }

    public String getMode() {
        return this.mode;
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }

    public String toString() {
        return "Image{path='" + this.path + CoreConstants.SINGLE_QUOTE_CHAR + ", size='" + this.size + CoreConstants.SINGLE_QUOTE_CHAR + ", type='" + this.type + CoreConstants.SINGLE_QUOTE_CHAR + ", mode='" + this.mode + CoreConstants.SINGLE_QUOTE_CHAR + ", width=" + this.width + ", height=" + this.height + CoreConstants.CURLY_RIGHT;
    }
}
