package com.ug.eon.android.tv.util.filesystem;

import java.io.IOException;
import java.nio.file.Path;

/* renamed from: com.ug.eon.android.tv.util.filesystem.FileReaderWriter */
public interface FileReaderWriter {
    void copyFileFromContentProvider(String str, Path path) throws IOException;

    void copyFolder(Path path, Path path2) throws IOException;

    void deleteFile(Path path) throws IOException;

    void deleteFolder(Path path) throws IOException;

    String readFile(String str);

    void writeFile(Path path, String str);

    void writeFile(Path path, byte[] bArr);

    void writeFileToCache(String str, String str2);

    void writeFileToCache(String str, byte[] bArr);

    void writeFileToCache(Path path, String str);

    void writeFileToCache(Path path, byte[] bArr);

    void zipFolder(Path path, Path path2) throws IOException;
}
