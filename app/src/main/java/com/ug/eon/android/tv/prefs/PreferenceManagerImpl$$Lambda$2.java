package com.ug.eon.android.tv.prefs;

import com.google.gson.GsonBuilder;
import com.ug.eon.android.tv.util.Optional.Function;

/* renamed from: com.ug.eon.android.tv.prefs.PreferenceManagerImpl$$Lambda$2 */
final /* synthetic */ class PreferenceManagerImpl$$Lambda$2 implements Function {
    private final Class arg$1;

    PreferenceManagerImpl$$Lambda$2(Class cls) {
        this.arg$1 = cls;
    }

    public Object apply(Object obj) {
        return new GsonBuilder().create().fromJson((String) obj, this.arg$1);
    }
}
