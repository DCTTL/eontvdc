package com.ug.eon.android.tv;

import com.google.gson.JsonObject;
import com.ug.eon.android.tv.cdnresolver.CDNResolveTaskResultListener;
import com.ug.eon.android.tv.util.RCUInfoTaskResultListener;
import com.ug.eon.android.tv.web.BatchFileDownloadTaskCompletedListener;
import com.ug.eon.android.tv.web.StartupParameters;

/* renamed from: com.ug.eon.android.tv.Chameleon */
public interface Chameleon extends BatchFileDownloadTaskCompletedListener, RCUInfoTaskResultListener, CDNResolveTaskResultListener {
    void addInterface(Object obj, String str);

    void changeRoute(String str);

    void changeRoute(String str, int i);

    void clearCache();

    void destroy();

    void linkToContent(String str, JsonObject jsonObject);

    void loadChameleon(StartupParameters startupParameters);

    void onBitrateChanged(int i);

    void onCancelVoiceSearch();

    void onChannelImagesDownloaded(boolean z);

    void onFinalVoiceResult(String str);

    void onLoggingLevelChanged(int i);

    void onNativeKeyDown(int i);

    void onNativeKeyUp(int i);

    void onNetworkCheck(boolean z);

    void onPartialVoiceResult(String str);

    void onPlayerStateChanged(int i);

    void onRefreshToken();

    void onResumePauseActivity(boolean z, boolean z2);

    void onUpdate(String str);

    void setBackgroundColor(int i);

    void setUpScale(int i);

    void setVideoEncoding(String str);

    void showOTTFingerprint(String str, long j);

    void showOsdNotification(String str);

    void updateChannelsData();
}
