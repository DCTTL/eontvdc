package com.ug.eon.android.tv.prefs;

/* renamed from: com.ug.eon.android.tv.prefs.ConfigPref */
public class ConfigPref {
    private String loggingLevel;

    public String getLoggingLevel() {
        return this.loggingLevel;
    }

    public void setLoggingLevel(String loggingLevel2) {
        this.loggingLevel = loggingLevel2;
    }
}
