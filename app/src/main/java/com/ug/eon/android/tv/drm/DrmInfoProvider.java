package com.ug.eon.android.tv.drm;

 import android.text.TextUtils;

 import androidx.annotation.Nullable;

 import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.ug.eon.android.tv.infoserver.InfoServerClient;
import com.ug.eon.android.tv.prefs.PreferenceManager;
import com.ug.eon.android.tv.util.LogUC;
import com.ug.eon.android.tv.util.Optional;

/* renamed from: com.ug.eon.android.tv.drm.DrmInfoProvider */
public class DrmInfoProvider {
    private static final String KEY_DEVICE_NUMBER = "deviceNumber";
    private static final String TAG = DrmInfoProvider.class.getName();
    private InfoServerClient isClient;
    private Optional<String> mDeviceNumber = this.preferenceManager.getValue(KEY_DEVICE_NUMBER);
    private PreferenceManager preferenceManager;

    /* renamed from: com.ug.eon.android.tv.drm.DrmInfoProvider$DrmInfo */
    private static class DrmInfo {
        @SerializedName("CxAuthenticationDataToken")
        private String cxAuthenticationDataToken;
        @SerializedName("CxClientInfo")
        private CxClientInfo cxClientInfo;
        @SerializedName("Version")
        private String version;

        /* renamed from: com.ug.eon.android.tv.drm.DrmInfoProvider$DrmInfo$CxClientInfo */
        private static class CxClientInfo {
            @SerializedName("CxDeviceId")
            private String cxDeviceId;
            @SerializedName("DeviceType")
            private String deviceType;
            @SerializedName("DrmClientType")
            private String drmClientType;
            @SerializedName("DrmClientVersion")
            private String drmClientVersion;

            private CxClientInfo() {
            }

            public String getDeviceType() {
                return this.deviceType;
            }

            public void setDeviceType(String deviceType2) {
                this.deviceType = deviceType2;
            }

            public String getDrmClientType() {
                return this.drmClientType;
            }

            public void setDrmClientType(String drmClientType2) {
                this.drmClientType = drmClientType2;
            }

            public String getDrmClientVersion() {
                return this.drmClientVersion;
            }

            public void setDrmClientVersion(String drmClientVersion2) {
                this.drmClientVersion = drmClientVersion2;
            }

            public String getCxDeviceId() {
                return this.cxDeviceId;
            }

            public void setCxDeviceId(String cxDeviceId2) {
                this.cxDeviceId = cxDeviceId2;
            }
        }

        private DrmInfo() {
        }

        public static DrmInfo createDrmInfo() {
            DrmInfo drmInfo = new DrmInfo();
            drmInfo.setVersion("1.0.0");
            CxClientInfo cxClientInfo2 = new CxClientInfo();
            cxClientInfo2.setDeviceType("Browser");
            cxClientInfo2.setDrmClientType("Widevine-HTML5");
            cxClientInfo2.setDrmClientVersion("1.0.0");
            drmInfo.setCxClientInfo(cxClientInfo2);
            return drmInfo;
        }

        public String getVersion() {
            return this.version;
        }

        public void setVersion(String version2) {
            this.version = version2;
        }

        public String getCxAuthenticationDataToken() {
            return this.cxAuthenticationDataToken;
        }

        public void setCxAuthenticationDataToken(String cxAuthenticationDataToken2) {
            this.cxAuthenticationDataToken = cxAuthenticationDataToken2;
        }

        public CxClientInfo getCxClientInfo() {
            return this.cxClientInfo;
        }

        public void setCxClientInfo(CxClientInfo cxClientInfo2) {
            this.cxClientInfo = cxClientInfo2;
        }
    }

    public DrmInfoProvider(InfoServerClient isc, PreferenceManager prefsMgr) {
        this.isClient = isc;
        this.preferenceManager = prefsMgr;
    }

    @Nullable
    public String getDrmInfo() {
        String token = this.isClient.getDrmToken();
        if (!TextUtils.isEmpty(token)) {
            return getDrmTokenJson(token);
        }
        LogUC.m43w(TAG, "Unable to obtain DRM token");
        return null;
    }

    private String getDrmTokenJson(String drmToken) {
        DrmInfo drmInfo = DrmInfo.createDrmInfo();
        drmInfo.setCxAuthenticationDataToken(drmToken);
        DrmInfo.CxClientInfo cxClientInfo = drmInfo.getCxClientInfo();
        if (!this.mDeviceNumber.isPresent()) {
            this.mDeviceNumber = this.preferenceManager.getValue(KEY_DEVICE_NUMBER);
        }
        if (!this.mDeviceNumber.isPresent()) {
            LogUC.m43w(TAG, "Unable to obtain device number");
            return null;
        }
        cxClientInfo.setCxDeviceId((String) this.mDeviceNumber.get());
        drmInfo.setCxClientInfo(cxClientInfo);
        return new Gson().toJson((Object) drmInfo);
    }
}
