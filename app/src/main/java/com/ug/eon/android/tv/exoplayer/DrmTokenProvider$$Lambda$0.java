package com.ug.eon.android.tv.exoplayer;

import com.ug.eon.android.tv.prefs.ServiceProviderPrefs;
import com.ug.eon.android.tv.util.Optional.Function;

/* renamed from: com.ug.eon.android.tv.exoplayer.DrmTokenProvider$$Lambda$0 */
final /* synthetic */ class DrmTokenProvider$$Lambda$0 implements Function {
    static final Function $instance = new DrmTokenProvider$$Lambda$0();

    private DrmTokenProvider$$Lambda$0() {
    }

    public Object apply(Object obj) {
        return ((ServiceProviderPrefs) obj).getLicenseServerUrlWidewine();
    }
}
