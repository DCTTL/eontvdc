package com.ug.eon.android.tv.web;

/* renamed from: com.ug.eon.android.tv.web.PlayerInterface$$CC */
public abstract /* synthetic */ class PlayerInterface$$CC {
    public static void playDvbVideo(PlayerInterface playerInterface, int networkId, int streamId, int serviceId) {
        throw new UnsupportedOperationException("Method playDvbVideo not supported by this player.");
    }

    public static void teletext(PlayerInterface playerInterface) {
        throw new UnsupportedOperationException("Method teletext not supported by this player.");
    }
}
