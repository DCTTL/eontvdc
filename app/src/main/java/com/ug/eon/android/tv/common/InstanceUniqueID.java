package com.ug.eon.android.tv.common;

import android.content.Context;
import android.content.SharedPreferences.Editor;
import com.ug.eon.android.tv.util.LogUC;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.UUID;

/* renamed from: com.ug.eon.android.tv.common.InstanceUniqueID */
public class InstanceUniqueID {
    private static final String INSTALLATION = "INSTALLATION";
    private static final String KEY_UUID = "uuid.key";
    private static final String PREFERENCE_STORAGE = "com.ug.eon.preferences";
    private static final String TAG = InstanceUniqueID.class.getName();
    private Context mContext;
    private String mUUID;

    public InstanceUniqueID(Context context) {
        this.mContext = context.getApplicationContext();
    }

    public synchronized String getId() {
        if (this.mUUID == null) {
            this.mUUID = readUUID();
        }
        LogUC.m39d(TAG, "getId: " + this.mUUID);
        return this.mUUID;
    }

    private String readUUID() {
        String uuid = getUUIDValue();
        if (uuid != null) {
            return uuid.replace("-", "");
        }
        File installation = new File(this.mContext.getFilesDir(), INSTALLATION);
        if (installation.exists()) {
            try {
                uuid = readInstallationFile(installation);
                LogUC.m39d(TAG, "[uuid migration] read from file: " + uuid);
                setUUIDValue(uuid);
            } catch (IOException e) {
                uuid = null;
            }
        }
        if (uuid == null) {
            uuid = UUID.randomUUID().toString();
            LogUC.m39d(TAG, "generated new uuid: " + uuid);
            setUUIDValue(uuid);
        }
        return uuid.replace("-", "");
    }

    private static String readInstallationFile(File installation) throws IOException {
        RandomAccessFile f=new RandomAccessFile(installation,"r");
        byte[] bytes=new byte[(int)f.length()];
        f.readFully(bytes);
        f.close();
        return new String(bytes);
    }

    private String getUUIDValue() {
        return this.mContext.getSharedPreferences(PREFERENCE_STORAGE, 0).getString(KEY_UUID, null);
    }

    private void setUUIDValue(String uuid) {
        Editor editor = this.mContext.getSharedPreferences(PREFERENCE_STORAGE, 0).edit();
        editor.putString(KEY_UUID, uuid);
        editor.apply();
    }
}
