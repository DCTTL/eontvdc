package com.ug.eon.android.tv;

import android.content.Context;
import android.view.SurfaceView;
import com.ug.eon.android.tv.drm.DrmInfoProvider;
import com.ug.eon.android.tv.exoplayer.DrmTokenProvider;
import com.ug.eon.android.tv.exoplayer.ExoPlayer;
import com.ug.eon.android.tv.infoserver.InfoServerClientImpl;
import com.ug.eon.android.tv.prefs.PreferenceManager;
import com.ug.eon.android.tv.util.EventListener;
import com.ug.eon.android.tv.util.LogUC;

/* renamed from: com.ug.eon.android.tv.PlayerFactory */
public class PlayerFactory {
    private static final String TAG = PlayerFactory.class.getName();

    private PlayerFactory() {
    }

    public static Player playerInstance(Context context, PlayerType playerType, SurfaceView view, PreferenceManager preferenceManager, EventListener eventListener) {
        DrmInfoProvider drmInfoProvider = new DrmInfoProvider(new InfoServerClientImpl(preferenceManager), preferenceManager);
        LogUC.m39d(TAG, "create exo player instance");
        DrmTokenProvider drmTokenProvider = new DrmTokenProvider(preferenceManager, drmInfoProvider);
        drmTokenProvider.getClass();
        return new Player(new ExoPlayer(context, view, PlayerFactory$$Lambda$0.get$Lambda(drmTokenProvider), eventListener));
    }
}
