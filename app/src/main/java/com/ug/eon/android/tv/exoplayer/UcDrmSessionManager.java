package com.ug.eon.android.tv.exoplayer;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import androidx.annotation.NonNull;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.drm.DefaultDrmSessionManager.EventListener;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.drm.DrmInitData.SchemeData;
import com.google.android.exoplayer2.drm.DrmSession;
import com.google.android.exoplayer2.drm.DrmSession.DrmSessionException;
import com.google.android.exoplayer2.drm.DrmSessionManager;
import com.google.android.exoplayer2.drm.ExoMediaDrm;
import com.google.android.exoplayer2.drm.ExoMediaDrm.OnEventListener;
import com.google.android.exoplayer2.drm.FrameworkMediaCrypto;
import com.google.android.exoplayer2.drm.FrameworkMediaDrm;
import com.google.android.exoplayer2.drm.HttpMediaDrmCallback;
import com.google.android.exoplayer2.drm.UnsupportedDrmException;
import com.google.android.exoplayer2.extractor.mp4.PsshAtomUtil;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.Util;
import com.ug.eon.android.tv.exoplayer.UcDrmSession.ProvisioningManager;
import com.ug.eon.android.tv.util.LogUC;
import com.ug.eon.android.tv.util.function.Supplier;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

@TargetApi(18)
/* renamed from: com.ug.eon.android.tv.exoplayer.UcDrmSessionManager */
public class UcDrmSessionManager implements DrmSessionManager<FrameworkMediaCrypto>, ProvisioningManager {
    private static final String CENC_SCHEME_MIME_TYPE = "cenc";
    private static final int INITIAL_DRM_REQUEST_RETRY_COUNT = 3;
    static final int MODE_DOWNLOAD = 2;
    static final int MODE_PLAYBACK = 0;
    static final int MODE_QUERY = 1;
    static final int MODE_RELEASE = 3;
    private static final String TAG = UcDrmSessionManager.class.getSimpleName();
    /* access modifiers changed from: private */
    public UcDrmSession currentSession;
    private final Supplier<DrmInfo> drmSupplier;
    private final Handler eventHandler;
    private final EventListener eventListener;
    private final int initialDrmRequestRetryCount;
    private final ExoMediaDrm<FrameworkMediaCrypto> mediaDrm;
    volatile MediaDrmHandler mediaDrmHandler;
    /* access modifiers changed from: private */
    public int mode;
    private final Map<byte[], byte[]> offlineKeyCache;
    private byte[] offlineLicenseKeySetId;
    private final HashMap<String, String> optionalKeyRequestParameters;
    private Looper playbackLooper;
    private final List<UcDrmSession> provisioningSessions;
    private final UUID uuid;

    /* renamed from: com.ug.eon.android.tv.exoplayer.UcDrmSessionManager$MediaDrmEventListener */
    private class MediaDrmEventListener implements OnEventListener<FrameworkMediaCrypto> {
        private MediaDrmEventListener() {
        }

        public void onEvent(ExoMediaDrm<? extends FrameworkMediaCrypto> exoMediaDrm, byte[] sessionId, int event, int extra, byte[] data) {
            if (UcDrmSessionManager.this.mode == 0) {
                UcDrmSessionManager.this.mediaDrmHandler.obtainMessage(event, sessionId).sendToTarget();
            }
        }
    }

    @SuppressLint({"HandlerLeak"})
    /* renamed from: com.ug.eon.android.tv.exoplayer.UcDrmSessionManager$MediaDrmHandler */
    private class MediaDrmHandler extends Handler {
        MediaDrmHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            byte[] sessionId = (byte[]) msg.obj;
            if (UcDrmSessionManager.this.currentSession != null && UcDrmSessionManager.this.currentSession.hasSessionId(sessionId)) {
                UcDrmSessionManager.this.currentSession.onMediaDrmEvent(msg.what);
            }
        }
    }

    @Retention(RetentionPolicy.SOURCE)
    /* renamed from: com.ug.eon.android.tv.exoplayer.UcDrmSessionManager$Mode */
    public @interface Mode {
    }

    UcDrmSessionManager(Supplier<DrmInfo> supplier, EventListener eventListener2, Handler handler) throws UnsupportedDrmException {
        this(C.WIDEVINE_UUID, FrameworkMediaDrm.newInstance(C.WIDEVINE_UUID), null, handler, eventListener2, 3, supplier);
    }

    private UcDrmSessionManager(UUID uuid2, ExoMediaDrm<FrameworkMediaCrypto> mediaDrm2, HashMap<String, String> optionalKeyRequestParameters2, Handler eventHandler2, EventListener eventListener2, int initialDrmRequestRetryCount2, Supplier<DrmInfo> supplier) {
        Assertions.checkNotNull(uuid2);
        Assertions.checkNotNull(mediaDrm2);
        this.uuid = uuid2;
        this.mediaDrm = mediaDrm2;
        this.optionalKeyRequestParameters = optionalKeyRequestParameters2;
        this.eventHandler = eventHandler2;
        this.eventListener = eventListener2;
        this.initialDrmRequestRetryCount = initialDrmRequestRetryCount2;
        this.drmSupplier = supplier;
        this.offlineKeyCache = new HashMap();
        this.provisioningSessions = new ArrayList();
        mediaDrm2.setPropertyString("sessionSharing", "enable");
        mediaDrm2.setOnEventListener(new MediaDrmEventListener());
    }

    private void setMode(int mode2, byte[] offlineLicenseKeySetId2) {
        Assertions.checkState(this.offlineKeyCache.isEmpty());
        if (mode2 == 1 || mode2 == 3) {
            Assertions.checkNotNull(offlineLicenseKeySetId2);
        }
        this.mode = mode2;
        this.offlineLicenseKeySetId = offlineLicenseKeySetId2;
    }

    public boolean canAcquireSession(@NonNull DrmInitData drmInitData) {
        if (getSchemeData(drmInitData, this.uuid, true) == null) {
            return false;
        }
        String schemeType = drmInitData.schemeType;
        if (schemeType == null || "cenc".equals(schemeType)) {
            return true;
        }
        if ((C.CENC_TYPE_cbc1.equals(schemeType) || C.CENC_TYPE_cbcs.equals(schemeType) || C.CENC_TYPE_cens.equals(schemeType)) && Util.SDK_INT < 24) {
            return false;
        }
        return true;
    }

    public DrmSession<FrameworkMediaCrypto> acquireSession(Looper playbackLooper2, DrmInitData drmInitData) {
        Assertions.checkState(this.playbackLooper == null || this.playbackLooper == playbackLooper2);
        if (this.offlineKeyCache.isEmpty()) {
            this.playbackLooper = playbackLooper2;
            if (this.mediaDrmHandler == null) {
                this.mediaDrmHandler = new MediaDrmHandler(playbackLooper2);
            }
        }
        SchemeData data = getSchemeData(drmInitData, this.uuid, false);
        if (data == null) {
            IllegalStateException illegalStateException = new IllegalStateException("Media does not support uuid: " + this.uuid);
            if (!(this.eventHandler == null || this.eventListener == null)) {
                this.eventHandler.post(new UcDrmSessionManager$$Lambda$0(this, illegalStateException));
            }
            return new ErrorStateDrmSession(new DrmSessionException(illegalStateException));
        }
        byte[] initData = getSchemeInitData(data, this.uuid);
        String mimeType = getSchemeMimeType(data, this.uuid);
        this.offlineLicenseKeySetId = null;
        Iterator it = this.offlineKeyCache.entrySet().iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Entry<byte[], byte[]> entry = (Entry) it.next();
            if (Arrays.equals((byte[]) entry.getKey(), initData)) {
                this.offlineLicenseKeySetId = (byte[]) entry.getValue();
                break;
            }
        }
        if (this.currentSession == null || (!this.currentSession.hasInitData(initData) && !this.currentSession.hasOfflineKey(this.offlineLicenseKeySetId))) {
            if (this.offlineLicenseKeySetId == null) {
                LogUC.m39d(TAG, "offlineLicenseKeySetId is null, needs to download license");
                this.mode = 2;
                this.currentSession = new UcDrmSession(this.uuid, this.mediaDrm, this, initData, mimeType, this.mode, this.offlineLicenseKeySetId, this.optionalKeyRequestParameters, createHttpMediaDrmCallback((DrmInfo) this.drmSupplier.get()), playbackLooper2, this.eventHandler, this.eventListener, this.initialDrmRequestRetryCount);
                this.currentSession.setOfflineKeyConsumer(new UcDrmSessionManager$$Lambda$1(this, initData));
            } else {
                LogUC.m39d(TAG, "offlineLicenseKeySetId exists, restore keys");
                this.mode = 1;
                this.currentSession = new UcDrmSession(this.uuid, this.mediaDrm, this, initData, mimeType, this.mode, this.offlineLicenseKeySetId, this.optionalKeyRequestParameters, null, playbackLooper2, this.eventHandler, this.eventListener, this.initialDrmRequestRetryCount);
            }
            this.currentSession.acquire();
            return this.currentSession;
        }
        this.currentSession.acquire();
        LogUC.m39d(TAG, "currentSession can be used");
        return this.currentSession;
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void lambda$acquireSession$0$UcDrmSessionManager(IllegalStateException error) {
        this.eventListener.onDrmSessionManagerError(error);
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void lambda$acquireSession$1$UcDrmSessionManager(byte[] initData, byte[] value) {
        byte[] bArr = (byte[]) this.offlineKeyCache.put(initData, value);
    }

    private HttpMediaDrmCallback createHttpMediaDrmCallback(DrmInfo drmInfo) {
        HttpMediaDrmCallback drmCallback = new HttpMediaDrmCallback(drmInfo.getLicenseServerUrl(), new UcHttpDataSourceFactory(null));
        String[] drmKeyProperties = drmInfo.getDrmKeyProperties();
        for (int i = 0; i < drmKeyProperties.length - 1; i += 2) {
            drmCallback.setKeyRequestProperty(drmKeyProperties[i], drmKeyProperties[i + 1]);
        }
        return drmCallback;
    }

    public void releaseSession(DrmSession<FrameworkMediaCrypto> session) {
        if (!(session instanceof ErrorStateDrmSession)) {
            LogUC.m39d(TAG, "release drm session");
            UcDrmSession drmSession = (UcDrmSession) session;
            if (drmSession.release()) {
                this.currentSession = null;
                if (this.provisioningSessions.size() > 1 && this.provisioningSessions.get(0) == drmSession) {
                    ((UcDrmSession) this.provisioningSessions.get(1)).provision();
                }
                this.provisioningSessions.remove(drmSession);
            }
        }
    }

    public void onProvisionCompleted() {
        for (UcDrmSession session : this.provisioningSessions) {
            session.onProvisionCompleted();
        }
        this.provisioningSessions.clear();
    }

    public void provisionRequired(UcDrmSession session) {
        this.provisioningSessions.add(session);
        if (this.provisioningSessions.size() == 1) {
            session.provision();
        }
    }

    public void onProvisionError(Exception error) {
        for (UcDrmSession session : this.provisioningSessions) {
            session.onProvisionError(error);
        }
        this.provisioningSessions.clear();
    }

    private static SchemeData getSchemeData(DrmInitData drmInitData, UUID uuid2, boolean allowMissingData) {
        boolean uuidMatches;
        List<SchemeData> matchingSchemeDatas = new ArrayList<>(drmInitData.schemeDataCount);
        for (int i = 0; i < drmInitData.schemeDataCount; i++) {
            SchemeData schemeData = drmInitData.get(i);
            if (schemeData.matches(uuid2) || (C.CLEARKEY_UUID.equals(uuid2) && schemeData.matches(C.COMMON_PSSH_UUID))) {
                uuidMatches = true;
            } else {
                uuidMatches = false;
            }
            if (uuidMatches && (schemeData.data != null || allowMissingData)) {
                matchingSchemeDatas.add(schemeData);
            }
        }
        if (matchingSchemeDatas.isEmpty()) {
            return null;
        }
        if (C.WIDEVINE_UUID.equals(uuid2)) {
            for (int i2 = 0; i2 < matchingSchemeDatas.size(); i2++) {
                SchemeData matchingSchemeData = (SchemeData) matchingSchemeDatas.get(i2);
                int version = matchingSchemeData.hasData() ? PsshAtomUtil.parseVersion(matchingSchemeData.data) : -1;
                if (Util.SDK_INT < 23 && version == 0) {
                    return matchingSchemeData;
                }
                if (Util.SDK_INT >= 23 && version == 1) {
                    return matchingSchemeData;
                }
            }
        }
        return (SchemeData) matchingSchemeDatas.get(0);
    }

    private static byte[] getSchemeInitData(SchemeData data, UUID uuid2) {
        byte[] schemeInitData = data.data;
        if (Util.SDK_INT >= 21) {
            return schemeInitData;
        }
        byte[] psshData = PsshAtomUtil.parseSchemeSpecificData(schemeInitData, uuid2);
        if (psshData == null) {
            return schemeInitData;
        }
        return psshData;
    }

    private static String getSchemeMimeType(SchemeData data, UUID uuid2) {
        String schemeMimeType = data.mimeType;
        if (Util.SDK_INT >= 26 || !C.CLEARKEY_UUID.equals(uuid2)) {
            return schemeMimeType;
        }
        if (MimeTypes.VIDEO_MP4.equals(schemeMimeType) || MimeTypes.AUDIO_MP4.equals(schemeMimeType)) {
            return "cenc";
        }
        return schemeMimeType;
    }
}
