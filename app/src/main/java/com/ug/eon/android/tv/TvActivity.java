package com.ug.eon.android.tv;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.RelativeLayout;

import androidx.core.app.ActivityCompat;

import com.crashlytics.android.Crashlytics.Builder;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.crashlytics.android.core.CrashlyticsCore;
import com.ug.eon.android.tv.NetworkCheck.NetworkReceiver;
import com.ug.eon.android.tv.analytics.AnalyticsManager;
import com.ug.eon.android.tv.ui.EonIntroView;
import com.ug.eon.android.tv.prefs.PreferenceManager;
import com.ug.eon.android.tv.prefs.PreferenceManagerImpl;
import com.ug.eon.android.tv.prefs.SharedPrefsProviderImpl;
import com.ug.eon.android.tv.util.Dpad;
import com.ug.eon.android.tv.util.LogUC;
import com.ug.eon.android.tv.web.DeepLink;
import com.ug.eon.android.tv.web.StartupParameters;
import com.ug.eon.android.tv.web.hal.WebDeviceInterface;

import io.fabric.sdk.android.Fabric;

/* renamed from: com.ug.eon.android.tv.TvActivity */
public class TvActivity extends Activity {
    private static final String EON_SPLASH_SCREEN = "eon_splash_status";
    private static final String EVENT_APP_STARTED = "Application started";
    private static final String EXTRA_DEEP_LINK = "deepLink";
    private static final String EXTRA_DEEP_LINK_DATA = "deepLinkData";
    private static final int RECORD_AUDIO_PERMISSION_TAG = 1001;
    private static final String TAG = TvActivity.class.getName();
    private boolean mCreated;
    private Dpad mDPad;
    private NetworkReceiver mNetworkReceiver;
    private WebDeviceInterface mWebDeviceInterface;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Builder().core(new CrashlyticsCore.Builder().disabled(false).build()).build());
        setContentView(R.layout.activity_tv);
        this.mCreated = false;
        this.mDPad = new Dpad();
        this.mNetworkReceiver = new NetworkReceiver();
        initMainActivityOnCreate();
        getWindow().addFlags(128);
        Answers.getInstance().logCustom(new CustomEvent(EVENT_APP_STARTED));
    }

    /* access modifiers changed from: private */
    /* renamed from: checkAndPromptForRecordAudioPermission */
    public boolean bridge$lambda$0$TvActivity() {
        boolean hasRecordAudioPermition = true;
        if (VERSION.SDK_INT >= 23) {
            hasRecordAudioPermition = checkSelfPermission("android.permission.RECORD_AUDIO") == PackageManager.PERMISSION_GRANTED;
            if (!hasRecordAudioPermition) {
                ActivityCompat.requestPermissions(this, new String[]{"android.permission.RECORD_AUDIO"}, 1001);
            }
        } else {
            ActivityCompat.requestPermissions(this, new String[]{"android.permission.RECORD_AUDIO"}, 1001);
        }
        return hasRecordAudioPermition;
    }

    private void initMainActivityOnCreate() {
        Intent intent = getIntent();
        PreferenceManager pm = new PreferenceManagerImpl(new SharedPrefsProviderImpl(getApplicationContext()));
        AnalyticsManager analyticsManager = new AnalyticsManager(this);
        StartupParameters params = new StartupParameters();
        params.setStartupAction(intent.getStringExtra(EXTRA_DEEP_LINK));
        params.setStartupActionData(intent.getStringExtra(EXTRA_DEEP_LINK_DATA));
        params.setRecordAudioPermissionCheck(new TvActivity$$Lambda$0(this));
        this.mWebDeviceInterface = new WebDeviceInterface(this, pm, analyticsManager, params);
        playEonSplash(pm);
        this.mNetworkReceiver.setOnConnectionChangeListener(this.mWebDeviceInterface);
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        DeepLink deepLink = DeepLink.getDeepLink(intent.getStringExtra(EXTRA_DEEP_LINK));
        if (deepLink != null) {
            LogUC.m39d(TAG, "received deep link: " + deepLink.name());
            this.mWebDeviceInterface.doDeepLink(deepLink, intent.getStringExtra(EXTRA_DEEP_LINK_DATA));
        }
    }

    public void onResume() {
        super.onResume();
        if (!getPackageManager().hasSystemFeature("android.software.leanback")) {
            hideSystemUI();
        }
        LogUC.m39d(TAG, "On Resume " + this.mCreated);
        if (this.mCreated && this.mWebDeviceInterface != null) {
            this.mWebDeviceInterface.reloadState(true, true);
        }
        registerReceiver(this.mNetworkReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        this.mCreated = true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        LogUC.m39d(TAG, "On Pause");
        if (this.mWebDeviceInterface != null) {
            this.mWebDeviceInterface.reloadState(false, false);
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        LogUC.m39d(TAG, "onStart");
        registerReceiver(this.mNetworkReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        LogUC.m39d(TAG, "onStop");
        unregisterReceiver(this.mNetworkReceiver);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.mWebDeviceInterface.destroyPlayer();
        LogUC.m39d(TAG, "onDestroy: STOP VIDEO");
    }

    private void hideSystemUI() {
        getWindow().getDecorView().setSystemUiVisibility(3846);
    }

    private void playEonSplash(PreferenceManager preferenceManager) {
        if (!((Boolean) preferenceManager.getBoolean(EON_SPLASH_SCREEN, false).get()).booleanValue()) {
            EonIntroView eonIntroView = new EonIntroView(this);
            ((RelativeLayout) findViewById(R.C0745id.parent_of_all)).addView(eonIntroView);
            eonIntroView.play();
            preferenceManager.setBoolean(EON_SPLASH_SCREEN, true);
        }
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        LogUC.m39d(TAG, "dispatch Event Code: " + event.getKeyCode());
        LogUC.m39d(TAG, "dispatch Event Action: " + event.getAction());
        int keyCode = this.mDPad.getDirectionPressed(event);
        if (keyCode < 0) {
            keyCode = event.getKeyCode();
        }
        LogUC.m39d(TAG, "KEY " + event.getAction() + " CODE: " + keyCode);
        if (keyCode == 24 || keyCode == 25) {
            return super.dispatchKeyEvent(event);
        }
        if (keyCode == 143 || this.mWebDeviceInterface == null) {
            return false;
        }
        return this.mWebDeviceInterface.dispatchNativeKey(keyCode, event.getAction());
    }
}
