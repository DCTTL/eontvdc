package com.ug.eon.android.tv.util;

import androidx.annotation.Nullable;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import io.fabric.sdk.android.services.common.CommonUtils;

/* renamed from: com.ug.eon.android.tv.util.CryptoUtils */
public class CryptoUtils {
    private static final String AES_CBC_PKCS5PADDING = "AES/CBC/PKCS5Padding";
    private static final int BLOCK_SIZE = 16;
    private static final String TAG = CryptoUtils.class.getSimpleName();

    private CryptoUtils() {
    }

    @Nullable
    public static String calculateSha1(String value) {
        try {
            MessageDigest md = MessageDigest.getInstance(CommonUtils.SHA1_INSTANCE);
            byte[] textBytes = value.getBytes(StandardCharsets.UTF_8);
            md.update(textBytes, 0, textBytes.length);
            return toHex(md.digest());
        } catch (NoSuchAlgorithmException e) {
            LogUC.m40e("CryptoUtils", e.getMessage());
            return null;
        }
    }

    public static String toHex(byte[] data) {
        StringBuilder hexString = new StringBuilder();
        for (byte aData : data) {
            hexString.append(Integer.toHexString(aData & 255));
        }
        return hexString.toString();
    }

    public static byte[] encryptAES(String text, String key, IvParameterSpec ivParameterSpec) {
        try {
            SecretKeySpec keySpec = new SecretKeySpec(Base64.getUrlDecoder().decode(key.getBytes()), "AES");
            Cipher cipher = Cipher.getInstance(AES_CBC_PKCS5PADDING);
            cipher.init(1, keySpec, ivParameterSpec);
            return cipher.doFinal(text.getBytes());
        } catch (GeneralSecurityException e) {
            LogUC.m40e(TAG, "failed to encrypt aes");
            return new byte[0];
        }
    }

    public static IvParameterSpec getRandomIvParameter() {
        byte[] iv = new byte[16];
        new SecureRandom().nextBytes(iv);
        return new IvParameterSpec(iv);
    }
}
