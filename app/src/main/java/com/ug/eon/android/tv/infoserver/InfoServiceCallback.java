package com.ug.eon.android.tv.infoserver;

/* renamed from: com.ug.eon.android.tv.infoserver.InfoServiceCallback */
public interface InfoServiceCallback<T> {
    void onResponse(T t);
}
