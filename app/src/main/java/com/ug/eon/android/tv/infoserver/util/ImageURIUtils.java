package com.ug.eon.android.tv.infoserver.util;

import com.ug.eon.android.tv.infoserver.entities.Image;
import com.ug.eon.android.tv.prefs.PreferenceManager;
import com.ug.eon.android.tv.prefs.ServerPrefs;
import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;

/* renamed from: com.ug.eon.android.tv.infoserver.util.ImageURIUtils */
public class ImageURIUtils {
    private ImageURIUtils() {
    }

    public static String getLImageURI(List<Image> images, PreferenceManager preferenceManager) {
        return getImageURIBySize(images, preferenceManager, "L");
    }

    public static String getXLImageURI(List<Image> images, PreferenceManager preferenceManager) {
        return getImageURIBySize(images, preferenceManager, "XL");
    }

    public static String getImageURIBySize(List<Image> images, PreferenceManager preferenceManager, String size) {
        return getImageURI(images, preferenceManager, new ImageURIUtils$$Lambda$0(size));
    }

    public static String getImageURI(List<Image> images, PreferenceManager preferenceManager, Predicate<Image> condition) {
        String imagePath = "";
        Iterator it = images.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Image image = (Image) it.next();
            if (condition.test(image)) {
                imagePath = image.getPath();
                break;
            }
        }
        if (imagePath.isEmpty()) {
            return "";
        }
        return (String) preferenceManager.getServerPrefs().map(new ImageURIUtils$$Lambda$1(imagePath)).orElse("");
    }

    static final /* synthetic */ String lambda$getImageURI$1$ImageURIUtils(String imageURI, ServerPrefs prefs) {
        if (prefs.getImageServerUrl() == null || prefs.getImageServerUrl().isEmpty()) {
            return "";
        }
        return prefs.getImageServerUrl() + imageURI;
    }
}
