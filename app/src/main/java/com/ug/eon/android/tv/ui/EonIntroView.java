package com.ug.eon.android.tv.ui;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.AttributeSet;
import android.widget.FrameLayout.LayoutParams;
import android.widget.VideoView;
import com.ug.eon.android.tv.R;

/* renamed from: com.ug.eon.android.tv.ui.EonIntroView */
public class EonIntroView extends VideoView {
    public EonIntroView(Context context) {
        super(context);
        setLayoutParams(new LayoutParams(-1, -1));
    }

    public EonIntroView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void play() {
        loadVideo(getContext());
    }

    private void loadVideo(Context context) {
        setVideoPath("android.resource://" + context.getPackageName() + "/" + R.raw.eon_logo_animation);
        setOnPreparedListener(new EonIntroView$$Lambda$0(this));
        setOnCompletionListener(new EonIntroView$$Lambda$1(this));
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void lambda$loadVideo$0$EonIntroView(MediaPlayer mp) {
        start();
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void lambda$loadVideo$1$EonIntroView(MediaPlayer mp) {
        setVisibility(8);
        stopPlayback();
    }
}
