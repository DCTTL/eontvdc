package com.ug.eon.android.tv.searchintegration;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.MatrixCursor;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.exoplayer2.util.MimeTypes;
import com.ug.eon.android.tv.infoserver.InfoServerClient;
import com.ug.eon.android.tv.infoserver.InfoServerClientImpl;
import com.ug.eon.android.tv.infoserver.entities.Asset;
import com.ug.eon.android.tv.infoserver.entities.AssetType;
import com.ug.eon.android.tv.infoserver.entities.Image;
import com.ug.eon.android.tv.prefs.PreferenceManager;
import com.ug.eon.android.tv.prefs.PreferenceManagerImpl;
import com.ug.eon.android.tv.prefs.ServerPrefs;
import com.ug.eon.android.tv.prefs.SharedPrefsProviderImpl;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import org.json.JSONObject;

/* renamed from: com.ug.eon.android.tv.searchintegration.EonSearchProvider */
public class EonSearchProvider extends ContentProvider {
    private static final String PACKAGE_NAME = ((Package) Objects.requireNonNull(EonSearchProvider.class.getPackage())).getName();
    private static PreferenceManager preferenceManager = null;
    private static final List<String> searchDatabaseColumns = Arrays.asList(new String[]{"_id", "suggest_text_1", "suggest_text_2", "suggest_result_card_image", "suggest_production_year", "suggest_duration", "suggest_content_type", "suggest_intent_data"});
    private static final UriMatcher uriMatcher = new UriMatcher(-1);

    static {
        uriMatcher.addURI(PACKAGE_NAME, "livetv/*", AssetType.LIVETV.getOrdinal());
        uriMatcher.addURI(PACKAGE_NAME, "cutv/*", AssetType.CUTV.getOrdinal());
        uriMatcher.addURI(PACKAGE_NAME, "vod/*", AssetType.VOD.getOrdinal());
    }

    private static String remapAssetType(AssetType assetType) {
        if (assetType.equals(AssetType.LIVETV) || assetType.equals(AssetType.CUTV)) {
            return "EVENT";
        }
        return "VOD";
    }

    private static String prepareImagePath(AssetType assetType, List<Image> images) {
        String imageType = null;
        if (assetType.equals(AssetType.LIVETV) || assetType.equals(AssetType.CUTV)) {
            imageType = "EVENT_16_9";
        } else if (assetType.equals(AssetType.VOD)) {
            imageType = "VOD_POSTER_21_31";
        }
        for (Image image : images) {
            if (image.getType().equals(imageType)) {
                return (String) preferenceManager.getServerPrefs().map(new EonSearchProvider$$Lambda$0(image)).orElse(null);
            }
        }
        return null;
    }

    static final /* synthetic */ String lambda$prepareImagePath$0$EonSearchProvider(Image image, ServerPrefs prefs) {
        if (prefs.getImageServerUrl() == null || prefs.getImageServerUrl().isEmpty()) {
            return null;
        }
        return prefs.getImageServerUrl() + image.getPath();
    }

    private void populateCursor(MatrixCursor mc, AssetType searchAsset, List<? extends Asset> assets) {
        for (int i = 0; i < assets.size(); i++) {
            Asset asset = (Asset) assets.get(i);
            JSONObject globalSearchData = new JSONObject();
            try {
                globalSearchData.put("type", remapAssetType(asset.getAssetType()));
                globalSearchData.put(TtmlNode.ATTR_ID, asset.getId());
                globalSearchData.put("channelId", asset.getChannelId());
                mc.addRow(new Object[]{Integer.valueOf(i), asset.getTitle(), asset.getShortDescription(), prepareImagePath(searchAsset, asset.getImages()), Integer.valueOf(asset.getYear()), Long.valueOf(asset.getDuration()), MimeTypes.BASE_TYPE_VIDEO, globalSearchData.toString()});
            } catch (Exception e) {
            }
        }
    }

    public boolean onCreate() {
        preferenceManager = new PreferenceManagerImpl(new SharedPrefsProviderImpl(getContext()));
        return true;
    }

    private InfoServerClient getInfoServerClient() {
        return new InfoServerClientImpl(preferenceManager);
    }

    /* JADX WARNING: type inference failed for: r4v0, types: [java.lang.Throwable] */
    /* JADX WARNING: type inference failed for: r4v1, types: [java.lang.Throwable] */
    /* JADX WARNING: type inference failed for: r4v3 */
    /* JADX WARNING: type inference failed for: r1v1, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r1v2, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r3v24 */
    /* JADX WARNING: type inference failed for: r1v3, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r4v4 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 6 */
     /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.database.Cursor query(@NonNull android.net.Uri r8, @Nullable java.lang.String[] r9, @Nullable java.lang.String r10, @Nullable java.lang.String[] r11, @Nullable java.lang.String r12) {
        /*
            r7 = this;
            r5 = 0
            r4 = 0
            android.content.UriMatcher r3 = uriMatcher
            int r3 = r3.match(r8)
            com.ug.eon.android.tv.infoserver.entities.AssetType r2 = com.ug.eon.android.tv.infoserver.entities.AssetType.valueOf(r3)
            if (r2 != 0) goto L_0x0010
            r1 = r4
        L_0x000f:
            return r1
        L_0x0010:
            android.database.MatrixCursor r1 = new android.database.MatrixCursor
            java.util.List<java.lang.String> r3 = searchDatabaseColumns
            java.lang.String[] r5 = new java.lang.String[r5]
            java.lang.Object[] r3 = r3.toArray(r5)
            java.lang.String[] r3 = (java.lang.String[]) r3
            r1.<init>(r3)
            com.ug.eon.android.tv.infoserver.InfoServerClient r5 = r7.getInfoServerClient()     // Catch:{ Throwable -> 0x006e, all -> 0x008c }
            if (r11 == 0) goto L_0x0037
            r3 = 0
            r3 = r11[r3]     // Catch:{ Throwable -> 0x006e, all -> 0x008c }
        L_0x0028:
            com.ug.eon.android.tv.infoserver.entities.Assets r0 = r5.searchAssets(r3, r2)     // Catch:{ Throwable -> 0x006e, all -> 0x008c }
            if (r0 != 0) goto L_0x0042
            if (r1 == 0) goto L_0x0035
            if (r4 == 0) goto L_0x003e
            r1.close()     // Catch:{ Throwable -> 0x0039 }
        L_0x0035:
            r1 = r4
            goto L_0x000f
        L_0x0037:
            r3 = r4
            goto L_0x0028
        L_0x0039:
            r3 = move-exception
            r4.addSuppressed(r3)
            goto L_0x0035
        L_0x003e:
            r1.close()
            goto L_0x0035
        L_0x0042:
            com.ug.eon.android.tv.infoserver.entities.AssetType r3 = com.ug.eon.android.tv.infoserver.entities.AssetType.LIVETV     // Catch:{ Throwable -> 0x006e, all -> 0x008c }
            boolean r3 = r2.equals(r3)     // Catch:{ Throwable -> 0x006e, all -> 0x008c }
            if (r3 == 0) goto L_0x005e
            java.util.List r3 = r0.getLiveTv()     // Catch:{ Throwable -> 0x006e, all -> 0x008c }
            r7.populateCursor(r1, r2, r3)     // Catch:{ Throwable -> 0x006e, all -> 0x008c }
        L_0x0051:
            if (r1 == 0) goto L_0x000f
            if (r4 == 0) goto L_0x00a1
            r1.close()     // Catch:{ Throwable -> 0x0059 }
            goto L_0x000f
        L_0x0059:
            r3 = move-exception
            r4.addSuppressed(r3)
            goto L_0x000f
        L_0x005e:
            com.ug.eon.android.tv.infoserver.entities.AssetType r3 = com.ug.eon.android.tv.infoserver.entities.AssetType.CUTV     // Catch:{ Throwable -> 0x006e, all -> 0x008c }
            boolean r3 = r2.equals(r3)     // Catch:{ Throwable -> 0x006e, all -> 0x008c }
            if (r3 == 0) goto L_0x007c
            java.util.List r3 = r0.getCuTv()     // Catch:{ Throwable -> 0x006e, all -> 0x008c }
            r7.populateCursor(r1, r2, r3)     // Catch:{ Throwable -> 0x006e, all -> 0x008c }
            goto L_0x0051
        L_0x006e:
            r3 = move-exception
            throw r3     // Catch:{ all -> 0x0070 }
        L_0x0070:
            r4 = move-exception
            r6 = r4
            r4 = r3
            r3 = r6
        L_0x0074:
            if (r1 == 0) goto L_0x007b
            if (r4 == 0) goto L_0x00ab
            r1.close()     // Catch:{ Throwable -> 0x00a6 }
        L_0x007b:
            throw r3
        L_0x007c:
            com.ug.eon.android.tv.infoserver.entities.AssetType r3 = com.ug.eon.android.tv.infoserver.entities.AssetType.VOD     // Catch:{ Throwable -> 0x006e, all -> 0x008c }
            boolean r3 = r2.equals(r3)     // Catch:{ Throwable -> 0x006e, all -> 0x008c }
            if (r3 == 0) goto L_0x008e
            java.util.List r3 = r0.getVod()     // Catch:{ Throwable -> 0x006e, all -> 0x008c }
            r7.populateCursor(r1, r2, r3)     // Catch:{ Throwable -> 0x006e, all -> 0x008c }
            goto L_0x0051
        L_0x008c:
            r3 = move-exception
            goto L_0x0074
        L_0x008e:
            if (r1 == 0) goto L_0x0095
            if (r4 == 0) goto L_0x009d
            r1.close()     // Catch:{ Throwable -> 0x0098 }
        L_0x0095:
            r1 = r4
            goto L_0x000f
        L_0x0098:
            r3 = move-exception
            r4.addSuppressed(r3)
            goto L_0x0095
        L_0x009d:
            r1.close()
            goto L_0x0095
        L_0x00a1:
            r1.close()
            goto L_0x000f
        L_0x00a6:
            r5 = move-exception
            r4.addSuppressed(r5)
            goto L_0x007b
        L_0x00ab:
            r1.close()
            goto L_0x007b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ug.eon.android.tv.searchintegration.EonSearchProvider.query(android.net.Uri, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String):android.database.Cursor");
    }

    @Nullable
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        return null;
    }

    public int delete(@NonNull Uri uri, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }

    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }
}
