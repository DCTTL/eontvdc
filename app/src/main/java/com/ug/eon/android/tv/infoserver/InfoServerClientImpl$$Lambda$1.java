package com.ug.eon.android.tv.infoserver;

import com.ug.eon.android.tv.util.Optional.Function;

/* renamed from: com.ug.eon.android.tv.infoserver.InfoServerClientImpl$$Lambda$1 */
final /* synthetic */ class InfoServerClientImpl$$Lambda$1 implements Function {
    private final InfoServerClientImpl arg$1;
    private final String arg$2;
    private final String arg$3;

    InfoServerClientImpl$$Lambda$1(InfoServerClientImpl infoServerClientImpl, String str, String str2) {
        this.arg$1 = infoServerClientImpl;
        this.arg$2 = str;
        this.arg$3 = str2;
    }

    public Object apply(Object obj) {
        return this.arg$1.lambda$searchAssets$1$InfoServerClientImpl(this.arg$2, this.arg$3, (ISApi) obj);
    }
}
