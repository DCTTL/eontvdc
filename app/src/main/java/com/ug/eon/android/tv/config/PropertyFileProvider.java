package com.ug.eon.android.tv.config;

import android.content.Context;
import java.util.Properties;

/* renamed from: com.ug.eon.android.tv.config.PropertyFileProvider */
public interface PropertyFileProvider {
    Properties getProperties(Context context, String str);
}
