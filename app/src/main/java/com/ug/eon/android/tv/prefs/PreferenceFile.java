package com.ug.eon.android.tv.prefs;

/* renamed from: com.ug.eon.android.tv.prefs.PreferenceFile */
public enum PreferenceFile {
    LOCAL_CACHE,
    DEFAULT,
    PREFERENCE
}
