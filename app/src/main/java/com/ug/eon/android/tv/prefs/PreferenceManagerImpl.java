package com.ug.eon.android.tv.prefs;

import com.google.gson.Gson;
import com.ug.eon.android.tv.util.Optional;

/* renamed from: com.ug.eon.android.tv.prefs.PreferenceManagerImpl */
public class PreferenceManagerImpl implements PreferenceManager {
    private static final String PREF_AUTH = "accessObj";
    private static final String PREF_PLAYER_PRIMARY_AUDIO_LANGUAGE = "firstAudioLanguage";
    private static final String PREF_PLAYER_PRIMARY_SUBTITLE_LANGUAGE = "firstSubtitleLanguage";
    private static final String PREF_PLAYER_SECONDARY_AUDIO_LANGUAGE = "secondAudioLanguage";
    private static final String PREF_PLAYER_SECONDARY_SUBTITLE_LANGUAGE = "secondSubtitleLanguage";
    private static final String PREF_REMOTE_CONFIGURATION = "remoteConfigurationJson";
    private static final String PREF_SERVERS = "servers";
    private static final String PREF_SERVICE_PROVIDER = "serviceProviders";
    private static final String PREF_SUBSCRIBED_FCM_TOPICS = "subscribedFcmTopics";
    private SharedPrefsProvider sharedPrefsProvider;

    public PreferenceManagerImpl(SharedPrefsProvider sharedPrefsProvider2) {
        this.sharedPrefsProvider = sharedPrefsProvider2;
    }

    public Optional<String> getValue(String key) {
        return getValue(PreferenceFile.DEFAULT, key);
    }

    public Optional<Boolean> getBoolean(String key, boolean defaultValue) {
        return Optional.m44of(Boolean.valueOf(this.sharedPrefsProvider.getBoolean(PreferenceFile.PREFERENCE, key, defaultValue)));
    }

    public Optional<Long> getLong(String key) {
        long result = this.sharedPrefsProvider.getLong(PreferenceFile.PREFERENCE, key, -1);
        if (result != -1) {
            return Optional.m44of(Long.valueOf(result));
        }
        return Optional.empty();
    }

    public Optional<String> getValue(PreferenceFile file, String key) {
        String result = this.sharedPrefsProvider.getString(file, key, null);
        if (result != null) {
            return Optional.m44of(result);
        }
        return Optional.empty();
    }

    public Optional<String> getAuthToken() {
        return getAuthPrefs().map(PreferenceManagerImpl$$Lambda$0.$instance);
    }

    public Optional<AuthPrefs> getAuthPrefs() {
        return getPrefVal(PREF_AUTH, AuthPrefs.class);
    }

    public Optional<ServerPrefs> getServerPrefs() {
        return getPrefVal(PREF_SERVERS, ServerPrefs.class);
    }

    public Optional<ConfigPref> getConfigPrefs() {
        return getPrefVal(PREF_REMOTE_CONFIGURATION, ConfigPref.class);
    }

    public Optional<ServiceProviderPrefs> getServiceProviderPrefs() {
        return getPrefVal(PREF_SERVICE_PROVIDER, ServiceProviderPrefs.class);
    }

    public Optional<PlayerLanguages> getPlayerLanguagesPrefs() {
        PlayerLanguages languages = new PlayerLanguages();
        String primaryAudioLang = this.sharedPrefsProvider.getString(PREF_PLAYER_PRIMARY_AUDIO_LANGUAGE, "");
        String secondaryAudioLang = this.sharedPrefsProvider.getString(PREF_PLAYER_SECONDARY_AUDIO_LANGUAGE, "");
        String primarySubtitleLang = this.sharedPrefsProvider.getString(PREF_PLAYER_PRIMARY_SUBTITLE_LANGUAGE, "");
        String secondarySubtitleLang = this.sharedPrefsProvider.getString(PREF_PLAYER_SECONDARY_SUBTITLE_LANGUAGE, "");
        languages.setPrimaryAudioLanguage(primaryAudioLang);
        languages.setSecondaryAudioLanguage(secondaryAudioLang);
        languages.setPrimarySubtitleLanguage(primarySubtitleLang);
        languages.setSecondarySubtitleLanguage(secondarySubtitleLang);
        return Optional.m44of(languages);
    }

    public Optional<String[]> getFcmTopics() {
        return getAuthPrefs().map(PreferenceManagerImpl$$Lambda$1.$instance);
    }

    public Optional<String[]> getAppSubscribedTopics() {
        return getPrefVal(PREF_SUBSCRIBED_FCM_TOPICS, String[].class);
    }

    private <T> Optional<T> getPrefVal(String prefName, Class<T> type) {
        return getValue(prefName).map(new PreferenceManagerImpl$$Lambda$2(type));
    }

    public void setValue(String key, String value) {
        setValue(PreferenceFile.DEFAULT, key, value);
    }

    public void setBoolean(String key, boolean value) {
        this.sharedPrefsProvider.setBoolean(PreferenceFile.PREFERENCE, key, value);
    }

    public void setLong(String key, long value) {
        this.sharedPrefsProvider.setLong(PreferenceFile.PREFERENCE, key, value);
    }

    public void setValue(PreferenceFile file, String key, String value) {
        this.sharedPrefsProvider.setString(file, key, value);
    }

    public void setConfigPrefs(ConfigPref configJson) {
        this.sharedPrefsProvider.setString(PREF_REMOTE_CONFIGURATION, new Gson().toJson((Object) configJson));
    }

    public void remove(String key) {
        this.sharedPrefsProvider.remove(key);
    }
}
