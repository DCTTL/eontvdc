package com.ug.eon.android.tv.searchintegration;

import com.ug.eon.android.tv.infoserver.entities.Image;
import com.ug.eon.android.tv.prefs.ServerPrefs;
import com.ug.eon.android.tv.util.Optional.Function;

/* renamed from: com.ug.eon.android.tv.searchintegration.EonSearchProvider$$Lambda$0 */
final /* synthetic */ class EonSearchProvider$$Lambda$0 implements Function {
    private final Image arg$1;

    EonSearchProvider$$Lambda$0(Image image) {
        this.arg$1 = image;
    }

    public Object apply(Object obj) {
        return EonSearchProvider.lambda$prepareImagePath$0$EonSearchProvider(this.arg$1, (ServerPrefs) obj);
    }
}
