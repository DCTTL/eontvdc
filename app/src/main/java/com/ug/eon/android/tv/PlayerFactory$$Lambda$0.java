package com.ug.eon.android.tv;

import com.ug.eon.android.tv.exoplayer.DrmTokenProvider;
import com.ug.eon.android.tv.util.function.Supplier;

/* renamed from: com.ug.eon.android.tv.PlayerFactory$$Lambda$0 */
final /* synthetic */ class PlayerFactory$$Lambda$0 implements Supplier {
    private final DrmTokenProvider arg$1;

    private PlayerFactory$$Lambda$0(DrmTokenProvider drmTokenProvider) {
        this.arg$1 = drmTokenProvider;
    }

    static Supplier get$Lambda(DrmTokenProvider drmTokenProvider) {
        return new PlayerFactory$$Lambda$0(drmTokenProvider);
    }

    public Object get() {
        return this.arg$1.getDrmInfoWidevine();
    }
}
