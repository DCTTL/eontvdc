package com.ug.eon.android.tv.util.filesystem;

import android.content.Context;
import com.ug.eon.android.tv.util.LogUC;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.Path;
import org.apache.commons.io.FileUtils;

/* renamed from: com.ug.eon.android.tv.util.filesystem.DefaultFileReaderWriter */
public class DefaultFileReaderWriter implements FileReaderWriter {
    private static final String TAG = DefaultFileReaderWriter.class.getName();
    private Context mContext;

    public DefaultFileReaderWriter(Context context) {
        this.mContext = context;
    }

    public void writeFile(Path filePath, byte[] data) {
        if (!filePath.isAbsolute()) {
            LogUC.m43w(TAG, "Path is relative, cannot create");
            return;
        }
        try {
            internalWriteFile(createFile(filePath), data);
        } catch (IOException e) {
            LogUC.m40e(TAG, "File write failed: " + e.toString());
        }
    }

    public void writeFile(Path filePath, String data) {
        writeFile(filePath, data.getBytes());
    }

    public void writeFileToCache(Path filePath, byte[] data) {
        if (filePath.isAbsolute()) {
            LogUC.m43w(TAG, "Path is absolute, cannot create in cache");
            return;
        }
        try {
            internalWriteFile(createFileInCache(filePath), data);
        } catch (IOException e) {
            LogUC.m40e(TAG, "File write failed: " + e.toString());
        }
    }

    public void writeFileToCache(Path filePath, String data) {
        writeFileToCache(filePath, data.getBytes());
    }

    public void writeFileToCache(String filePath, byte[] data) {
        try {
            internalWriteFile(createFileInCache(filePath), data);
        } catch (IOException e) {
            LogUC.m40e(TAG, "File write failed: " + e.toString());
        }
    }

    public void writeFileToCache(String filePath, String data) {
        writeFileToCache(filePath, data.getBytes());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0026, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0027, code lost:
        if (r0 != null) goto L_0x0029;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0029, code lost:
        if (r2 != null) goto L_0x002b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x002e, code lost:
        throw r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x002f, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0030, code lost:
        r2.addSuppressed(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0034, code lost:
        r0.close();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void internalWriteFile(java.io.File r5, byte[] r6) throws java.io.IOException {
        /*
            r4 = this;
            if (r5 != 0) goto L_0x000a
            java.lang.String r1 = TAG
            java.lang.String r2 = "file is null"
            com.ug.eon.android.tv.util.LogUC.m43w(r1, r2)
        L_0x0009:
            return
        L_0x000a:
            java.io.FileOutputStream r0 = new java.io.FileOutputStream
            r0.<init>(r5)
            r2 = 0
            r0.write(r6)     // Catch:{ Throwable -> 0x0024 }
            if (r0 == 0) goto L_0x0009
            if (r2 == 0) goto L_0x0020
            r0.close()     // Catch:{ Throwable -> 0x001b }
            goto L_0x0009
        L_0x001b:
            r1 = move-exception
            r2.addSuppressed(r1)
            goto L_0x0009
        L_0x0020:
            r0.close()
            goto L_0x0009
        L_0x0024:
            r2 = move-exception
            throw r2     // Catch:{ all -> 0x0026 }
        L_0x0026:
            r1 = move-exception
            if (r0 == 0) goto L_0x002e
            if (r2 == 0) goto L_0x0034
            r0.close()     // Catch:{ Throwable -> 0x002f }
        L_0x002e:
            throw r1
        L_0x002f:
            r3 = move-exception
            r2.addSuppressed(r3)
            goto L_0x002e
        L_0x0034:
            r0.close()
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ug.eon.android.tv.util.filesystem.DefaultFileReaderWriter.internalWriteFile(java.io.File, byte[]):void");
    }

    private File createFileInCache(String filePath) {
        if (filePath == null || filePath.isEmpty()) {
            LogUC.m43w(TAG, "file name is empty");
            return null;
        }
        if (!filePath.startsWith("/")) {
            filePath = "/" + filePath;
        }
        File file = new File(this.mContext.getCacheDir(), filePath);
        file.getParentFile().mkdirs();
        if (!file.getParentFile().exists()) {
            return null;
        }
        return file;
    }

    private File createFileInCache(Path filePath) {
        File file = this.mContext.getCacheDir().toPath().resolve(filePath).toFile();
        file.getParentFile().mkdirs();
        if (file.getParentFile().exists()) {
            return file;
        }
        return null;
    }

    private File createFile(Path filePath) {
        File file = filePath.toFile();
        file.getParentFile().mkdirs();
        if (file.getParentFile().exists()) {
            return file;
        }
        return null;
    }

    public String readFile(String filePath) {
        FileInputStream inputStream;
        Throwable th;
        StringBuilder stringBuilder = new StringBuilder();
        try {
            inputStream = new FileInputStream(new File(this.mContext.getCacheDir(), filePath));
            th = null;
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String str = "";
                while (true) {
                    String receiveString = bufferedReader.readLine();
                    if (receiveString == null) {
                        break;
                    }
                    stringBuilder.append(receiveString);
                }
                if (inputStream != null) {
                    if (th != null) {
                        try {
                            inputStream.close();
                        } catch (Throwable th2) {
                            th.addSuppressed(th2);
                        }
                    } else {
                        inputStream.close();
                    }
                }
            } catch (Throwable th3) {
                Throwable th4 = th3;
                th = r7;
                th = th4;
            }
        } catch (FileNotFoundException e) {
            LogUC.m40e(TAG, "File not found: " + e.toString());
        } catch (IOException e2) {
            LogUC.m40e(TAG, "Can not read file: " + e2.toString());
        }
        return stringBuilder.toString();
        if (inputStream != null) {
            if (th != null) {
                try {
                    inputStream.close();
                } catch (Throwable th5) {
                    th.addSuppressed(th5);
                }
            } else {
                inputStream.close();
            }
        }
        throw th;
        throw th;
    }

    public void copyFolder(Path srcPath, Path outputPath) throws IOException {
        FileUtils.copyDirectory(srcPath.toFile(), outputPath.toFile());
    }

    public void deleteFolder(Path path) throws IOException {
        FileUtils.deleteDirectory(path.toFile());
    }

    public void deleteFile(Path logFilePath) throws IOException {
        FileUtils.forceDelete(logFilePath.toFile());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0040, code lost:
        r14 = r9;
        r9 = r8;
        r8 = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0080, code lost:
        r8 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0081, code lost:
        r9 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0090, code lost:
        r12 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:?, code lost:
        r9.addSuppressed(r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x003f, code lost:
        r9 = move-exception;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0080 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:1:0x002b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void zipFolder(java.nio.file.Path r16, java.nio.file.Path r17) throws java.io.IOException {
        /*
            r15 = this;
            java.lang.String r8 = TAG
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r10 = "Zipping to file: "
            java.lang.StringBuilder r9 = r9.append(r10)
            java.nio.file.Path r10 = r17.toAbsolutePath()
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r9 = r9.toString()
            com.ug.eon.android.tv.util.LogUC.m39d(r8, r9)
            java.io.FileOutputStream r4 = new java.io.FileOutputStream
            java.io.File r8 = r17.toFile()
            r4.<init>(r8)
            java.util.zip.ZipOutputStream r7 = new java.util.zip.ZipOutputStream
            r7.<init>(r4)
            r10 = 0
            java.io.File r6 = r16.toFile()     // Catch:{ Throwable -> 0x003d, all -> 0x0080 }
            java.io.File[] r2 = r6.listFiles()     // Catch:{ Throwable -> 0x003d, all -> 0x0080 }
            if (r2 != 0) goto L_0x004b
            java.io.IOException r8 = new java.io.IOException     // Catch:{ Throwable -> 0x003d, all -> 0x0080 }
            java.lang.String r9 = "File list empty, nothing to zip"
            r8.<init>(r9)     // Catch:{ Throwable -> 0x003d, all -> 0x0080 }
            throw r8     // Catch:{ Throwable -> 0x003d, all -> 0x0080 }
        L_0x003d:
            r8 = move-exception
            throw r8     // Catch:{ all -> 0x003f }
        L_0x003f:
            r9 = move-exception
            r14 = r9
            r9 = r8
            r8 = r14
        L_0x0043:
            if (r7 == 0) goto L_0x004a
            if (r9 == 0) goto L_0x00b8
            r7.close()     // Catch:{ Throwable -> 0x00b3 }
        L_0x004a:
            throw r8
        L_0x004b:
            int r11 = r2.length     // Catch:{ Throwable -> 0x003d, all -> 0x0080 }
            r8 = 0
        L_0x004d:
            if (r8 >= r11) goto L_0x00a2
            r1 = r2[r8]     // Catch:{ Throwable -> 0x003d, all -> 0x0080 }
            r9 = 1024(0x400, float:1.435E-42)
            byte[] r0 = new byte[r9]     // Catch:{ Throwable -> 0x003d, all -> 0x0080 }
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ Throwable -> 0x003d, all -> 0x0080 }
            r3.<init>(r1)     // Catch:{ Throwable -> 0x003d, all -> 0x0080 }
            r9 = 0
            java.util.zip.ZipEntry r12 = new java.util.zip.ZipEntry     // Catch:{ Throwable -> 0x0072, all -> 0x00bc }
            java.lang.String r13 = r1.getName()     // Catch:{ Throwable -> 0x0072, all -> 0x00bc }
            r12.<init>(r13)     // Catch:{ Throwable -> 0x0072, all -> 0x00bc }
            r7.putNextEntry(r12)     // Catch:{ Throwable -> 0x0072, all -> 0x00bc }
        L_0x0067:
            int r5 = r3.read(r0)     // Catch:{ Throwable -> 0x0072, all -> 0x00bc }
            if (r5 <= 0) goto L_0x0083
            r12 = 0
            r7.write(r0, r12, r5)     // Catch:{ Throwable -> 0x0072, all -> 0x00bc }
            goto L_0x0067
        L_0x0072:
            r8 = move-exception
            throw r8     // Catch:{ all -> 0x0074 }
        L_0x0074:
            r9 = move-exception
            r14 = r9
            r9 = r8
            r8 = r14
        L_0x0078:
            if (r3 == 0) goto L_0x007f
            if (r9 == 0) goto L_0x009e
            r3.close()     // Catch:{ Throwable -> 0x0099, all -> 0x0080 }
        L_0x007f:
            throw r8     // Catch:{ Throwable -> 0x003d, all -> 0x0080 }
        L_0x0080:
            r8 = move-exception
            r9 = r10
            goto L_0x0043
        L_0x0083:
            r7.closeEntry()     // Catch:{ Throwable -> 0x0072, all -> 0x00bc }
            if (r3 == 0) goto L_0x008d
            if (r9 == 0) goto L_0x0095
            r3.close()     // Catch:{ Throwable -> 0x0090, all -> 0x0080 }
        L_0x008d:
            int r8 = r8 + 1
            goto L_0x004d
        L_0x0090:
            r12 = move-exception
            r9.addSuppressed(r12)     // Catch:{ Throwable -> 0x003d, all -> 0x0080 }
            goto L_0x008d
        L_0x0095:
            r3.close()     // Catch:{ Throwable -> 0x003d, all -> 0x0080 }
            goto L_0x008d
        L_0x0099:
            r11 = move-exception
            r9.addSuppressed(r11)     // Catch:{ Throwable -> 0x003d, all -> 0x0080 }
            goto L_0x007f
        L_0x009e:
            r3.close()     // Catch:{ Throwable -> 0x003d, all -> 0x0080 }
            goto L_0x007f
        L_0x00a2:
            if (r7 == 0) goto L_0x00a9
            if (r10 == 0) goto L_0x00af
            r7.close()     // Catch:{ Throwable -> 0x00aa }
        L_0x00a9:
            return
        L_0x00aa:
            r8 = move-exception
            r10.addSuppressed(r8)
            goto L_0x00a9
        L_0x00af:
            r7.close()
            goto L_0x00a9
        L_0x00b3:
            r10 = move-exception
            r9.addSuppressed(r10)
            goto L_0x004a
        L_0x00b8:
            r7.close()
            goto L_0x004a
        L_0x00bc:
            r8 = move-exception
            goto L_0x0078
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ug.eon.android.tv.util.filesystem.DefaultFileReaderWriter.zipFolder(java.nio.file.Path, java.nio.file.Path):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0043, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        r3.addSuppressed(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x004a, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x004b, code lost:
        r5 = r3;
        r3 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0059, code lost:
        r3 = th;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0059 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:9:0x0022] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void copyFileFromContentProvider(java.lang.String r8, java.nio.file.Path r9) throws java.io.IOException {
        /*
            r7 = this;
            r5 = 0
            android.content.Context r3 = r7.mContext
            android.content.ContentResolver r3 = r3.getContentResolver()
            android.net.Uri r4 = android.net.Uri.parse(r8)
            java.io.InputStream r0 = r3.openInputStream(r4)
            if (r0 != 0) goto L_0x0022
            if (r0 == 0) goto L_0x0018
            if (r5 == 0) goto L_0x001e
            r0.close()     // Catch:{ Throwable -> 0x0019 }
        L_0x0018:
            return
        L_0x0019:
            r3 = move-exception
            r5.addSuppressed(r3)
            goto L_0x0018
        L_0x001e:
            r0.close()
            goto L_0x0018
        L_0x0022:
            java.io.File r2 = r7.createFile(r9)     // Catch:{ Throwable -> 0x0048, all -> 0x0059 }
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Throwable -> 0x0048, all -> 0x0059 }
            r1.<init>(r2)     // Catch:{ Throwable -> 0x0048, all -> 0x0059 }
            r3 = 0
            r7.copyStream(r0, r1)     // Catch:{ Throwable -> 0x005b, all -> 0x007c }
            if (r1 == 0) goto L_0x0036
            if (r5 == 0) goto L_0x0055
            r1.close()     // Catch:{ Throwable -> 0x0043, all -> 0x0059 }
        L_0x0036:
            if (r0 == 0) goto L_0x0018
            if (r5 == 0) goto L_0x006f
            r0.close()     // Catch:{ Throwable -> 0x003e }
            goto L_0x0018
        L_0x003e:
            r3 = move-exception
            r5.addSuppressed(r3)
            goto L_0x0018
        L_0x0043:
            r4 = move-exception
            r3.addSuppressed(r4)     // Catch:{ Throwable -> 0x0048, all -> 0x0059 }
            goto L_0x0036
        L_0x0048:
            r3 = move-exception
            throw r3     // Catch:{ all -> 0x004a }
        L_0x004a:
            r4 = move-exception
            r5 = r3
            r3 = r4
        L_0x004d:
            if (r0 == 0) goto L_0x0054
            if (r5 == 0) goto L_0x0078
            r0.close()     // Catch:{ Throwable -> 0x0073 }
        L_0x0054:
            throw r3
        L_0x0055:
            r1.close()     // Catch:{ Throwable -> 0x0048, all -> 0x0059 }
            goto L_0x0036
        L_0x0059:
            r3 = move-exception
            goto L_0x004d
        L_0x005b:
            r4 = move-exception
            throw r4     // Catch:{ all -> 0x005d }
        L_0x005d:
            r3 = move-exception
        L_0x005e:
            if (r1 == 0) goto L_0x0065
            if (r4 == 0) goto L_0x006b
            r1.close()     // Catch:{ Throwable -> 0x0066, all -> 0x0059 }
        L_0x0065:
            throw r3     // Catch:{ Throwable -> 0x0048, all -> 0x0059 }
        L_0x0066:
            r6 = move-exception
            r4.addSuppressed(r6)     // Catch:{ Throwable -> 0x0048, all -> 0x0059 }
            goto L_0x0065
        L_0x006b:
            r1.close()     // Catch:{ Throwable -> 0x0048, all -> 0x0059 }
            goto L_0x0065
        L_0x006f:
            r0.close()
            goto L_0x0018
        L_0x0073:
            r4 = move-exception
            r5.addSuppressed(r4)
            goto L_0x0054
        L_0x0078:
            r0.close()
            goto L_0x0054
        L_0x007c:
            r3 = move-exception
            r4 = r5
            goto L_0x005e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ug.eon.android.tv.util.filesystem.DefaultFileReaderWriter.copyFileFromContentProvider(java.lang.String, java.nio.file.Path):void");
    }

    private void copyStream(InputStream is, OutputStream os) throws IOException {
        byte[] buf = new byte[1024];
        while (true) {
            int num = is.read(buf);
            if (num != -1) {
                os.write(buf, 0, num);
            } else {
                return;
            }
        }
    }
}
