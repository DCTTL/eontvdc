package com.ug.eon.android.tv.exoplayer;

import android.text.TextUtils;
import com.google.android.exoplayer2.C;
import com.ug.eon.android.tv.drm.DrmInfoProvider;
import com.ug.eon.android.tv.prefs.PreferenceManager;
import com.ug.eon.android.tv.util.LogUC;

/* renamed from: com.ug.eon.android.tv.exoplayer.DrmTokenProvider */
public class DrmTokenProvider {
    private static final String TAG = DrmTokenProvider.class.getName();
    private DrmInfoProvider mDrmInfoProvider;
    private PreferenceManager mPreferenceManager;
    private String mWidevineLicenceServerUrl = getWidevineLicenceServerUrl();

    public DrmTokenProvider(PreferenceManager preferenceManager, DrmInfoProvider drmInfoProvider) {
        this.mPreferenceManager = preferenceManager;
        this.mDrmInfoProvider = drmInfoProvider;
    }

    public DrmInfo getDrmInfoWidevine() {
        String drmToken = this.mDrmInfoProvider.getDrmInfo();
        if (drmToken == null) {
            LogUC.m43w(TAG, "DRM info is empty. Cancelling current action..");
            return null;
        }
        LogUC.m39d(TAG, "drmToken: " + drmToken);
        LogUC.m39d(TAG, "licence server url: " + getWidevineLicenceServerUrl());
        DrmInfo drmInfo = new DrmInfo(C.WIDEVINE_UUID, getWidevineLicenceServerUrl());
        drmInfo.setDrmKeyProperties(new String[]{"Conax-Custom-Data", drmToken});
        return drmInfo;
    }

    private String getWidevineLicenceServerUrl() {
        if (!TextUtils.isEmpty(this.mWidevineLicenceServerUrl)) {
            return this.mWidevineLicenceServerUrl;
        }
        this.mWidevineLicenceServerUrl = (String) this.mPreferenceManager.getServiceProviderPrefs().map(DrmTokenProvider$$Lambda$0.$instance).orElse(null);
        return this.mWidevineLicenceServerUrl;
    }
}
