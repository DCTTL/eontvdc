package com.ug.eon.android.tv;

import com.ug.eon.android.tv.util.function.BooleanSupplier;

/* renamed from: com.ug.eon.android.tv.TvActivity$$Lambda$0 */
final /* synthetic */ class TvActivity$$Lambda$0 implements BooleanSupplier {
    private final TvActivity arg$1;

    TvActivity$$Lambda$0(TvActivity tvActivity) {
        this.arg$1 = tvActivity;
    }

    public boolean getAsBoolean() {
        return this.arg$1.bridge$lambda$0$TvActivity();
    }
}
