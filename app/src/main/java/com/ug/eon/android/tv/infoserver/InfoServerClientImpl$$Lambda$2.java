package com.ug.eon.android.tv.infoserver;

import com.ug.eon.android.tv.util.Optional.Function;

/* renamed from: com.ug.eon.android.tv.infoserver.InfoServerClientImpl$$Lambda$2 */
final /* synthetic */ class InfoServerClientImpl$$Lambda$2 implements Function {
    private final InfoServerClientImpl arg$1;

    InfoServerClientImpl$$Lambda$2(InfoServerClientImpl infoServerClientImpl) {
        this.arg$1 = infoServerClientImpl;
    }

    public Object apply(Object obj) {
        return this.arg$1.lambda$getWatchNextItems$2$InfoServerClientImpl((ISApi) obj);
    }
}
