package com.ug.eon.android.tv.ui;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;

/* renamed from: com.ug.eon.android.tv.ui.EonIntroView$$Lambda$0 */
final /* synthetic */ class EonIntroView$$Lambda$0 implements OnPreparedListener {
    private final EonIntroView arg$1;

    EonIntroView$$Lambda$0(EonIntroView eonIntroView) {
        this.arg$1 = eonIntroView;
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        this.arg$1.lambda$loadVideo$0$EonIntroView(mediaPlayer);
    }
}
