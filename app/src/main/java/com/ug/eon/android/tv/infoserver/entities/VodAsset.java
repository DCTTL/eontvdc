package com.ug.eon.android.tv.infoserver.entities;

/* renamed from: com.ug.eon.android.tv.infoserver.entities.VodAsset */
public class VodAsset extends Asset {
    private long duration;
    private int year;

    public VodAsset() {
        super(AssetType.VOD);
    }

    public int getYear() {
        return this.year;
    }

    public long getDuration() {
        return this.duration;
    }
}
