package com.ug.eon.android.tv.prefs;

/* renamed from: com.ug.eon.android.tv.prefs.SharedPrefsProvider */
public interface SharedPrefsProvider {
    boolean getBoolean(PreferenceFile preferenceFile, String str, boolean z);

    long getLong(PreferenceFile preferenceFile, String str, long j);

    String getString(PreferenceFile preferenceFile, String str, String str2);

    String getString(String str, String str2);

    void remove(PreferenceFile preferenceFile, String str);

    void remove(String str);

    void setBoolean(PreferenceFile preferenceFile, String str, boolean z);

    void setLong(PreferenceFile preferenceFile, String str, long j);

    void setString(PreferenceFile preferenceFile, String str, String str2);

    void setString(String str, String str2);
}
