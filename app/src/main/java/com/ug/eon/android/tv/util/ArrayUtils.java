package com.ug.eon.android.tv.util;

import java.util.HashMap;
import java.util.Map;

/* renamed from: com.ug.eon.android.tv.util.ArrayUtils */
public class ArrayUtils {
    private ArrayUtils() {
    }

    public static <T> Map<T, DifferenceResult> symmetricDifference(T[] arrayFirst, T[] arraySecond) {
        Map<T, DifferenceResult> syncMap = new HashMap<>();
        for (T first : arrayFirst) {
            syncMap.put(first, DifferenceResult.FIRST_SET);
        }
        for (T second : arraySecond) {
            syncMap.put(second, syncMap.containsKey(second) ? DifferenceResult.BOTH : DifferenceResult.SECOND_SET);
        }
        return syncMap;
    }
}
