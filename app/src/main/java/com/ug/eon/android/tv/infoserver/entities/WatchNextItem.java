package com.ug.eon.android.tv.infoserver.entities;

import java.util.List;

/* renamed from: com.ug.eon.android.tv.infoserver.entities.WatchNextItem */
public class WatchNextItem {
    private WatchNextItemPayload payload;
    private String type;

    /* renamed from: com.ug.eon.android.tv.infoserver.entities.WatchNextItem$WatchNextItemPayload */
    public class WatchNextItemPayload {
        private int channelId;
        private List<Image> channelLogos;
        private String endTime;

        /* renamed from: id */
        private int f35id;
        private List<Image> images;
        private String shortDescription;
        private String startTime;
        private String title;

        public WatchNextItemPayload() {
        }

        public int getId() {
            return this.f35id;
        }

        public int getChannelId() {
            return this.channelId;
        }

        public String getTitle() {
            return this.title;
        }

        public String getShortDescription() {
            return this.shortDescription;
        }

        public String getStartTime() {
            return this.startTime;
        }

        public String getEndTime() {
            return this.endTime;
        }

        public List<Image> getImages() {
            return this.images;
        }

        public List<Image> getChannelLogos() {
            return this.channelLogos;
        }
    }

    public String getType() {
        return this.type;
    }

    public WatchNextItemPayload getPayload() {
        return this.payload;
    }
}
