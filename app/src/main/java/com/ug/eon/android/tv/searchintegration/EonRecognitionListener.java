package com.ug.eon.android.tv.searchintegration;

import android.os.Bundle;
import android.speech.RecognitionListener;
import com.ug.eon.android.tv.util.LogUC;
import java.util.ArrayList;

/* renamed from: com.ug.eon.android.tv.searchintegration.EonRecognitionListener */
public class EonRecognitionListener implements RecognitionListener {
    private static final String TAG = EonRecognitionListener.class.getName();
    private String currentResult = null;
    private Action onEndOfSpeech;
    private Consumer<Integer> onError;
    private Consumer<String> onFinalResult;
    private Consumer<String> onPartialResult;
    private Action onReadyForSpeech;
    private Consumer<Float> onRmsChanged;

    public EonRecognitionListener(Consumer<String> finalResult, Consumer<String> partialResult, Action readyForSpeech, Action endOfSpeech, Consumer<Integer> error, Consumer<Float> rmsChanged) {
        this.onFinalResult = finalResult;
        this.onReadyForSpeech = readyForSpeech;
        this.onEndOfSpeech = endOfSpeech;
        this.onError = error;
        this.onRmsChanged = rmsChanged;
        this.onPartialResult = partialResult;
    }

    public void onReadyForSpeech(Bundle params) {
        LogUC.m39d(TAG, "onReadyForSpeech");
        if (this.onReadyForSpeech != null) {
            this.onReadyForSpeech.apply();
        }
    }

    public void onBeginningOfSpeech() {
        LogUC.m39d(TAG, "onBeginningOfSpeech");
    }

    public void onRmsChanged(float rmsdB) {
        LogUC.m39d(TAG, "onRmsChanged: " + rmsdB);
        this.onRmsChanged.accept(Float.valueOf(rmsdB));
    }

    public void onBufferReceived(byte[] buffer) {
        LogUC.m39d(TAG, "onBufferReceived");
    }

    public void onEndOfSpeech() {
        LogUC.m39d(TAG, "onEndOfSpeech");
        if (this.onEndOfSpeech != null) {
            this.onEndOfSpeech.apply();
        }
    }

    public void onError(int error) {
        LogUC.m39d(TAG, "error " + error);
        if (this.onError != null) {
            this.onError.accept(Integer.valueOf(error));
        }
    }

    public void onResults(Bundle results) {
        ArrayList<String> matches = results.getStringArrayList("results_recognition");
        if (matches != null && !matches.isEmpty()) {
            this.currentResult = (String) matches.get(0);
            LogUC.m39d(TAG, "onResults " + this.currentResult);
            if (this.onFinalResult != null) {
                this.onFinalResult.accept(this.currentResult);
            }
        }
    }

    public void onPartialResults(Bundle partialResults) {
        ArrayList<String> matches = partialResults.getStringArrayList("results_recognition");
        if (matches != null && !matches.isEmpty()) {
            this.currentResult = (String) matches.get(0);
            LogUC.m39d(TAG, "onPartialResults " + this.currentResult);
            if (this.onPartialResult != null) {
                this.onPartialResult.accept(this.currentResult);
            }
        }
    }

    public void onEvent(int eventType, Bundle params) {
        LogUC.m39d(TAG, "onEvent " + eventType);
    }
}
