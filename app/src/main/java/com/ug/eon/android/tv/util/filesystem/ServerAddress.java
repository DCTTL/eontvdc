package com.ug.eon.android.tv.util.filesystem;

/* renamed from: com.ug.eon.android.tv.util.filesystem.ServerAddress */
public enum ServerAddress {
    IMAGE_SERVER,
    STATIC_SERVER
}
