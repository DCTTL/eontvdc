package com.ug.eon.android.tv.infoserver.entities;

import java.util.List;

/* renamed from: com.ug.eon.android.tv.infoserver.entities.Asset */
public class Asset {
    private transient AssetType assetType;

    /* renamed from: id */
    private long f32id;
    private List<Image> images;
    private String shortDescription;
    private String title;

    public Asset(AssetType assetType2) {
        this.assetType = assetType2;
    }

    public long getId() {
        return this.f32id;
    }

    public AssetType getAssetType() {
        return this.assetType;
    }

    public String getTitle() {
        return this.title;
    }

    public String getShortDescription() {
        return this.shortDescription;
    }

    public List<Image> getImages() {
        return this.images;
    }

    public int getChannelId() {
        return 0;
    }

    public int getYear() {
        return 0;
    }

    public long getDuration() {
        return 0;
    }

    public List<Image> getChannelLogos() {
        return null;
    }
}
