package com.ug.eon.android.tv.exoplayer;

import com.google.android.exoplayer2.drm.DrmSession;
import com.google.android.exoplayer2.drm.DrmSession.DrmSessionException;
import com.google.android.exoplayer2.drm.ExoMediaCrypto;
import com.google.android.exoplayer2.util.Assertions;
import java.util.Map;

/* renamed from: com.ug.eon.android.tv.exoplayer.ErrorStateDrmSession */
final class ErrorStateDrmSession<T extends ExoMediaCrypto> implements DrmSession<T> {
    private final DrmSessionException error;

    public ErrorStateDrmSession(DrmSessionException error2) {
        this.error = (DrmSessionException) Assertions.checkNotNull(error2);
    }

    public int getState() {
        return DrmSession.STATE_ERROR;
    }

    public DrmSessionException getError() {
        return this.error;
    }

    public T getMediaCrypto() {
        return null;
    }

    public Map<String, String> queryKeyStatus() {
        return null;
    }

    public byte[] getOfflineLicenseKeySetId() {
        return null;
    }
}
