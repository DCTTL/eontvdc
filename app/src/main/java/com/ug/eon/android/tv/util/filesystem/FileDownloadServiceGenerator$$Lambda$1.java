package com.ug.eon.android.tv.util.filesystem;

import com.ug.eon.android.tv.prefs.ServerPrefs;
import com.ug.eon.android.tv.util.Optional.Function;

/* renamed from: com.ug.eon.android.tv.util.filesystem.FileDownloadServiceGenerator$$Lambda$1 */
final /* synthetic */ class FileDownloadServiceGenerator$$Lambda$1 implements Function {
    static final Function $instance = new FileDownloadServiceGenerator$$Lambda$1();

    private FileDownloadServiceGenerator$$Lambda$1() {
    }

    public Object apply(Object obj) {
        return FileDownloadServiceGenerator.lambda$createFileDownloadClient$1$FileDownloadServiceGenerator((ServerPrefs) obj);
    }
}
