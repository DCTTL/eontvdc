package com.ug.eon.android.tv.util;

import android.os.Build.VERSION;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.view.Display;
import android.view.Display.Mode;
import android.view.Window;
import android.view.WindowManager.LayoutParams;

/* renamed from: com.ug.eon.android.tv.util.DisplaySettings */
public class DisplaySettings {
    private static final float PREFERRED_REFRESH_RATE = 50.0f;
    private static final String TAG = DisplaySettings.class.getName();

    private DisplaySettings() {
    }

    public static void forceRefreshRate(@NonNull Display display, @NonNull Window window) {
        LayoutParams layoutParams = window.getAttributes();
        if (VERSION.SDK_INT >= 23) {
            Mode forceMode = getPreferredMode(display);
            LogUC.m39d(TAG, "current mode: " + display.getMode().toString());
            if (forceMode != null) {
                LogUC.m39d(TAG, "force mode: " + forceMode.toString());
                layoutParams.preferredDisplayModeId = forceMode.getModeId();
            } else {
                return;
            }
        } else {
            LogUC.m39d(TAG, "current refresh mode: " + layoutParams.preferredRefreshRate);
            layoutParams.preferredRefreshRate = PREFERRED_REFRESH_RATE;
        }
        window.setAttributes(layoutParams);
    }

    @Nullable
    @RequiresApi(api = 23)
    private static Mode getPreferredMode(@NonNull Display display) {
        Mode[] supportedModes;
        Mode currentMode = display.getMode();
        if (currentMode.getRefreshRate() == PREFERRED_REFRESH_RATE) {
            LogUC.m39d(TAG, "already set 50Hz");
            return null;
        }
        for (Mode mode : display.getSupportedModes()) {
            LogUC.m39d(TAG, "supported mode: " + mode.toString());
            if (mode.getRefreshRate() == PREFERRED_REFRESH_RATE && mode.getPhysicalWidth() == currentMode.getPhysicalWidth() && mode.getPhysicalHeight() == currentMode.getPhysicalHeight()) {
                return mode;
            }
        }
        return null;
    }
}
