package com.ug.eon.android.tv.util.filesystem;

/* renamed from: com.ug.eon.android.tv.util.filesystem.FileDownloader */
public interface FileDownloader {
    boolean downloadFile(String str, String str2);
}
