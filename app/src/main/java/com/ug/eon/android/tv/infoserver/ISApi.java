package com.ug.eon.android.tv.infoserver;

import com.google.gson.JsonObject;
import com.ug.eon.android.tv.infoserver.entities.Assets;
import com.ug.eon.android.tv.infoserver.entities.DrmToken;
import com.ug.eon.android.tv.infoserver.entities.LauncherRouteItem;
import com.ug.eon.android.tv.infoserver.entities.WatchNextItem;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

/* renamed from: com.ug.eon.android.tv.infoserver.ISApi */
public interface ISApi {
    @POST("drm/token")
    Call<DrmToken> getDrmToken(@Header("Authorization") String str);

    @GET("drm")
    Call<DrmToken> getDrmToken(@Query("i") String str, @Query("a") String str2, @Query("u") String str3, @Query("sp") String str4);

    @GET("launcher/routes")
    Call<List<LauncherRouteItem>> getLauncherRouteItems(@Header("Authorization") String str);

    @GET("launcher/watchnext")
    Call<List<WatchNextItem>> getWatchNextItems(@Header("Authorization") String str);

    @POST("devices/stb")
    Call<JsonObject> registerStbDevice(@Header("Authorization") String str, @Body JsonObject jsonObject);

    @GET("search")
    Call<Assets> searchAssets(@Header("Authorization") String str, @Query("q") String str2, @Query("sort") String str3, @Query("assetType") String str4);
}
