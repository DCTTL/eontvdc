package com.ug.eon.android.tv.util;

import android.view.InputEvent;
import android.view.MotionEvent;

/* renamed from: com.ug.eon.android.tv.util.Dpad */
public class Dpad {
    int directionPressed = -1;

    public int getDirectionPressed(InputEvent event) {
        if (event instanceof MotionEvent) {
            MotionEvent motionEvent = (MotionEvent) event;
            float xaxis = motionEvent.getAxisValue(15);
            float yaxis = motionEvent.getAxisValue(16);
            if (Float.compare(xaxis, -1.0f) == 0) {
                this.directionPressed = 21;
            } else if (Float.compare(xaxis, 1.0f) == 0) {
                this.directionPressed = 22;
            } else if (Float.compare(yaxis, -1.0f) == 0) {
                this.directionPressed = 19;
            } else if (Float.compare(yaxis, 1.0f) == 0) {
                this.directionPressed = 20;
            }
        } else {
            this.directionPressed = -1;
        }
        LogUC.m41i("Button clicked", this.directionPressed + "");
        LogUC.m41i("Button clicked", event.toString());
        return this.directionPressed;
    }
}
