package com.ug.eon.android.tv.web;

/* renamed from: com.ug.eon.android.tv.web.AuthFailedHandler */
public interface AuthFailedHandler {
    void onAuthFailed();
}
