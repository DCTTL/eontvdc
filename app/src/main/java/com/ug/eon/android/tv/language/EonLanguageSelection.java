package com.ug.eon.android.tv.language;

import com.ug.eon.android.tv.util.LogUC;

/* renamed from: com.ug.eon.android.tv.language.EonLanguageSelection */
public enum EonLanguageSelection {
    BOSNIAN,
    CROATIAN,
    DUTCH,
    ENGLISH,
    FRENCH,
    GERMAN,
    HUNGARIAN,
    ITALIAN,
    MACEDONIAN,
    MONTENEGRIN,
    PORTUGUESE,
    ROMANIAN,
    SLOVENIAN,
    SERBIAN,
    SPANISH,
    NO_SUBTITLES,
    UNKNOWN;

    public static EonLanguageSelection fromISO639(String lang) {
        if (lang == null) {
            LogUC.m43w("EonLanguageSelection", "input string is null");
            return UNKNOWN;
        }
        String lowerCaseLang = lang.toLowerCase();
        if (lowerCaseLang.startsWith("sr") || lowerCaseLang.startsWith("scc")) {
            return SERBIAN;
        }
        if (lowerCaseLang.startsWith("en")) {
            return ENGLISH;
        }
        if (lowerCaseLang.startsWith("hr")) {
            return CROATIAN;
        }
        if (lowerCaseLang.startsWith("sl")) {
            return SLOVENIAN;
        }
        if (lowerCaseLang.startsWith("ro") || lowerCaseLang.startsWith("rum")) {
            return ROMANIAN;
        }
        if (lowerCaseLang.startsWith("fr")) {
            return FRENCH;
        }
        if (lowerCaseLang.startsWith("es") || lowerCaseLang.startsWith("spa")) {
            return SPANISH;
        }
        if (lowerCaseLang.startsWith("bs") || lowerCaseLang.startsWith("bos")) {
            return BOSNIAN;
        }
        if (lowerCaseLang.startsWith("hu")) {
            return HUNGARIAN;
        }
        if (lowerCaseLang.startsWith("mk") || lowerCaseLang.startsWith("mac")) {
            return MACEDONIAN;
        }
        if (lowerCaseLang.startsWith("de") || lowerCaseLang.startsWith("ger")) {
            return GERMAN;
        }
        if (lowerCaseLang.startsWith("it")) {
            return ITALIAN;
        }
        if (lowerCaseLang.startsWith("pt") || lowerCaseLang.startsWith("por")) {
            return PORTUGUESE;
        }
        if (lowerCaseLang.startsWith("nl") || lowerCaseLang.startsWith("dut")) {
            return DUTCH;
        }
        if (lowerCaseLang.startsWith("mne")) {
            return MONTENEGRIN;
        }
        if (lowerCaseLang.equals("nosub")) {
            return NO_SUBTITLES;
        }
        return UNKNOWN;
    }
}
