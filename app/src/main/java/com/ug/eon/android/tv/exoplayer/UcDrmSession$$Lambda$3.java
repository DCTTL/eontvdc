package com.ug.eon.android.tv.exoplayer;

/* renamed from: com.ug.eon.android.tv.exoplayer.UcDrmSession$$Lambda$3 */
final /* synthetic */ class UcDrmSession$$Lambda$3 implements Runnable {
    private final UcDrmSession arg$1;
    private final Exception arg$2;

    UcDrmSession$$Lambda$3(UcDrmSession ucDrmSession, Exception exc) {
        this.arg$1 = ucDrmSession;
        this.arg$2 = exc;
    }

    public void run() {
        this.arg$1.lambda$onError$0$UcDrmSession(this.arg$2);
    }
}
