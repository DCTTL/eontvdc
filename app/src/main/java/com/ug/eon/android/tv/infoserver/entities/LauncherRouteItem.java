package com.ug.eon.android.tv.infoserver.entities;

import java.util.List;
import ch.qos.logback.core.CoreConstants;

/* renamed from: com.ug.eon.android.tv.infoserver.entities.LauncherRouteItem */
public class LauncherRouteItem {
    private String description;

    /* renamed from: id */
    private int f34id;
    private List<Image> images;
    private String route;
    private int serviceProviderId;
    private String title;

    public int getId() {
        return this.f34id;
    }

    public void setId(int id) {
        this.f34id = id;
    }

    public int getServiceProviderId() {
        return this.serviceProviderId;
    }

    public void setServiceProviderId(int serviceProviderId2) {
        this.serviceProviderId = serviceProviderId2;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description2) {
        this.description = description2;
    }

    public String getRoute() {
        return this.route;
    }

    public void setRoute(String route2) {
        this.route = route2;
    }

    public List<Image> getImages() {
        return this.images;
    }

    public void setImages(List<Image> images2) {
        this.images = images2;
    }

    public String toString() {
        return "LauncherRouteItem{id=" + this.f34id + ", serviceProviderId=" + this.serviceProviderId + ", title='" + this.title + CoreConstants.SINGLE_QUOTE_CHAR + ", description='" + this.description + CoreConstants.SINGLE_QUOTE_CHAR + ", route='" + this.route + CoreConstants.SINGLE_QUOTE_CHAR + ", images=" + this.images + CoreConstants.CURLY_RIGHT;
    }
}
