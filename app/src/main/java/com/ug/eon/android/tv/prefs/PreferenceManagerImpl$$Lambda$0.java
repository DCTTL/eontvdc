package com.ug.eon.android.tv.prefs;

import com.ug.eon.android.tv.util.Optional.Function;

/* renamed from: com.ug.eon.android.tv.prefs.PreferenceManagerImpl$$Lambda$0 */
final /* synthetic */ class PreferenceManagerImpl$$Lambda$0 implements Function {
    static final Function $instance = new PreferenceManagerImpl$$Lambda$0();

    private PreferenceManagerImpl$$Lambda$0() {
    }

    public Object apply(Object obj) {
        return ((AuthPrefs) obj).getAccessToken();
    }
}
