package com.ug.eon.android.tv.NetworkCheck;

/* renamed from: com.ug.eon.android.tv.NetworkCheck.OnNetworkChange */
public interface OnNetworkChange {
    void onConnectionChange(boolean z);
}
