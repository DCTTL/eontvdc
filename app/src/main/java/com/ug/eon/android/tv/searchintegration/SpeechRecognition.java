package com.ug.eon.android.tv.searchintegration;

/* renamed from: com.ug.eon.android.tv.searchintegration.SpeechRecognition */
public interface SpeechRecognition {
    void startListening();

    void stopListening();
}
