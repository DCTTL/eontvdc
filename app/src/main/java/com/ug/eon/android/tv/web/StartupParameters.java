package com.ug.eon.android.tv.web;

import com.ug.eon.android.tv.util.function.BooleanSupplier;

/* renamed from: com.ug.eon.android.tv.web.StartupParameters */
public class StartupParameters {
    private BooleanSupplier recordAudioPermissionCheck;
    private String startupAction;
    private String startupActionData;
    private String startupMode;

    public String getStartupAction() {
        return this.startupAction != null ? this.startupAction : "";
    }

    public void setStartupAction(String startupAction2) {
        this.startupAction = startupAction2;
    }

    public String getStartupActionData() {
        return this.startupActionData != null ? this.startupActionData : "";
    }

    public void setStartupActionData(String startupActionData2) {
        this.startupActionData = startupActionData2;
    }

    public String getStartupMode() {
        return this.startupMode != null ? this.startupMode : "";
    }

    public void setStartupMode(String mode) {
        this.startupMode = mode;
    }

    public void setRecordAudioPermissionCheck(BooleanSupplier recordAudioCheck) {
        this.recordAudioPermissionCheck = recordAudioCheck;
    }

    public BooleanSupplier getRecordAudioPermissionCheck() {
        return this.recordAudioPermissionCheck;
    }
}
