package com.ug.eon.android.tv.NetworkCheck;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.ug.eon.android.tv.util.LogUC;

/* renamed from: com.ug.eon.android.tv.NetworkCheck.NetworkReceiver */
public class NetworkReceiver extends BroadcastReceiver {
    private static final String TAG = NetworkReceiver.class.getName();
    private OnNetworkChange mConnectionListener;

    public void onReceive(Context context, Intent intent) {
        LogUC.m39d(TAG, intent.getAction());
        boolean networkStatus = isNetworkConnected(context);
        LogUC.m39d(TAG, "Network status: " + networkStatus);
        if (this.mConnectionListener != null) {
            this.mConnectionListener.onConnectionChange(networkStatus);
        }
    }

    private boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm == null) {
            return false;
        }
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork == null) {
            return false;
        }
        LogUC.m39d(TAG, "NetworkType: " + activeNetwork.getTypeName());
        return activeNetwork.isConnected();
    }

    public void setOnConnectionChangeListener(OnNetworkChange listener) {
        this.mConnectionListener = listener;
    }
}
