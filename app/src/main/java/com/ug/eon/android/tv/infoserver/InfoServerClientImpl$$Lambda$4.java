package com.ug.eon.android.tv.infoserver;

import com.ug.eon.android.tv.util.Optional.Function;

/* renamed from: com.ug.eon.android.tv.infoserver.InfoServerClientImpl$$Lambda$4 */
final /* synthetic */ class InfoServerClientImpl$$Lambda$4 implements Function {
    private final InfoServerClientImpl arg$1;

    InfoServerClientImpl$$Lambda$4(InfoServerClientImpl infoServerClientImpl) {
        this.arg$1 = infoServerClientImpl;
    }

    public Object apply(Object obj) {
        return this.arg$1.lambda$getLauncherRouteItems$4$InfoServerClientImpl((ISApi) obj);
    }
}
