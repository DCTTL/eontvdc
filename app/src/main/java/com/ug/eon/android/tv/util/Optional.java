package com.ug.eon.android.tv.util;

import java.util.Objects;
import java.util.function.Supplier;

/* renamed from: com.ug.eon.android.tv.util.Optional */
public class Optional<T> {
    private T value;

    /* renamed from: com.ug.eon.android.tv.util.Optional$Action */
    public interface Action<T> {
        void apply(T t);
    }

    /* renamed from: com.ug.eon.android.tv.util.Optional$Function */
    public interface Function<T, R> {
        R apply(T t);
    }

    private Optional() {
        this.value = null;
    }

    private Optional(T value2) {
        this.value = Objects.requireNonNull(value2);
    }

    public static <T> Optional<T> empty() {
        return new Optional<>();
    }

    /* renamed from: of */
    public static <T> Optional<T> m44of(T value2) {
        return new Optional<>(value2);
    }

    public static <T> Optional<T> ofNullable(T value2) {
        return value2 == null ? empty() : m44of(value2);
    }

    public <R> Optional<R> map(Function<? super T, ? extends R> mapper) {
        Objects.requireNonNull(mapper);
        if (this.value == null) {
            return empty();
        }
        return ofNullable(mapper.apply(this.value));
    }

    public void ifPresent(Action<T> action) {
        if (this.value != null) {
            action.apply(this.value);
        }
    }

    public boolean isPresent() {
        return this.value != null;
    }

    public T get() {
        return this.value;
    }

    public T orElse(T other) {
        return this.value != null ? this.value : other;
    }

    public <X extends Throwable> T orElseThrow(Supplier<? extends X> exceptionSupplier) throws Throwable {
        if (this.value != null) {
            return this.value;
        }
        throw ((Throwable) exceptionSupplier.get());
    }
}
