package com.ug.eon.android.tv.prefs;

import com.google.gson.annotations.SerializedName;

/* renamed from: com.ug.eon.android.tv.prefs.AuthPrefs */
public class AuthPrefs {
    private String access_token;
    private int comm_id;
    private String device_number;
    private String refresh_token;
    @SerializedName("stream_key")
    private String streamKey;
    @SerializedName("stream_un")
    private String streamUn;
    private String[] topics;

    public String getAccessToken() {
        return this.access_token;
    }

    public void setAccessToken(String accessToken) {
        this.access_token = accessToken;
    }

    public String getRefreshToken() {
        return this.refresh_token;
    }

    public void setRefreshToken(String refreshToken) {
        this.refresh_token = refreshToken;
    }

    public String getDeviceNumber() {
        return this.device_number;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.device_number = deviceNumber;
    }

    public int getCommId() {
        return this.comm_id;
    }

    public void setCommId(int commId) {
        this.comm_id = commId;
    }

    public String getStreamKey() {
        return this.streamKey;
    }

    public String getStreamUn() {
        return this.streamUn;
    }

    public String[] getTopics() {
        return this.topics;
    }
}
