package com.ug.eon.android.tv.web;

/* renamed from: com.ug.eon.android.tv.web.UcWebInterface$$Lambda$8 */
final /* synthetic */ class UcWebInterface$$Lambda$8 implements Runnable {
    private final UcWebInterface arg$1;
    private final int arg$2;
    private final int arg$3;

    UcWebInterface$$Lambda$8(UcWebInterface ucWebInterface, int i, int i2) {
        this.arg$1 = ucWebInterface;
        this.arg$2 = i;
        this.arg$3 = i2;
    }

    public void run() {
        this.arg$1.lambda$sendEvent$4$UcWebInterface(this.arg$2, this.arg$3);
    }
}
