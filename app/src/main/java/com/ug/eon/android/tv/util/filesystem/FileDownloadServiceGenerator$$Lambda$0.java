package com.ug.eon.android.tv.util.filesystem;

import com.ug.eon.android.tv.prefs.ServerPrefs;
import com.ug.eon.android.tv.util.Optional.Function;

/* renamed from: com.ug.eon.android.tv.util.filesystem.FileDownloadServiceGenerator$$Lambda$0 */
final /* synthetic */ class FileDownloadServiceGenerator$$Lambda$0 implements Function {
    static final Function $instance = new FileDownloadServiceGenerator$$Lambda$0();

    private FileDownloadServiceGenerator$$Lambda$0() {
    }

    public Object apply(Object obj) {
        return FileDownloadServiceGenerator.lambda$createFileDownloadClient$0$FileDownloadServiceGenerator((ServerPrefs) obj);
    }
}
