package com.ug.eon.android.tv.searchintegration;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.ug.eon.android.tv.TvActivity;
import com.ug.eon.android.tv.util.LogUC;

/* renamed from: com.ug.eon.android.tv.searchintegration.EonSearchActivity */
public abstract class EonSearchActivity extends Activity {
    private static final String TAG = EonSearchActivity.class.getName();

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUC.m41i(TAG, "app global search requested");
        startMainActivity();
    }

    private void startMainActivity() {
        Context context = getApplicationContext();
        Intent mainActivityIntent = new Intent(context, TvActivity.class);
        mainActivityIntent.addFlags(805306368);
        mainActivityIntent.setAction("android.intent.action.MAIN");
        mainActivityIntent.addCategory("android.intent.category.LAUNCHER");
        String data = getIntent().getDataString();
        if (data != null && !data.isEmpty()) {
            mainActivityIntent.putExtra("deepLink", "detail");
            mainActivityIntent.putExtra("deepLinkData", data);
        }
        context.startActivity(mainActivityIntent);
        finish();
    }
}
