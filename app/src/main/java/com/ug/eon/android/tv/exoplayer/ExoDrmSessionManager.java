package com.ug.eon.android.tv.exoplayer;

import android.media.MediaDrm;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.Nullable;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.drm.DefaultDrmSessionManager;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.drm.DrmSession;
import com.google.android.exoplayer2.drm.DrmSessionManager;
import com.google.android.exoplayer2.drm.FrameworkMediaCrypto;
import com.google.android.exoplayer2.drm.FrameworkMediaDrm;
import com.google.android.exoplayer2.drm.HttpMediaDrmCallback;
import com.google.android.exoplayer2.drm.UnsupportedDrmException;
import com.ug.eon.android.tv.util.LogUC;
import java.util.function.Supplier;

/* renamed from: com.ug.eon.android.tv.exoplayer.ExoDrmSessionManager */
public final class ExoDrmSessionManager implements DrmSessionManager<FrameworkMediaCrypto> {
    private static final boolean MULTI_SESSION_DRM = true;
    private static final String TAG = ExoDrmSessionManager.class.getName();
    private DrmSessionManager<FrameworkMediaCrypto> mDrmSessionManager;
    private final Supplier<DrmInfo> mDrmSupplier;
    private final Handler mHandler;

    ExoDrmSessionManager(Supplier<DrmInfo> drmSupplier, Handler handler) {
        this.mDrmSupplier = drmSupplier;
        this.mHandler = handler;
    }

    public boolean canAcquireSession(DrmInitData drmInitData) {
        boolean canAcquireSession;
        if (this.mDrmSessionManager == null || !this.mDrmSessionManager.canAcquireSession(drmInitData)) {
            canAcquireSession = false;
        } else {
            canAcquireSession = true;
        }
        LogUC.m39d(TAG, "canAcquireSession: " + canAcquireSession);
        if (canAcquireSession) {
            return true;
        }
        if (!isWidevineSupported(drmInitData)) {
            return false;
        }
        DrmInfo drmInfo = (DrmInfo) this.mDrmSupplier.get();
        if (drmInfo == null || drmInfo.getLicenseServerUrl() == null) {
            LogUC.m39d(TAG, "drm info is null");
            return false;
        }
        this.mDrmSessionManager = createDrmSessionManager(drmInfo);
        if (this.mDrmSessionManager == null || !this.mDrmSessionManager.canAcquireSession(drmInitData)) {
            return false;
        }
        return true;
    }

    public DrmSession<FrameworkMediaCrypto> acquireSession(Looper playbackLooper, DrmInitData drmInitData) {
        DrmSession<FrameworkMediaCrypto> drmSession = this.mDrmSessionManager != null ? this.mDrmSessionManager.acquireSession(playbackLooper, drmInitData) : null;
        LogUC.m39d(TAG, "acquireSession, drm session null: " + (drmSession == null));
        return drmSession;
    }

    @Nullable
    private DrmSessionManager<FrameworkMediaCrypto> createDrmSessionManager(DrmInfo drmInfo) {
        LogUC.m39d(TAG, "createDrmSessionManager");
        HttpMediaDrmCallback drmCallback = new HttpMediaDrmCallback(drmInfo.getLicenseServerUrl(), new UcHttpDataSourceFactory(null));
        String[] drmKeyProperties = drmInfo.getDrmKeyProperties();
        for (int i = 0; i < drmKeyProperties.length - 1; i += 2) {
            drmCallback.setKeyRequestProperty(drmKeyProperties[i], drmKeyProperties[i + 1]);
        }
        try {
            return new DefaultDrmSessionManager(drmInfo.getSelectedCdmUUID(), FrameworkMediaDrm.newInstance(drmInfo.getSelectedCdmUUID()), drmCallback, null);//TODO, this.mHandler, null, true);
        } catch (UnsupportedDrmException e) {
            LogUC.m40e(TAG, "createDrmSessionManager " + Log.getStackTraceString(e));
            return null;
        }
    }

    public void releaseSession(DrmSession<FrameworkMediaCrypto> drmSession) {
        LogUC.m39d(TAG, "releaseSession: " + drmSession.getState());
        if (this.mDrmSessionManager != null) {
            this.mDrmSessionManager.releaseSession(drmSession);
        }
    }

    private boolean isWidevineSupported(DrmInitData drmInitData) {
        for (int i = 0; i < drmInitData.schemeDataCount; i++) {
            if (drmInitData.get(i).matches(C.WIDEVINE_UUID) && MediaDrm.isCryptoSchemeSupported(C.WIDEVINE_UUID)) {
                return true;
            }
        }
        return false;
    }
}
