package com.ug.eon.android.tv.exoplayer;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import com.google.android.exoplayer2.source.AdaptiveMediaSourceEventListener;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MediaSourceEventListener;
import com.google.android.exoplayer2.source.dash.DashChunkSource.Factory;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.TransferListener;

/* renamed from: com.ug.eon.android.tv.exoplayer.ExoMediaSource */
class ExoMediaSource {
    static final DefaultBandwidthMeter BANDWIDTH_METER = new DefaultBandwidthMeter();
    private Context mContext;
    private Handler mHandler = new Handler();

    ExoMediaSource(Context context) {
        this.mContext = context;
    }

    /* access modifiers changed from: 0000 */
    public MediaSource getHlsMediaSource(Uri uri, AdaptiveMediaSourceEventListener listener) {
        return new HlsMediaSource(uri, dataSource(this.mContext, BANDWIDTH_METER), new Handler(), listener);
    }

    /* access modifiers changed from: 0000 */
    public MediaSource getDashMediaSource(Uri uri, AdaptiveMediaSourceEventListener listener) {
        return new DashMediaSource(uri, dataSource(this.mContext, null), (Factory) new DefaultDashChunkSource.Factory(dataSource(this.mContext, BANDWIDTH_METER)), this.mHandler, (MediaSourceEventListener) listener);
    }

    private DataSource.Factory dataSource(Context context, TransferListener listener) {
        return new DefaultDataSourceFactory(context.getApplicationContext(), listener, (DataSource.Factory) new UcHttpDataSourceFactory(listener));
    }
}
