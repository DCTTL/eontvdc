package com.ug.eon.android.tv;

import android.annotation.SuppressLint;
import android.os.SystemClock;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.gson.JsonObject;
import com.ug.eon.android.tv.util.LogUC;
import com.ug.eon.android.tv.web.StartupParameters;
import org.apache.commons.io.IOUtils;

/* renamed from: com.ug.eon.android.tv.ChameleonView */
public class ChameleonView implements Chameleon {
    private static final String CALL_JS_METHOD_FORMAT = "javascript:window.%s";
    private static final String TAG = ChameleonView.class.getSimpleName();
    /* access modifiers changed from: private */
    public WebView mWebView;

    public ChameleonView(WebView webView) {
        this.mWebView = webView;
        setup();
    }

    public void onResumePauseActivity(boolean data, boolean playLastAllowed) {
        callJsMethod("onResumePauseActivity", Boolean.valueOf(data), Boolean.valueOf(playLastAllowed));
    }

    public void onNativeKeyDown(int keyCode) {
        callJsMethod("onNativeKeyDown", Integer.valueOf(keyCode));
    }

    public void onNativeKeyUp(int keyCode) {
        callJsMethod("onNativeKeyUp", Integer.valueOf(keyCode));
    }

    public void onPlayerStateChanged(int finalEvent) {
        callJsMethod("onPlayerStateChanged", Integer.valueOf(finalEvent));
    }

    public void onNetworkCheck(boolean value) {
        callJsMethod("onNetworkCheck", Boolean.valueOf(value));
    }

    public void onBitrateChanged(int finalEvent) {
        callJsMethod("onBitrateChanged", Integer.valueOf(finalEvent));
    }

    public void updateChannelsData() {
        callJsMethod("updateChannelsData", new Object[0]);
    }

    public void changeRoute(String route, int channelId) {
        callJsMethod("changeRoute", route, Integer.valueOf(channelId));
    }

    public void changeRoute(String route) {
        callJsMethod("changeRoute", route);
    }

    public void linkToContent(String type, JsonObject content) {
        callJsMethod("linkToContent", type, content);
    }

    public void onRefreshToken() {
        callJsMethod("onRefreshToken", new Object[0]);
    }

    public void showOsdNotification(String message) {
        callJsMethod("showOsdNotification", message.replace(IOUtils.LINE_SEPARATOR_UNIX, "\\n").replace("'", "\\'"));
    }

    public void showOTTFingerprint(String position, long duration) {
        callJsMethod("showOTTFingerprint", position, Long.valueOf(duration));
    }

    public void onFinalVoiceResult(String result) {
        callJsMethod("onFinalVoiceResult", result);
    }

    public void onCancelVoiceSearch() {
        callJsMethod("onCancelVoiceSearch", new Object[0]);
    }

    public void onPartialVoiceResult(String result) {
        callJsMethod("onPartialVoiceResult", result);
    }

    public void onUpdate(String message) {
        callJsMethod("onUpdate", message);
    }

    public void onLoggingLevelChanged(int level) {
        callJsMethod("onLoggingLevelChanged", Integer.valueOf(level));
    }

    public void onChannelImagesDownloaded(boolean result) {
        callJsMethod("onChannelImagesDownloaded", Boolean.valueOf(result));
    }

    public void destroy() {
        this.mWebView.destroy();
    }

    public void loadChameleon(StartupParameters startupParams) {
        String simpleUrl = this.mWebView.getContext().getResources().getString(R.string.project_id);
        String clientId = BuildConfigDev.CLIENT_ID;
        String url = simpleUrl.concat("?clientId=" + clientId + "&clientSecret=" + BuildConfigDev.CLIENT_SECRET + "&productId=" + "eon" + "&mode=" + (startupParams.getStartupMode().isEmpty() ? "default" : startupParams.getStartupMode()) + "&t=" + SystemClock.currentThreadTimeMillis());
        LogUC.m41i(TAG, "Loading web app, url = " + url);
        this.mWebView.loadUrl(url);
    }

    @SuppressLint("JavascriptInterface")
    public void addInterface(Object object, String name) {
        this.mWebView.addJavascriptInterface(object, name);
    }

    public void setUpScale(int width) {
        if (width >= 1920) {
            this.mWebView.setInitialScale(100);
            return;
        }
        this.mWebView.getSettings().setUseWideViewPort(true);
        this.mWebView.setInitialScale(66);
    }

    public void setBackgroundColor(int color) {
        this.mWebView.setBackgroundColor(color);
    }

    public void clearCache() {
        this.mWebView.clearCache(true);
    }

    private void setup() {
        WebSettings webSettings = this.mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setAllowUniversalAccessFromFileURLs(true);
        webSettings.setMinimumFontSize(1);
        webSettings.setDomStorageEnabled(true);
        this.mWebView.setLayerType(2, null);
        this.mWebView.getSettings().setCacheMode(2);
        this.mWebView.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                ChameleonView.this.mWebView.setBackgroundColor(0);
            }
        });
        setupUserAgent();
    }

    private void setupUserAgent() {
        this.mWebView.getSettings().setUserAgentString(this.mWebView.getSettings().getUserAgentString() + " " + this.mWebView.getContext().getString(R.string.user_agent));
    }

    private void callJsMethod(String methodName, Object... params) {
        LogUC.m39d(TAG, createJsCall(methodName, params));
        this.mWebView.loadUrl(String.format(CALL_JS_METHOD_FORMAT, new Object[]{createJsCall(methodName, params)}));
    }

    public static String createJsCall(String methodName, Object... params) {
        String methodName2 = methodName + "(";
        int size = params.length;
        for (int i = 0; i < size; i++) {
            methodName2 = methodName2 + parseParameter(params[i]);
            if (i != size - 1) {
                methodName2 = methodName2 + ",";
            }
        }
        return methodName2 + ")";
    }

    private static Object parseParameter(Object param) {
        if (param instanceof String) {
            return "'" + param + "'";
        }
        if ((param instanceof Number) || (param instanceof Boolean)) {
            return param;
        }
        if (param instanceof JsonObject) {
            return param.toString();
        }
        return "null";
    }

    public void onDownloadTaskCompleted(boolean result) {
        onChannelImagesDownloaded(result);
    }

    public void onRCUInfoReceived(String result) {
        callJsMethod("onRCUInfoReceived", result);
    }

    public void onCDNInfoReceived(String result) {
        callJsMethod("onCDNInfoReceived", result);
    }

    public void setVideoEncoding(String encoding) {
        callJsMethod("setVideoEncoding", encoding);
    }
}
