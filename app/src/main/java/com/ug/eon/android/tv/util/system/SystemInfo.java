package com.ug.eon.android.tv.util.system;

import android.os.Build.VERSION;
import android.os.Environment;

/* renamed from: com.ug.eon.android.tv.util.system.SystemInfo */
public class SystemInfo {
    private SystemInfo() {
    }

    public static int getWatchNextMinSupportedApiLevel() {
        return 26;
    }

    public static int getAndroidSDKVersion() {
        return VERSION.SDK_INT;
    }

    public static boolean isExternalStorageWritable() {
        return "mounted".equals(Environment.getExternalStorageState());
    }

    public static boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        return "mounted".equals(state) || "mounted_ro".equals(state);
    }

    public static String getExternalDataFolder() {
        return Environment.getExternalStorageDirectory().getPath();
    }
}
