package com.ug.eon.android.tv.config;

import android.content.Context;
import com.ug.eon.android.tv.util.LogUC;
import java.io.IOException;
import java.util.Properties;

/* renamed from: com.ug.eon.android.tv.config.DefaultPropertyFileProvider */
public class DefaultPropertyFileProvider implements PropertyFileProvider {
    private static final String TAG = DefaultPropertyFileProvider.class.getName();

    public Properties getProperties(Context context, String filePath) {
        Properties properties = new Properties();
        try {
            properties.load(context.getAssets().open(filePath));
        } catch (IOException e) {
            LogUC.m39d(TAG, "Cannot find/read property file (features.properties)");
        }
        return properties;
    }
}
