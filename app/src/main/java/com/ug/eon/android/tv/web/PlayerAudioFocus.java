package com.ug.eon.android.tv.web;

import android.media.AudioAttributes;
import android.media.AudioFocusRequest;
import android.media.AudioFocusRequest.Builder;
import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.os.Build.VERSION;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import com.ug.eon.android.tv.util.LogUC;

/* renamed from: com.ug.eon.android.tv.web.PlayerAudioFocus */
public class PlayerAudioFocus {
    private static final String TAG = PlayerAudioFocus.class.getName();
    private OnAudioFocusChangeListener mAudioChangeListener;
    private AudioFocusRequest mAudioFocus;
    private AudioManager mAudioManger;

    public PlayerAudioFocus(AudioManager audioManager, OnAudioFocusChangeListener listener) {
        this.mAudioManger = audioManager;
        this.mAudioChangeListener = listener;
        if (VERSION.SDK_INT >= 26) {
            this.mAudioFocus = getAudioFocus();
        }
    }

    public boolean requestAudioFocus() {
        LogUC.m39d(TAG, "request audio focus");
        if (VERSION.SDK_INT >= 26) {
            if (this.mAudioManger.requestAudioFocus(this.mAudioFocus) == 1) {
                return true;
            }
            return false;
        } else if (this.mAudioManger.requestAudioFocus(this.mAudioChangeListener, 3, 1) != 1) {
            return false;
        } else {
            return true;
        }
    }

    public void abandonAudioFocus() {
        LogUC.m39d(TAG, "abandon audio focus");
        if (VERSION.SDK_INT >= 26) {
            this.mAudioManger.abandonAudioFocusRequest(this.mAudioFocus);
        } else {
            this.mAudioManger.abandonAudioFocus(this.mAudioChangeListener);
        }
    }

    @RequiresApi(api = 26)
    private AudioFocusRequest getAudioFocus() {
        return new Builder(1).setAudioAttributes(new AudioAttributes.Builder().setUsage(1).setContentType(3).build()).setAcceptsDelayedFocusGain(true).setOnAudioFocusChangeListener(this.mAudioChangeListener, new Handler()).build();
    }
}
