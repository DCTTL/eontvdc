package com.ug.eon.android.tv.util;

/* renamed from: com.ug.eon.android.tv.util.RCUInfoTaskResultListener */
public interface RCUInfoTaskResultListener {
    void onRCUInfoReceived(String str);
}
