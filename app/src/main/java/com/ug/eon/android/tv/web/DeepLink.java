package com.ug.eon.android.tv.web;

import androidx.annotation.Nullable;
import com.google.firebase.analytics.FirebaseAnalytics.Event;
import com.ug.eon.android.tv.config.PropertyFileConfigurationManager;

/* renamed from: com.ug.eon.android.tv.web.DeepLink */
public enum DeepLink {
    NOW_TV("nowontv", "NowTv"),
    GUIDE("guide", "Guide"),
    LIVE_TV("livetv", "PlayerTv"),
    VOD("vod", "VodLanding"),
    RADIO("radio", "Radio"),
    HOME("home", "Home"),
    WATCH_NEXT(PropertyFileConfigurationManager.WATCH_NEXT_PROPERTY, "PlayerTv"),
    DETAIL("detail", null),
    PLAYER("player", "Player"),
    SEARCH(Event.SEARCH, "Search"),
    LIBRARY("mylibrary", "MyLibrary");
    
    private String action;
    private String route;

    private DeepLink(String action2, String route2) {
        this.action = action2;
        this.route = route2;
    }

    @Nullable
    public static DeepLink getDeepLink(@Nullable String action2) {
        DeepLink[] values;
        if (action2 == null) {
            return null;
        }
        for (DeepLink link : values()) {
            if (link.action.equals(action2)) {
                return link;
            }
        }
        return null;
    }

    public String getRoute() {
        return this.route;
    }
}
