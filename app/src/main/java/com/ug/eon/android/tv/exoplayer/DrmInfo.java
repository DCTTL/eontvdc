package com.ug.eon.android.tv.exoplayer;

import java.util.UUID;

/* renamed from: com.ug.eon.android.tv.exoplayer.DrmInfo */
public final class DrmInfo {
    private String[] drmKeyProperties;
    private final String licenseServerUrl;
    private final UUID selectedCdmUUID;

    DrmInfo(UUID selectedCdmUUID2, String licenseServerUrl2) {
        if (selectedCdmUUID2 == null) {
            throw new IllegalArgumentException("CDM UUID is null");
        }
        this.selectedCdmUUID = selectedCdmUUID2;
        this.licenseServerUrl = licenseServerUrl2;
    }

    /* access modifiers changed from: 0000 */
    public String getLicenseServerUrl() {
        return this.licenseServerUrl;
    }

    /* access modifiers changed from: 0000 */
    public UUID getSelectedCdmUUID() {
        return this.selectedCdmUUID;
    }

    /* access modifiers changed from: 0000 */
    public void setDrmKeyProperties(String[] drmKeyProperties2) {
        this.drmKeyProperties = drmKeyProperties2;
    }

    /* access modifiers changed from: 0000 */
    public String[] getDrmKeyProperties() {
        return this.drmKeyProperties;
    }
}
